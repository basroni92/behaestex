/* eslint-disable object-shorthand */

/* global Chart, CustomTooltips, getStyle, hexToRgba */

/**
 * --------------------------------------------------------------------------
 * CoreUI Free Boostrap Admin Template (v2.1.10): main.js
 * Licensed under MIT (https://coreui.io/license)
 * --------------------------------------------------------------------------
 */

/* eslint-disable no-magic-numbers */
// Disable the on-canvas tooltip

var _show_modal = $.fn.modal.Constructor.prototype.show;
$.fn.modal.Constructor.prototype.show = function() {
	var a = $(this._element),
		b = a.find('.modal-content'),
		c = arguments[0];
	if(b.length > 0 && !b.hasClass('ui-draggable'))
		b.draggable({handle: ".modal-header"});
    _show_modal.apply(this, arguments);
	if(c !== 'undefined')
		a.data('invoker', $(c));
};

var _hiden_modal = $.fn.modal.Constructor.prototype.hide;
$.fn.modal.Constructor.prototype.hide = function(){
	_hiden_modal.apply(this, arguments);
	var currElement = $(this._element),
		currInvoker = currElement.data('invoker');
	if(currInvoker && currInvoker.closest('form').length > 0)
		return;
	var modal_show = $('.modal.show');
	if(modal_show.length === 0){
		let pencarian_view = $MAIN_CONTENT.find('input[type="search"]:visible');
		setTimeout(() => {
			pencarian_view.trigger('focus');
		}, 500);
	}
};

/*********************************************************
** Base Prototype 										**
*********************************************************/
	
/* Number Format */
Number.prototype.formatMoney = function(c, d, t){
	var n = this,
		c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? (_DECIMAL_SEPARATOR_ || ".") : d,
		t = t == undefined ? (_THOUSAND_SEPARATOR_ || ",") : t,
		s = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
[_THOUSAND_SEPARATOR_, _DECIMAL_SEPARATOR_] = ((9999).toLocaleString('En-us', {minimumFractionDigits : 1})).replace(/[0-9]/g, '').split('');

/* String TitleCase prototype */
String.prototype.toTitleCase = function() {
  return this.replace(/(^|\s)\S/g, function(t) { return t.toUpperCase() });
}

/* Initialize */
var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE_UP = $('#menu_toggle_up'),
    $MENU_TOGGLE_DOWN = $('#menu_toggle_down'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $MAIN_CONTENT = $('#main-content'),
	$BREADCRUMB = $('#breadcrumb'),
	$LEFT_COL = $('.left_col'),
	$RIGHT_COL = $('.right_col'),
	$NAV_MENU = $("h3.nav-title + ul.nav"),
    $FOOTER = $('footer'),
	$CURRENT_MODULE,
	errorForm = {},
	hashCode = function(str) {
		return str.split('').reduce((prevHash, currVal) =>
			(((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, 0);
	},
	isJSON = function(str) {
		try { JSON.parse(str) }
		catch (e) { return false}
		return true;
	},
	randNum = function() {
	  return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
	},
	randString = function (length = 5, possible = null) {
		let str = "";
		possible = possible || "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for(var i = 0; i < length; i++ )
			str += possible.charAt(Math.floor(Math.random() * possible.length));
		return str;
	},
	keyCodeRestrict = function(obj, validChars){
		var keychar = '', key = obj.which;
		if(key == null) return true;
		keychar = String.fromCharCode(key);
		keychar = keychar.toLowerCase();
		validChars = validChars.toLowerCase();
		if(validChars.indexOf(keychar) != -1) return true;
		if(key==null || key==0 || key==8 || key==9 || key==13 || key==27) return true;
		return false;
	},
	setCookie = function(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	},
	getCookie = function(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	},
	removeCookie = function(name) {   
		document.cookie = name +'=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	},
	$error_http = {
		401: function () {
			window.location = $BASE_URL+'login';
		},
	},
	show_message = function(message, messageType = 'error', callback){
		let dialog_message = $("#main-dialog-message");
		if(dialog_message.length > 0)
			dialog_message.remove();
		
		if(callback != undefined){
			if(typeof callback === 'string')
				var callback = new Function(callback);
			callback();
		}
		
		let title, class_dialog;
		switch(messageType){
			case 'success': title = 'Berhasil', class_dialog = 'success'; break;
			case 'warning': title = 'Peringatan', class_dialog = 'warning'; break;
			case 'info': title = 'Informasi', class_dialog = 'info'; break;
			default: title = 'Kesalahan', class_dialog = 'danger'; break;
		}
		
		dialog_message = $('<div id="main-dialog-message"/>').addClass('modal').attr({'tabIndex':'-1', 'role':'dialog'}).appendTo($BODY);
		let modal_dialog = $('<div/>').addClass('modal-dialog modal-'+class_dialog).attr('role', 'document').appendTo(dialog_message),
			modal_content = $('<div/>').addClass('modal-content').appendTo(modal_dialog),
			modal_header = $('<div/>').addClass('modal-header').appendTo(modal_content),
			modal_title = $('<h5/>').addClass('modal-title').html(title).appendTo(modal_header),
			modal_close = $('<button/>').addClass('close').attr({'type':'button', 'data-dismiss':'modal', 'aria-label':'Close'}).appendTo(modal_header);
			modal_span_close = $('<span/>').attr('aria-hidden', 'true').html('<i class="fa fa-close"></i>').appendTo(modal_header),
			modal_body = $('<div/>').addClass('modal-body').appendTo(modal_content),
			modal_value_body = $('<p/>').html(message).appendTo(modal_body);
			modal_footer = $('<div/>').addClass('modal-footer').appendTo(modal_content),
			modal_footer_close = $('<button/>').addClass('btn btn-'+class_dialog).attr({'type':'button', 'data-dismiss':'modal'}).html('Tutup').appendTo(modal_footer);
		dialog_message.on('shown.bs.modal', function(){
			setTimeout(() => {
				modal_footer_close[0].focus();
			}, 100);
		});
		dialog_message.modal('toggle');
		
	},
	confirm_dialog = function(action = null, body = null, btn = null, title = null){
		body = body || 'Data akan disetujui, apakah anda yakin ?';
		btn = btn || {'value':'Menyetujui', 'class':'primary'};
		title = title || 'Apakah Anda Yakin ?';
		let currentDialog = $('div[modal-type="dialog-confirm"]'),
			create_modal = function() {
				let dialog = $(`
					<div id="main-dialog-confirm" class="modal" tabIndex="-1" role="dialog" modal-type="dialog-confirm">
						<div class="modal-dialog modal-${btn.class}" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">${title}</h5>
									<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
									<span aria-hidden="true"><i class="fa fa-close"></i></span>
								</div>
								<div class="modal-body">
									<p>${body}</p>
								</div>
								<div class="modal-footer">
									<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
									<button id="btn-next" class="btn btn-${btn.class}" type="button">${btn.value}</button>
								</div>
							</div>
						</div>
					</div>
				`).appendTo($BODY);
				
				dialog.find('#btn-next').off().on('click', function(e){
					e.preventDefault();
					e.stopPropagation();
					$('.loading')[0].style.display = 'block';
					load_url(action.url, action.data, action.tipe, action.datatipe, action.onSuccess, action.onError, action.msgTipe);
				});
				dialog.on('shown.bs.modal', function(){
					dialog.find('#btn-next')[0].focus();
				});
				dialog.modal('toggle');
			};
		if(currentDialog.length > 0)
			$.when(currentDialog.remove()).then(create_modal());
		else {
			create_modal()
		}
	},
	/* Load URL Content by Ajax */
	load_url = async function(url, data, tipe = 'POST', datatipe = 'json', onSuccess = null, onError = null, msgTipe = 'modal'){
		$('.loading')[0].style.display = 'block';
		onSuccess = onSuccess || function(response){};
		onError = onError || function(err){};
		let resp = {Result: "ERROR"},
			options = {
				url: url,
				type: tipe,
				data: data,
				dataType: datatipe,
				success: function(response){
					$('.loading')[0].style.display = 'none';
					if(datatipe == 'json') {
						$.extend(true, resp, response);
						if(resp.Result == 'OK'){
							if(resp.Message){
								if(msgTipe === 'modal') show_message(resp.Message,"success"); 
								else toastr.success(resp.Message, 'Berhasil', optionsToastr);
							}
						}
						else if(resp.Message){
							if(msgTipe === 'modal') show_message(resp.Message);
							else toastr.error(resp.Message, 'Kesalahan', optionsToastr);
						}
						if(resp.Callback){
							if(typeof resp.Callback === 'string')
								(new Function('data', resp.Callback))(data);
							else if(typeof resp.Callback === 'object'){
								resp.Callback.forEach(el => {
									try {
										if(typeof el === 'string')
											(new Function('data', el))(data);
									} catch(errCallback) {
										console.error(errCallback);
									}
								});
							}
						}
						onSuccess(resp);
					}
					else onSuccess(response);
				},
				error: function(err){
					$('.loading')[0].style.display = 'none';
					if($error_http[err.status] != undefined)
						$error_http[err.status](err);
					else {
						if(msgTipe === 'modal') show_message(err.status+' - '+err.statusText);
						else toastr.error(err.status+' - '+err.statusText, 'Kesalahan', optionsToastr);
					}
					onError(err);
				}
			};
		if(data instanceof FormData)
			$.extend(true, options, {processData: false, contentType: false});
		await $.ajax(options);
	},
	/* Load Menu */
	load_menu = function(reload = false) {
		if(reload)
			load_url($BASE_URL + 'default_/user_menus', {}, 'POST', 'json', function(response){
				delete response.Result;
				var user_menus = generate_menu($LIST_MODULES = response, true);
				$NAV_MENU.html(user_menus);
				init_sidebar();
				if($CURRENT_MODULE){
					if(search_module($CURRENT_MODULE, $LIST_MODULES, 'link'))
						load_module($CURRENT_MODULE);
					else window.location.reload();
				}
			});
		else {
			$NAV_MENU.html(generate_menu($LIST_MODULES, true));
			init_sidebar();
			if($CURRENT_MODULE){
				if(search_module($CURRENT_MODULE, $LIST_MODULES, 'link'))
					load_module($CURRENT_MODULE);
				else window.location.reload();
			}
		}
	},
	/* Generate Menu */
	generate_menu = function(modules = $LIST_MODULES, root = false) {
		var mod_list = root ? '' : '<ul class="nav-dropdown-items">';
		Object.keys(modules).forEach(function(idx) {
			var mod = modules[idx],
				label = mod.label.toLowerCase().toTitleCase(),
				uri = mod.link.toLowerCase();
			if(mod.id.match(/^(M)/ig, mod.id))
				// mod_list += '<li class="nav-item"><a class="nav-link" href="' + uri + '"><i class="nav-icon icon-doc"></i>' + label + '</a></li>';
				mod_list += '<li class="nav-item"><a class="nav-link" href="' + uri + '">'+label+'</a></li>';
			else{
				icon = mod.icon == "" ? "list" : mod.icon.toLowerCase();
				mod_list += '<li class="nav-item nav-dropdown"><a class="nav-link nav-dropdown-toggle">' + (root ? '<i class="nav-icon icon-' + icon + '"></i>' : '') + label + '</span></a>';
				mod_list += generate_menu(mod.childs);
				mod_list += '</li>';
			}
		});
		mod_list += root ? '' : '</ul>';
		return mod_list;
	}
	/* Filter Menu */
	filter_menu = function(el, q) {
		var z = false,
			els = el.children;
		for (var x = 0; x < els.length; x++) {
			var a = els[x];
			if((a.tagName).toLowerCase() !== 'li')
				continue;
			var b = a.children[0];
			if((b.tagName).toLowerCase() !== 'a')
				continue;
			
			if(b.href !== '')
				var d = ((b.text).toLowerCase()).indexOf(q.toLowerCase()) !== -1;
			else{
				var c = a.children[1];
				if((c.tagName).toLowerCase() !== 'ul')
					continue;
				var d = filter_menu(c, q);
			}
			if(d){
				if(a.classList.contains('d-none'))
					a.classList.remove('d-none');
				z = d;
			}else{
				if(!a.classList.contains('d-none'))
					a.classList.add('d-none');
			}
		}
		return z;
	},
	/* Load Module */
	load_module = function(module) {
		var module_name = module.replace($BASE_URL, '').toLowerCase(),
			nav_link_el = $NAV_MENU.find('a.nav-link[href="' + module_name + '"'),
			group_label = nav_link_el.parent().parent().siblings().text(),
			menu_label = nav_link_el.text(),
			breadcrumb_group = $('<li/>').addClass('breadcrumb-item').text(group_label),
			breadcrumb_menu = $('<li/>').addClass('breadcrumb-item active"').text(menu_label);
		load_url($BASE_URL + module_name, {}, 'POST', 'html', function(response){
			$('body > .ui-front').empty().remove();
			$MAIN_CONTENT.html(response);
			$SIDEBAR_MENU.find('.nav .nav-link.active').removeClass('active');
			nav_link_el.addClass('active');
			$SIDEBAR_MENU.find('.nav .nav-item.open').removeClass('open');
			nav_link_el.parentsUntil('.nav').filter('.nav-item.nav-dropdown').addClass('open');
			if(group_label != '' || menu_label != undefined)
				$BREADCRUMB.html([breadcrumb_group, breadcrumb_menu]);
			$CURRENT_MODULE = module_name.toUpperCase();
			if($BODY.hasClass('sidebar-show'))
				$('.navbar-toggler.sidebar-toggler').trigger('click');
		});
	},
	/* Search Module */
	search_module = function(query, modules = $LIST_MODULES, attr = 'id') {
		var found = null;
		Object.keys(modules).forEach(function(idx) {
			if(modules[idx][attr] == query) {
				found = modules[idx];
				return;
			}
			if(modules[idx].childs) {
				if(search_module(query, modules[idx].childs, attr)) {
					found = modules[idx];
					return;
				}
			}
		});
		return found;
	},
	init_sidebar = function() {
		// TODO: This is some kind of easy fix, maybe we can improve this
		var setContentHeight = function () {
			// reset height
			$RIGHT_COL.css('min-height', $(window).height());
			var bodyHeight = $BODY.outerHeight(),
				footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
				leftColHeight = $LEFT_COL.height() + 0,
				contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;
			// normalize content
			contentHeight -= $NAV_MENU.height() + footerHeight;
			$RIGHT_COL.css('min-height', contentHeight);
		};
		
		// Menu Click
		$SIDEBAR_MENU.find('ul.nav a.nav-link').on('click', function(ev) {
			if(this.href) load_module(this.href);
			ev.preventDefault();
		});
		
		// toggle small or large menu 
		$("#menu_toggle_up, #menu_toggle_down").on('click', function() {
			setContentHeight();
		});
		
		// recompute content when resizing
		$(window).smartresize(function(){  
			setContentHeight();
		});
		setContentHeight();
	},
	init_validator = function() {
		if(typeof(validator) === 'undefined'){ return; }
		// initialize the validator function
		validator.message.date = 'not a real date';

		// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
		$('form')
			.on('blur', 'input[required], input.optional, select.required', validator.checkField)
			.on('change', 'select.required', validator.checkField)
			.on('keypress', 'input[required][pattern]', validator.keypress);

		$('.multi.required').on('keyup blur', 'input', function() {
			validator.checkField.apply($(this).siblings().last()[0]);
		});

		$('form').submit(function(e) {
			e.preventDefault();
			var submit = true;		
			// evaluate the form using generic validaing
			if(!validator.checkAll($(this)))
				submit = false;
			if(submit)
				this.submit();
			return false;
		});
	},
	change_password = function () {
		var dialogChangePassword = $('#change-password-modal'),
			formChangePassword = $('#change-password-form');
		load_url(
			$BASE_URL + 'default_/change_password',
			formChangePassword.serialize(),
			'POST',
			'json',
			function(response){
				if(response.Result == 'OK'){
					dialogChangePassword.modal('hide');
					formChangePassword[0].reset();
				}
			}
		);
	},
	is_allow = function(access, acl_list){
		for(let a = 0; a < acl_list.length; a++)
			if(acl_list[a] == access)
				return true;
		return false;
	},
	displayStatusMaster = function(status){
		if(status == '0')
			return '<i class="fa fa-check text-primary"></i>  BARU';
		else if (status == '1')
			return '<i class="fa fa-check text-success"></i>  AKTIF';
		else
			return '<i class="fa fa-close text-danger"></i>  NON AKTIF';
	},
	displayStatusTrans = function(status){
		if(status == '0')
			return '<i class="fa fa-check text-primary"></i>  BARU';
		else if (status == '1')
			return '<i class="fa fa-check text-success"></i>  APPROVE';
		else
			return '<i class="fa fa-close text-danger"></i>  BATAL';
	},
	show_histori = function(id){
		$('.loading')[0].style.display = 'block';
		let dialog = $('#show-histori'),
			options = {
				dom: "<'row'<'col-12 col-md-6 pb-2 pb-lg-0'l><'col-12 col-md-6'f>>" +
					"<'row'<'col-12'<'table-responsive'tr>>>" +
					"<'row'<'col-12 col-md-6'i><'col-12 col-md-6'p>>",
				processing: true,
				serverSide: true,
				sPaginationType: 'full',
				aaSorting: [2, 'desc'],
				ajax: {
					url: $BASE_URL+'default_/get_histori', 
					type: 'POST',
					data: {'unik_riwayat': id}
				},
				initComplete: function(){
					let table = $(this),
						tableId = table[0].id,
						search = $('#'+tableId+'_filter input[type="search"]');
					
					// Refresh View
					$('#refresh-histori').off().on('click', function(e){
						e.preventDefault();
						table.DataTable().ajax.reload();
					});
					
					// Search Auto Focus
					dialog.on('shown.bs.modal', function(){
						search[0].focus();
					});
					
					search.on('click', function(e){
						e.preventDefault();
						this.select();
					});
				},
				columns: [
					{title: 'RIWAYAT', width: '50%', data: 'riwayat'},
					{title: 'TANGGAL', width: '13%', data: 'tanggal', className: 'dt-body-center',
						render: (data, type, row) => {
							if(type === 'display' || type === 'filter'){
								let result = data.split('-');
								return result[2]+'-'+result[1]+'-'+result[0];
							}
							return data;
						}
					},
					{title: 'TGL RIWAYAT', data: 'tgl_riwayat', visible: false},
					{title: 'JAM', width: '9%', data: 'jam', className: 'dt-body-center'},
					{title: 'PENGGUNA', width: '28%', data: 'pengguna'}
				],
			};
		if(dialog.length > 0){
			dialog.remove();
		}
		dialog = $(`
				<div id="show-histori" class="modal" tabIndex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 id="modal-title" class="modal-title">
									<i class="fa fa-bars"></i> Data Riwayat
								</h5>
								<span id="refresh-histori" aria-hidden="true" class="pointer" title="Muat Ulang"><i class="fa fa-refresh"></i></span>
							</div>
							<div class="modal-body p-1 p-md-2 p-lg-3">
								<table id="table-histori" style="width:100%"></table>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							</div>
						</div>
					</div>
				</div>
			`).appendTo($BODY);
			let table_histori = $("#table-histori").DataTable(options);
			dialog.modal('toggle');
		$('.loading')[0].style.display = 'none';
	},
	create_filter = function(id, data = null){
		let filter = $('#'+id+'_filter'),
			divGroup = $('<div class="input-group"/>').appendTo(filter),
			divSubGroup = $('<div class="input-group-prepend"/>').appendTo(divGroup),
			search = filter.find('input[type="search"]').appendTo(divGroup),
			linkFilter = $('<a class="input-group-text pointer" data-toggle="dropdown" role="button" aria-haspopup=true aria-expanded=false/>').html('<i class="fa fa-filter"/>').appendTo(divSubGroup),
			divFilter = $('<div id="filter-search" class="dropdown-menu full filter-search"/>').appendTo(divSubGroup),
			divider = $('<div class="group-divider"/>').appendTo(divFilter),
			labelDivider = $('<div class="divider-label"/>').html('Filter').appendTo(divider);
		search.click(function(){ this.select(); });
		$.each(data, function(i, obj){
			var i = $('<div class="form-group row clear" '+(i === (Object.keys(data).length-1) ? '' : 'style="margin-bottom:2%;"')+'/>').appendTo(divFilter),
				label = $('<label class="col-2 col-md-2 col-form-label label-filter" for="'+obj.id+'"/>').html(obj.label).appendTo(i),
				div = $('<div class="col-10 col-md-10 select-filter"/>').appendTo(i);
			if(obj.type === undefined || obj.type == 'select'){
				select = $('<select id='+obj.id+' class="form-control"/>').appendTo(div);
				if(obj.value){
					$.each(obj.value, function (i, item) {
						select.append($('<option>', {value: i, text : item}));
						if(obj.defaultValue === undefined){
							if(i === '-') select.val('-');
						}
						else select.val(obj.defaultValue);
					});
				}
				select.change(e => {
					if(obj.callback && typeof obj.callback == 'function')
						obj.callback();
					$('#'+id).DataTable().ajax.reload();
				});
			}
			else if(obj.type == 'search'){
				let divGroupSearch = $('<div class="input-group"/>').appendTo(div),
					inputHidden = $('<input type="hidden" id="'+obj.id+'_id"/>').appendTo(divGroupSearch),
					inputText = $('<input type="text" id="'+obj.id+'" class="form-control" onclick="this.select()" placeholder="'+obj.label+'"/>').appendTo(divGroupSearch),
					clearFilter = $('<span class="fa fa-close close-filter"/>').appendTo(divGroupSearch),
					divSubGroupSearch = $('<div class="input-group-append" id="'+obj.id+'_btn"/>').appendTo(divGroupSearch),
					findSpan = $('<span class="input-group-text bg-primary pointer"/>').html('<i class="fa fa-search"></i>').appendTo(divSubGroupSearch);
				inputHidden.change(e => $('#'+id).DataTable().ajax.reload());
				clearFilter.off().click(e => {
					e.preventDefault();
					inputText.val('');
					inputHidden.val('').trigger('change');
				});
			}
			else if(obj.type == 'dateRange'){
				let dateRange = obj.defaultValue,
					divDate = $('<div class="input-group"/>').appendTo(div),
					inputHide = $('<input type="hidden" id="'+obj.id+'_id"/>').appendTo(divDate),
					inputDate = $('<input type="text" id="'+obj.id+'" class="form-control range-datepicker" readOnly/>').appendTo(divDate);
					divSubGroupDate = $('<div class="input-group-append"/>').appendTo(divDate),
					btnSpan = $('<span class="input-group-text bg-primary pointer"/>').html('<i class="fa fa-calendar"></i>').appendTo(divSubGroupDate),
					dateRangeOptions = {
						changeMonth: true,
						changeYear: true,
						rangeSelect: true,
						numberOfMonths: 2,
						dateFormat : "yy-mm-dd",
						onSelect: function(selectedDate){
							$.datepicker._defaults.multipleMonthsOption.onSelect(this, selectedDate, (a, b) => inputHide.val(a+';'+b).trigger('change'));
						},
						onClose: function(){
							$.datepicker._defaults.multipleMonthsOption.onClose($(this).data('datepicker'))
						},
						onChangeMonthYear: (a, b, c) => {
							setTimeout(() => $.datepicker._defaults.multipleMonthsOption.setHighlight(c, c.fromDate), 200);
						},
						beforeShowDay: function(date){
							return $.datepicker._defaults.multipleMonthsOption.beforeShowDay($(this).data('datepicker'), date);
						}
					};
				if(!dateRange){
					let tgl2 = new Date(),
						tgl1 = new Date(),
						strTgl1 = '',
						strTgl2 = '';
					tgl1.setMonth(tgl2.getMonth() - 1);
					strTgl1 = tgl1.getFullYear()+'-'+(tgl1.getMonth()+1).toString().padStart(2, 0)+'-'+tgl1.getDate().toString().padStart(2, 0);
					strTgl2 = tgl2.getFullYear()+'-'+(tgl2.getMonth()+1).toString().padStart(2, 0)+'-'+tgl2.getDate().toString().padStart(2, 0);
					dateRange = [strTgl1, strTgl2];
				}
				inputHide.val(dateRange.join(';')).change(e => $('#'+id).DataTable().ajax.reload());
				inputDate.datepicker(dateRangeOptions);
				inputDate.val(dateRange.join(' - '));
				btnSpan.off().click(e => {
					e.preventDefault();
					inputDate.datepicker('show');
				});
			}
		});
		filter.find('.input-group-prepend').on('click', function(e){
			if($(this).hasClass('show'))
				e.stopPropagation();
		});
	},
	browser = function(){
		var result = '';
		if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1)
			result = 'Opera';
		else if(navigator.userAgent.indexOf("Chrome") != -1)
			result = 'Chrome';
		else if(navigator.userAgent.indexOf("Safari") != -1)
			result = 'Safari';
		else if(navigator.userAgent.indexOf("Firefox") != -1) 
			result = 'Firefox';
		else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
			result = 'IE';
		else 
			result = 'Unknown';
		return result;
	},
	event_focus = function(element){
		let els = element.find('input, select, textarea, button')
			.filter(function(idx, el){
				if(el.type === 'button' && el.dataset.dismiss === 'modal') return false;
				if(el.type === 'hidden') return false;
				if(el.readOnly) return false;
				if(el.disabled) return false;
				if(el.offsetParent === null) return false;
				return true;
			}),
			engine = browser();
		els.each(function(idx, el){
			if(idx === 0)
				$(el).focus();
			if(el.type == 'text' || el.type == 'email' || el.type == 'number' || el.type == 'checkbox' || el.type == 'date')
				el.addEventListener('keypress', function (e) {
					var key = e.which || e.keyCode;
					if(key === 13 && idx < (els.length - 1))
						els[idx + 1].focus();
				});
			if(el.type == 'file')
				el.addEventListener('change', function (e) {
					els[idx + 1].focus();
				});
			if(el.type == 'textarea')
				el.addEventListener('keypress', function (e) {
					var key = e.which || e.keyCode;
					if(key === 9 && idx < (els.length - 1))
						els[idx + 1].focus();
				});
			if(el.type == 'select-one')
				el.addEventListener('keypress', function (e) {
					var key = e.which || e.keyCode;
					if(idx < (els.length - 1)){
						if(key === 13 && engine === 'Firefox')
							els[idx + 1].focus();
						else if(key === 9 && engine === 'Chrome')
							els[idx + 1].focus();
					}
				});
		});
	},
	create_find = function(options, propagation){
		let opts = {
			dom: "<'row'<'col-12 col-md-12 col-lg-6'f>>" +
				"<'row'<'col-12'<'table-responsive'tr>>>" +
				"<'row'<'col-12 col-md-6'i><'col-12 col-md-6'p>>",
			processing: true,
			serverSide: true,
			destroy: true,
			deferRender: true,
			lengthChange: false,
			sPaginationType: 'full',
			pageLength: 5,
			aaSorting: [0, 'asc'],
			initComplete: function(data){
				let table = $(this),
					tableId = table[0].id,
					modal = table.closest('div.modal'),
					search = $('#'+tableId+'_filter input[type="search"]');
					
				// Refresh Find
				modal.find('.modal-header > span').off().click(e => {
					e.preventDefault();
					if(propagation) 
						e.stopPropagation();
					table.DataTable().ajax.reload();
				});
				
				// Search Auto Focus
				modal.on('shown.bs.modal', e => search[0].focus());
				
				search.click(e => {
					if(propagation)
						e.stopPropagation();
					e.target.select();
				});
			},
			ajax: {
				type: 'POST',
				dataType: "json",
			},
			createdRow: (row, data) => {
				$(row).addClass('pointer').data(data);
			},
			drawCallback: function(){
				if(propagation){
					$(this).closest('.dataTables_wrapper').find('.paginate_button').click(e => {
						e.preventDefault();
						e.stopPropagation();
					});
				}
			}
		};
		$.extend(true, opts, options);
		return opts;
	},
	view_detail = {
		dom: "<'row'<'col-12'<'table-responsive'tr>>>" +
			"<'row'<'col-12 col-md-6'i><'col-12 col-md-6'p>>",
		processing: true,
		serverSide: true,
		filter: false,
		destroy: true,
		deferRender: true,
		lengthChange: false,
		sPaginationType: 'full',
		pageLength: 5,
		aaSorting: [0, 'asc'],
		ajax: {
			type: 'POST',
			dataType: "json",
		},
	},
	actionDetail = (status) => {
		let all_action = '', action_histori = '';
		if(is_allow('edit', acl_list))
			all_action += '<a class="btn btn-warning btn-sm detail-edit '+(status != '0' ? 'disabled' : '')+'" title="Edit Data"><i class="fa fa-edit text-white"></i></a> ';
		if(is_allow('hapus', acl_list))
			all_action += '<a class="btn btn-danger btn-sm detail-hapus '+(status != '0' ? 'disabled' : '')+'" title="Hapus Data"><span class="btn-hapus"><i class="fa fa-trash text-white"></i></span></a> ';
		action_histori = '<a class="btn btn-dark btn-sm detail-histori" title="Histori Data"><span class="btn-histori"><i class="fa fa-history text-white"></i></span></a>';
		return all_action + action_histori;
	},
	optionsToastr = {
		closeButton: true
	},
	iframe_download = (url, params) => {
		let iframe = $('#iframe-download-excel'),
			form = $('<form/>').attr({'method': 'POST', 'action': url});
		iframe.contents().find('body').empty();
		$.each(params, (key, val) => {
			switch(key){
				case 'columns':
					val.map((value, index) => {
						$.each(value, (i, v) => {
							if(i === 'search'){
								$('<input type="hidden" name="'+key+'['+index+']'+'['+i+']'+'[value]'+'" value="'+v.value+'"/>').appendTo(form);
								$('<input type="hidden" name="'+key+'['+index+']'+'['+i+']'+'[regex]'+'" value="'+v.regex+'"/>').appendTo(form);
							}else{
								$('<input type="hidden" name="'+key+'['+index+']'+'['+i+']'+'" value="'+(v === null ? '' : v)+'"/>').appendTo(form);
							}
						});
					});
					break;
				case 'order':
					val.map((value, index) => {
						$.each(value, (i, v) => {
							if(i === 'column'){
								$('<input type="hidden" name="'+key+'[0][column]'+'" value="'+val[0].column+'"/>').appendTo(form);
								$('<input type="hidden" name="'+key+'[0][dir]'+'" value="'+val[0].dir+'"/>').appendTo(form);
							}
						});
					});	
					break;
				case 'search':
					$('<input type="hidden" name="'+key+'[value]'+'" value="'+val.value+'"/>').appendTo(form);
					$('<input type="hidden" name="'+key+'[regex]'+'" value="'+val.regex+'"/>').appendTo(form);
				break;
				default:
					$('<input type="hidden" name="'+key+'" value="'+val+'"/>').appendTo(form);
					break;
			}
		});
		
		form.appendTo(iframe.contents().find('body'));
		form.submit();
	},
	eventNumbers = (validChars, ...id) => {
		let numberList = validChars || '1234567890',
			thousandSeparator = null,
			decimalSeparator = null,
			fractionDigits = 2,
			precedingChar = function(el){
				if(!el) return;
				let preced = null;
				if('selectionStart' in el)
					preced = el.selectionStart - 1;
				else if (document.selection) {
					el.focus();
					let sel = document.selection.createRange(),
						selLen = document.selection.createRange().text.length;
					sel.moveStart('character', -el.value.length);
					preced = sel.text.length - selLen - 1;
				}
				return el.value[preced];
			};
		[thousandSeparator, decimalSeparator] = ((9999).toLocaleString('En-us', {minimumFractionDigits : 1})).replace(/[0-9]/g, '').split('');
		numberList = numberList.replace(thousandSeparator, decimalSeparator);
		
		if(id.length == 0)
			return;
		let validID = id.map(v => '#'+v).join(', ');
		$(validID).on('click keypress blur keyup paste', function(e){
			if(e.type == 'click'){
				this.select();
				if(parseFloat(this.value) == 0)
					this.value = '';
			}else if(e.type == 'keypress' || e.type == 'blur'){
				if(!keyCodeRestrict(e, numberList))
					e.preventDefault();
				if(e.type == 'blur' && this.value == '')
					this.value = 0;
				if(e.target.previousElementSibling != null)
					$(e.target.previousElementSibling).trigger(e.type);
			}else if(e.type == 'keyup' || e.type == 'input' || e.type == 'paste'){
				if(!keyCodeRestrict(e, numberList))
					e.preventDefault();
				let currentValue = isNaN(a = thousandSeparator === ',' ? parseFloat((this.value).replace(/\,/g, '')) : parseFloat((this.value).replace(/\./g, '').replace(/\,/g, '.'))) ? 0 : a;
					prevChar = precedingChar(this);
					decimalSeparatorPosition = this.value.indexOf(decimalSeparator),
					hasFraction = (this.value.length - 1) > decimalSeparatorPosition && decimalSeparatorPosition >= 0;
				if(prevChar === this.value[this.value.length - 1] && prevChar == decimalSeparator && !hasFraction) 
					return;
				if(prevChar === this.value[this.value.length - 1] && prevChar == '0' && hasFraction) 
					return;
				if(prevChar === this.value[this.value.length - 1] && prevChar == '-'){
					if(this.value == '-') 
						return;
					if(this.value == '0-') 
						return this.value = '-';
				}
				if(e.target.previousElementSibling != null){
					e.target.previousElementSibling.value = currentValue;
					$(e.target.previousElementSibling).trigger(e.type);
				}
				this.value = currentValue.toLocaleString('En-us', {maximumFractionDigits : fractionDigits});
			}
		});
	},
	removeFormError = async element => {
		errorForm = {};
		await Array.from(element.find('input, select, textarea'))
			.filter(el => {
				if(el.type == 'hidden') return false;
				else if(el.readOnly) return false;
				else if(el.disabled) return false;
				else if(el.offsetParent === null) return false;
				return true;
			})
			.forEach(el => {
				if(el.parentNode.querySelector('.invalid-feedback'))
					el.parentNode.querySelector('.invalid-feedback').remove();
				el.classList.remove('is-invalid');
			});
	},
	addFormError = async element => {
		await Object.keys(errorForm).forEach(el => {
			element.find(`#${el}`).addClass('is-invalid');
			$('<div class="invalid-feedback"/>').html(errorForm[el]).appendTo(element.find(`#${el}`).parent());
		});
		if(element.find('.is-invalid').length > 0)
			element.find('.is-invalid:eq(0)').trigger('focus');
	},
	setError = (id, msg) => {
		errorForm = {...errorForm, [id]: msg};
	},
	dateDiff = (date1, date2) => {
		date1.setHours(0);
		date1.setMinutes(0, 0, 0);
		date2.setHours(0);
		date2.setMinutes(0, 0, 0);
		let hasil = Math.abs(date1.getTime() - date2.getTime());
		return parseInt(hasil / (24 * 60 * 60 * 1000), 10) + 1;
	};

$(document).ready(function() {
	/**
	 * Resize function without multiple trigger
	 * 
	 * Usage:
	 * $(window).smartresize(function(){  
	 *     // code here
	 * });
	 */
	(function($,sr){
		var debounce = function (func, threshold, execAsap) {
		  var timeout;

			return function debounced () {
				var obj = this, args = arguments;
				function delayed () {
					if (!execAsap)
						func.apply(obj, args); 
					timeout = null; 
				}
				if (timeout)
					clearTimeout(timeout);
				else if (execAsap)
					func.apply(obj, args);
				timeout = setTimeout(delayed, threshold || 100); 
			};
		};
		jQuery.fn[sr] = function(fn){return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);};
	})(jQuery,'smartresize');
	
	/** Custom Default DataTable **/
	(function($, DataTable){
		$.extend(true, DataTable.defaults, {
			language: {
				"processing": "Sedang memproses...",
				"lengthMenu": "Jumlah data _MENU_",
				"info": "Menampilkan _START_ &#45; _END_ dari _TOTAL_ data",
				"infoEmpty": "",
				"infoFiltered": "(Pencarian dari _MAX_ total data)",
				"infoPostFix": "",
				"loadingRecords": "Loading...",
				"zeroRecords": "Data tidak tersedia",
				"emptyTable": "Data tidak tersedia",
				"search": "",
				"searchPlaceholder": "Pencarian",
				"paginate": {
					"first": "&#60;&#60;",
					"previous": "&#60;",
					"next": "&#62;",
					"last": "&#62;&#62;"
				},
				"aria": {
					"sortAscending":  ": activate to sort column ascending",
					"sortDescending": ": activate to sort column descending"
				}
			}
		});
		$.extend(true, DataTable.ext.classes, {
			sFilterInput: 'form-control',
			sFilter: 'custom_filter pb-2',
			sHeaderTH: 'dt-head-center',
			sTable: 'table table-striped table-hover dataTable',
		});
		$.fn.dataTable.ext.errMode = function(params1, params2, message){
			console.error(message);
		};
	})(jQuery, jQuery.fn.dataTable);

	init_validator();
	load_menu(true);
	$('#img-avatar').tooltip();
	
	var timeout,
		nav_search_input = $('.nav-search > .nav-search-input'),
		nav_search_clear = $('.nav-search > .nav-search-clear');
		
	nav_search_input.on({
		'keyup': function(){
			if(timeout !== undefined)
				clearTimeout(timeout);
			timeout = setTimeout(function(){
				filter_menu($('.sidebar-nav > ul.nav')[0], nav_search_input.val())
			}, 500);
			if(nav_search_input.val() == '')
				nav_search_clear.addClass('d-none');
			else nav_search_clear.removeClass('d-none');
		},
		'click': function(){
			this.select();
		}
	});
	nav_search_clear.click(function(){
		nav_search_input.val('').trigger('keyup');
	});
	nav_search_input.trigger('keyup');
	
	// Panel Toolbox
	$('.collapse-link').click(function(){
		let $BOX_PANEL = $(this).closest('.card'),
			$ICON = $(this).find('i'),
			$BOX_CONTENT = $BOX_PANEL.find('.card-body');
		
		// fix for some div with hardcoded fix class
		if ($BOX_PANEL.attr('style')) {
			$BOX_CONTENT.slideToggle(200, function(){
				$BOX_PANEL.removeAttr('style');
			});
		} else {
			$BOX_CONTENT.slideToggle(200); 
			$BOX_PANEL.css('height', 'auto');  
		}
		$ICON.toggleClass('fa-chevron-up fa-chevron-down');
	});

	$('.close-link').click(function(){
		let $BOX_PANEL = $(this).closest('.card'),
			box_container = $BOX_PANEL.parent();
		if($BOX_PANEL.hasClass('widget-panel')){
			$BOX_PANEL.parent().fadeOut(300, function(){ $(this).remove();});
		}else
			$BOX_PANEL.fadeOut(300, function(){ $(this).remove();});
	});
	// End Panel Toolbox
});