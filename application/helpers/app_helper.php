<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** Format Tanggal **/
if(!function_exists('format_tanggal')){
	function format_tanggal($format = "d F Y", $date = "now", $lang = "id"){
		$en = array("SUN","MON","TUE","WED","THU","FRI","SAT","JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
		$id = array("MINGGU","SENIN","SELASA","RABU","KAMIS","JUMAT","SABTU","JANUARI","FEBRUARI","MARET","APRIL","MEI","JUNI","JULI","AGUSTUS","SEPTEMBER","OKTOBER","NOVEMBER","DESEMBER","JAN","FEB","MAR","APR","MEI","JUN","JUL","AGU","SEP","OKT","NOV","DES");
		return str_replace($en, ${$lang}, date($format, strtotime($date)));
	}
}

/** Get Nama Bulan **/
if(!function_exists('get_nama_bulan')){
	function get_nama_bulan($bulan) {
		$nama_bulan = array(1=>"JANUARI", 2=>"FEBRUARI", 3=>"MARET", 4=>"APRIL", 5=>"MEI", 6=>"JUNI", 7=>"JULI", 8=>"AGUSTUS", 9=>"SEPTEMBER", 10=>"OKTOBER", 11=>"NOVEMBER", 12=>"DESEMBER");
		return in_array($bulan, array_keys($nama_bulan)) ? $nama_bulan[$bulan] : "NO NAME";
	}
}

/** Get Option Bulan **/
if(!function_exists('get_month_option')){
	function get_month_option($json = false, $all = true) {
		$monthOptions = array(1=>"JANUARI", 2=>"FEBRUARI", 3=>"MARET", 4=>"APRIL", 5=>"MEI", 6=>"JUNI", 7=>"JULI", 8=>"AGUSTUS", 9=>"SEPTEMBER", 10=>"OKTOBER", 11=>"NOVEMBER", 12=>"DESEMBER", "-"=>"SEMUA");
		if(!$all)
			unset($monthOptions['-']);
		return $json ? json_encode($monthOptions) : $monthOptions;
	}
}

/** Get Option Tahun **/
if(!function_exists('get_year_option')){
	function get_year_option($json = false) {
		$yearOptions = array();
		foreach(range(date('Y')-3, date('Y')+1) as $y)
			$yearOptions[$y] = $y;
		return $json ? json_encode($yearOptions) : $yearOptions;
	}
}

/** Get Status **/
if(!function_exists('get_status')){
	function get_status($json = false) {
		$status = array('-'=>'SEMUA','0'=>'BARU','1'=>'AKTIF','2'=>'NON AKTIF');
		return $json ? json_encode($status) : $status;
	}
}

/** Get Status Trans **/
if(!function_exists('get_status_trans')){
	function get_status_trans($json = false) {
		$status = array('-'=>'SEMUA', '0'=>'BARU', '1'=>'APPROVE', '2'=>'BATAL');
		return $json ? json_encode($status) : $status;
	}
}

/** Get Serial Random String **/
if(!function_exists('get_random_string')){
	function get_random_string($length = 10, $chars = null) {
		$characters = isset($chars) ? $chars : "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$charactersLength = strlen($characters);
		$randomString = "";
		for($i = 0; $i < $length; $i++)
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		return $randomString;
	}
}

/** Cek if date is valid format **/
if(!function_exists('is_valid_date')){
	function is_valid_date($date) {
		try {
			$date = new DateTime(trim($date));
		}
		catch(Exception $e){
			return false;
		}
		$month = $date->format('m');
		$day = $date->format('d');
		$year = $date->format('Y');
		return checkdate($month, $day, $year);
	}
}

/** get terbilang nominal **/
if(!function_exists('terbilang')){
	function terbilang($x) {
		$x = (int) floor($x);
		$abil = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
		if($x === 0)
			return "";
		elseif ($x < 0)
			return "MINUS ".terbilang(abs($x));
		elseif ($x < 12 && $x > 0)
			return " " . $abil[$x];
		elseif ($x < 20)
			return terbilang($x - 10) . " BELAS";
		elseif ($x < 100)
			return terbilang($x / 10) . " PULUH" . terbilang($x % 10);
		elseif ($x < 200)
			return " SERATUS" . terbilang($x - 100);
		elseif ($x < 1000)
			return terbilang($x / 100) . " RATUS" . terbilang($x % 100);
		elseif ($x < 2000)
			return " SERIBU" . terbilang($x - 1000);
		elseif ($x < 1000000)
			return terbilang($x / 1000) . " RIBU" . terbilang($x % 1000);
		elseif ($x < 1000000000)
			return terbilang($x / 1000000) . " JUTA" . terbilang($x % 1000000);
		elseif ($x < 1000000000000)
			return terbilang($x / 1000000000) . " MILIYAR" . terbilang($x % 1000000000);
	}
}

/** set phone number **/
if(!function_exists('set_phone_number')){
	function set_phone_number($params){
		return preg_replace('/[^0-9;]/', '', $params);
	}
}

/** HUE to RGB format **/
if(!function_exists('hue_to_rgb')){
	function hue_to_rgb($p, $q, $t){
		if($t < 0) $t += 1;
		if($t > 1) $t -= 1;
		if($t < 1/6) return $p + ($q - $p) * 6 * $t;
		if($t < 1/2) return $q;
		if($t < 2/3) return $p + ($q - $p) * (2/3 - $t) * 6;
		return $p;
	}
}

/** HSL to RGB format **/
if(!function_exists('hsl_to_rgb')){
	function hsl_to_rgb($h, $s, $l){
		if($s == 0){
			$r = $g = $b = $l;
		}else{
			$q = $l < 0.5 ? $l * (1 + $s) : $l + $s - $l * $s;
			$p = 2 * $l - $q;
			$r = hue_to_rgb($p, $q, $h + 1/3);
			$g = hue_to_rgb($p, $q, $h);
			$b = hue_to_rgb($p, $q, $h - 1/3);
		}

		return array(round($r * 255), round($g * 255), round($b * 255));
	}
}