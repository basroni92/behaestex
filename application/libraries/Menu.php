<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu {
	protected $ci;
	protected $db;
	protected $session;
	protected $currentModule;
	protected $currentModuleId = 0;
	protected $currentMenu = '';
	protected $currentMethod;
	protected $defaultController;
	protected $bypassController;
	private $all_menus = array();
	private $all_methods = array();
	private $all_user_menus = array();
	private $tree_user_menus = array();
	
	public function __construct() {
		/* CI Instance */
		$this->ci =& get_instance();
		
		/* Load base library */
		if(!isset($this->ci->db))
			$this->ci->load->database();
		$this->db =& $this->ci->db;
		
		if(!isset($this->ci->session))
			$this->ci->load->library('session');
		$this->session = $this->ci->session;
		
		$this->defaultController = strtolower(explode("/", $this->ci->router->default_controller)[0]);
		$this->bypassController = array($this->defaultController);
		$data_bypass = $this->db->get_where("menu", ['status_menu'=>'1'])->result_array();
		foreach ($data_bypass as $key => $data)
			$this->bypassController[] = $data["file_menu"];
		$this->getController();
	}
	
	private function _load_all_menu(){
        foreach(glob(APPPATH . 'modules/*') as $module_path) {
			if(is_dir($module_path)){
                $module = basename($module_path);
				if(strtolower($module) <> strtolower(explode("/", $this->ci->router->default_controller)[0])){
					if(file_exists($module_path.'/controllers/'.ucwords($module).'.php'))
						$this->all_menus[] = $module;
					else log_message("error", "The Module {$module} has not controller's class");
				}
            }
        }
	}
	
	public function getCurrentMenu(){
		$current_module = $this->currentModule == $this->defaultController ? "Dashboard" : $this->currentModule;
		return str_replace("_", "", $current_module);
	}
	
	public function getCurrentModule(){
		return $this->currentModule;
	}
	
	public function getCurrentMethod(){
		return $this->currentMethod;
	}

	public function getCurrentModuleId(){
		return $this->currentModuleId;
	}
	
	private function getController(){
		$this->currentModule = strtolower($this->ci->router->fetch_module());
		$data_module = $this->db->get_where("menu", ['file_menu'=>$this->currentModule,'status_menu'=>'1']);
		if($data_module->num_rows() == 0){
			if(!in_array($this->currentModule, $this->bypassController))
				log_message("error", "Module {$this->currentModule} is not exist");
		}else{
			$this->currentModuleId = $data_module->row()->id;
			$this->currentMenu = $data_module->row()->nama_menu;
		}
		$this->currentMethod = $this->ci->router->fetch_method();
	}
	
	public function get_all_menus(){
		if(empty($this->all_menus))
			$this->_load_all_menu();
		return $this->all_menus;
	}
	
	public function get_all_methods($module = null){
		if(empty($this->all_methods))
			$this->_load_all_menu();
		if(!empty($module))
			return $this->all_methods[$module];
		else
			return $this->all_methods;
	}
	
	public function get_user_menus($generate = true){
		$this->user_menus();
		if(!$generate)
			return $this->tree_user_menus;
		return $this->generate_menus($this->tree_user_menus, 0);
	}
	
	private function user_menus(){
		$user = $this->db->get_where("user", ['id'=>$this->session->user['id_user'], 'status_user'=>'1']);
		$this->db
			->select("id, label, parent_id, link, icon, ok", false)
			->from("
				(
					SELECT
						a.id,
						a.nama_gmenu AS label,
						0 AS parent_id,
						'' AS link,
						a.icon_gmenu AS icon,
						0 AS ok
					FROM gmenu a
					UNION ALL
					SELECT 
						CONCAT('M', b.id) AS id,
						b.nama_menu AS label,
						d.id AS parent_id,
						b.file_menu AS link,
						d.icon_gmenu AS icon,
						1 AS ok
					FROM akses a
					INNER JOIN menu b ON b.id=a.id_menu AND b.status_menu='1'
					INNER JOIN `user` c ON c.id=a.id_user AND c.status_user='1'
					INNER JOIN gmenu d ON d.id=b.id_gmenu
					WHERE a.id_user = '".$this->session->user['id_user']."'
					GROUP BY b.id
					LIMIT 10000000000000000000
				) a")
			->order_by("a.parent_id, a.label");
		$data_menu = $this->db->get()->result_array();
		$this->all_user_menus = $data_menu;
		foreach($data_menu as $a)
			$this->to_parent($a, $this->tree_user_menus);
		$this->filter($this->tree_user_menus);
	}
	
	private function generate_menus($a, $b){
		$html = "<ul class=\"nav".($b == 0 ? "" : "-dropdown-items")."\">";
		foreach ($a as $c){
			$c['label'] = ucwords(strtolower($c['label']));
			$c['link'] = strtolower($c['link']);
			if(preg_match("/^(M)/i", $c['id'])){
				$html .= "<li class='nav-item'><a class='nav-link' href='{$c['link']}'> <i class='nav-icon icon-paper-plane'></i> {$c['label']}</a></li>";
			}else{
				$icon = $c['icon'] == "" ? "list" : strtolower($c['icon']);
				$html .= "<li class='nav-item nav-dropdown'><a class='nav-link nav-dropdown-toggle'>".($b == 0 ? " <i class='nav-icon icon-$icon'></i> " : "")."{$c['label']}</span></a>";
				$html .= $this->generate_menus($c['childs'], $c['id'])."</li>";
			}
		}
		$html .= "</ul>";
		return $html;
	}
	
	private function to_parent($a, &$b){
		if($a['parent_id'] == 0)
			$b[$a['id']] = $a;
		else{
			foreach($b as &$c){
				if($a['parent_id'] == $c['id']){
					$b[$c['id']]['childs'][$a['id']] = $a;
					break;
				}else
					if(isset($c['childs']))
						$this->to_parent($a, $c['childs']);
			}
		}
	}

	private function filter(&$a){
		foreach($a as &$b){
			$c = isset($b['childs']) ? $this->filter($b['childs']) : $b['ok'] == 1;
			if($c) $d = 1;
			else unset($a[$b['id']]);
		}
		return isset($d) / 1;
	}
}
