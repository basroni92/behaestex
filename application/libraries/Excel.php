<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');  
 
require_once APPPATH."/third_party/PHPExcel.php";

class Excel extends PHPExcel {
	public $styleTitle = array (
		'font'  => array(
			'bold'  => true,
			'underline'=> true,
			'color' => array('rgb' => '000000'),
			'size'  => 12,
			'name'  => 'Arial'
		)
	);
	public $styleOption = array (
		'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'size'  => 10,
			'name'  => 'Arial'
		)
	);
	public $styleHeader = array (
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '000000'),
			'size'  => 10,
			'name'  => 'Arial'
		),
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'horizontal' =>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'wrap' => true
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		)
	);
	public $styleData = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'FFFFFF')
		)
	);
	public $styleDataCenter = array (
		'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'horizontal' =>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'wrap' => true
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		)
	);
	public $styleCustom = array (
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '000000'),
			'size'  => 10,
			'name'  => 'Arial'
		),
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'horizontal' =>PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'wrap' => true
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		)
	);
	public $styleBoldNumberAligmentRight = array (
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '000000'),
			'size'  => 10,
			'name'  => 'Arial'
		),
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'horizontal' =>PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'wrap' => true
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		)
	);
	public $styleFooter1 = array (
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'horizontal' =>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'FFFFFF')
		)
	);
	public $styleFooter2 = array (
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'horizontal' =>PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'wrap' => true
		),
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'FFFFFF')
		)
	);
	// STYLE DETAIL	
	public $styleSpecialData = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'borders' => array(
			'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'vertical' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'FFFFFF')
		)
	);
	public $styleBlackData = array(
		'borders' => array(
			'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		)
	);
	public $styleBlackCol = array(
		'borders' => array(
			'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		)
	);
	public $styleWhiteCol = array(
		'font'  => array(
			'color' => array('rgb' => 'FFFFFF'),
			'size'  => 8,
			'name'  => 'Arial'
		)	
	);
	
    public function __construct() {
        parent::__construct();
    }
	
	public function getNameFromNumber($num) {
		$numeric = $num % 26;
		$letter = chr(65 + $numeric);
		$num2 = intval($num / 26);
		if ($num2 > 0) {
			return $this->getNameFromNumber($num2 - 1) . $letter;
		} else {
			return $letter;
		}
	}
	
	public function getAbjadFromNumber($num) {
		$numeric = ($num - 1) % 26;
		$letter = chr(65 + $numeric);
		$num2 = intval(($num - 1) / 26);
		if ($num2 > 0) {
			return $this->getAbjadFromNumber($num2) . $letter;
		} else {
			return $letter;
		}
	}
	
	public function excel_column_from_number($x, $a = "A"){
		return (($b = floor(($x + 1) / 27)) > 0 ? chr(ord($a) + $b - 1) : "") . chr(ord($a) + ($x % 26));
	}
	
	public function number_from_excel_column($x, $a = "A"){
		$x = str_split($x);
        $c = count($x);
		$an = strlen($a) > 1 ? $this->number_from_excel_column($a) : ord($a);
		return array_sum(array_map(function($y, $z) use($c, $an){
		    if($z < $c - 1)
		        return ord($y) - $an + 26;
			else return ord($y) - $an;
		}, $x, array_keys($x)));
	}
	
	public $fillColourGreen = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => '1ABB9C')
		)
	);
	
	public $fillColourOrange = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'F39C12')
		)
	);
	
	public $fillColourBlue = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => '3498DB')
		)
	);
	
	public $fillColourViolete = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => '33E6FF')
		)
	);
	
	public $fillColourPink = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'D401CD')
		)
	);
	
	public $fillColourPurple = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => '9B59B6')
		)
	);
	
	public $fillColourYellow = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'FFFF00')
		)
	);
	
	public $fillColourRed = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'E74C3C')
		)
	);
	
	public $fillColourGrey = array(
		'alignment' => array(
			'vertical' =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		),
		'font'  => array(
			'color' => array('rgb' => '000000'),
			'size'  => 8,
			'name'  => 'Arial'
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'color' => array('rgb' => '000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb' => 'c2bcbc')
		)
	);
	
}
