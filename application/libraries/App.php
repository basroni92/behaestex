<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App {
	protected $ci;
	protected $db;
	protected $session;
	protected $currentModule;
	protected $currentModuleId = 0;
	protected $currentMenu = '';
	protected $currentMethod;
	public $response;
	protected $defaultController;
	protected $bypassController;
	private $current_allow_acl = array();
	
	public function __construct() {
		/* CI Instance */
		$this->ci =& get_instance();
		$this->response =& $this->ci->response;
		
		/* Load base library */
		if(!isset($this->ci->db))
			$this->ci->load->database();
		$this->db =& $this->ci->db;
		
		if(!isset($this->ci->session))
			$this->ci->load->library('session');
		$this->session = $this->ci->session;
		
		/* Controller & Access control */
		$this->defaultController = strtolower(explode("/", $this->ci->router->default_controller)[0]);
		$this->bypassController = array($this->defaultController);
		$data_bypass = $this->db->get_where("menu", ['status_menu'=>'1'])->result_array();
		foreach ($data_bypass as $key => $data)
			$this->bypassController[] = $data["file_menu"];
		
		$this->getController();
		$this->current_authority();
	}

	public function getCurrentMenu(){
		$current_module = $this->currentModule == $this->defaultController ? "Dashboard" : $this->currentModule;
		return str_replace("_", "", $current_module);
	}
	
	public function getCurrentModule(){
		return $this->currentModule;
	}
	
	public function getCurrentMethod(){
		return $this->currentMethod;
	}

	public function getCurrentModuleId(){
		return $this->currentModuleId;
	}
	
	private function getController(){
		$this->currentModule = strtolower($this->ci->router->fetch_module());
		$data_module = $this->db->get_where('menu', ['file_menu'=>$this->currentModule, 'status_menu'=>'1']);
		if($data_module->num_rows() == 0){
			if(!in_array($this->currentModule, $this->bypassController))
				log_message("error", "Module {$this->currentModule} is not exist");
		}else{
			$this->currentModuleId = $data_module->row()->id;
			$this->currentMenu = $data_module->row()->nama_menu;
		}
		$this->currentMethod = $this->ci->router->fetch_method();
	}
	
	private function current_authority(){
		if(!in_array($this->currentModule, $this->bypassController)){
			$this->db
				->select("
					a.tampil_akses AS index, a.tambah_akses AS tambah, a.edit_akses AS edit, a.hapus_akses AS hapus, 
					a.cetak_akses AS cetak, a.download_akses AS download, a.upload_akses AS upload,
					a.approve1_akses AS approve1, a.approve2_akses AS approve2, a.approve3_akses AS approve3,
					a.approve4_akses AS approve4, a.approve5_akses AS approve5")
				->from("akses a")
				->join("user b", "b.id=a.id_user AND b.status_user='1'", "INNER")
				->join("menu c", "c.id=a.id_menu AND c.status_menu='1'", "INNER")
				->join("gmenu d", "d.id=c.id_gmenu AND d.status_gmenu='1'", "INNER")
				->where("a.id_user", $this->session->user['id_user'])
				->where("c.file_menu", $this->currentModule);
			$curr_mode = $this->db->get();
			$this->current_allow_acl = array_keys($curr_mode->row_array() ?? [], "1");
		}
	}
	
	public function get_allow_acl($json = false){
		return $json ? strtolower(json_encode($this->current_allow_acl)) : $this->current_allow_acl;
	}
	
	public function is_allow_access($access, $module = null, $id_user = null){
		if(is_null($module))
			return in_array($access, $this->current_allow_acl);
		if(empty($id_user))
			$id_user = $this->session->user['id_user'];		
		$this->db
			->select("
				a.tampil_akses AS index, a.tambah_akses AS tambah, a.edit_akses AS edit, a.hapus_akses AS hapus, 
				a.cetak_akses AS cetak, a.download_akses AS download, a.upload_akses AS upload,
				a.approve1_akses AS approve1, a.approve2_akses AS approve2, a.approve3_akses AS approve3,
				a.approve4_akses AS approve4, a.approve5_akses AS approve5")
			->from("akses a")
			->join("user b", "b.id=a.id_user AND b.status_user='1'", "INNER")
			->join("menu c", "c.id=a.id_menu AND c.status_menu='1'", "INNER")
			->join("gmenu d", "d.id=c.id_gmenu AND d.status_gmenu='1'", "INNER")
			->where("a.id_user", $id_user)
			->where("c.file_menu", $module);
		$curr_mode = $this->db->get();
		if($curr_mode->num_rows() == 0)
			return false;
		return in_array($access, array_keys($curr_mode->row_array() ?? [], "1"));
	}
	
	public function save_histori($mode, $tabel, $id, $id_user = null){
		$id_user = is_null($id_user) ? $this->session->user['id_user'] : $id_user;
		$data = array (
			'nama_table'	=> $tabel,
			'id_table'		=> $id,
			'id_user'		=> $id_user,
			'unik_riwayat'	=> md5($tabel."-".$id),
			'mode_riwayat'	=> $mode,
		);
		if(!$this->db->insert("riwayat", $data)){
			$this->db->trans_rollback();
			return $this->response->set_message("Gagal Insert Histori.")->return(true);
		}
	}
	
	public function paramGetData($offset, $page_size, $sort, $filter = null, $search = null, $suggest = null){
		if(!empty($filter)){
			foreach($filter as $col=>$val){
				if($val <> "-")
					$this->db->having($col, $val);
			}
		}
		
		if(!empty($suggest))
			return $this->db
				->having("suggest LIKE '%$suggest%'")
				->order_by($sort)
				->limit($page_size, $offset)
				->get()
				->result_array();
		else {
			if(!empty($search))
				$this->db->having("search LIKE '%$search%'");
			$this->db->order_by($sort);
			$this->db->limit($page_size, $offset);
			$data = $this->db->get();
			$count = $this->db->query("SELECT FOUND_ROWS() AS TOTAL_ROWS");
			$result = array(
				"draw"				=> (int) $this->ci->input->post('draw'),
				"recordsTotal"		=> $count->row()->TOTAL_ROWS,
				"recordsFiltered"	=> $count->row()->TOTAL_ROWS,
				"data"				=> $data->result_array()
			);
			return (object) $result;
		}
	}
}
