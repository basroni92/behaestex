<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '1024M');
 
require_once APPPATH."/third_party/FPDF/fpdf.php";

class Pdf extends FPDF {
	private $ci;
	private $module = null;
	private $headerFunction = null;
	private $headerArguments = null;
	private $footerFunction = null;
	private $footerArguments = null;
	private $validateHeader = false;
	private $validateFooter = false;
	protected $widths;
	protected $lineHeight = 5;
	protected $paddingL = 0;
	protected $paddingT = 0;
	protected $paddingR = 0;
	protected $paddingB = 0;
	protected $aligns;
	protected $bgColor = null;
	protected $bgColorR = 255;
	protected $bgColorG = 255;
	protected $bgColorB = 255;
	
	protected $T128;                                         // Tableau des codes 128
	protected $ABCset = "";                                  // jeu des caractères éligibles au C128
	protected $Aset = "";                                    // Set A du jeu des caractères éligibles
	protected $Bset = "";                                    // Set B du jeu des caractères éligibles
	protected $Cset = "";                                    // Set C du jeu des caractères éligibles
	protected $SetFrom;                                      // Convertisseur source des jeux vers le tableau
	protected $SetTo;                                        // Convertisseur destination des jeux vers le tableau
	protected $JStart = array("A"=>103, "B"=>104, "C"=>105); // Caractères de sélection de jeu au début du C128
	protected $JSwap = array("A"=>101, "B"=>100, "C"=>99);   // Caractères de changement de jeu
	
	function __construct($arguments = array("orientation"=>"P", "unit"=>"mm", "size"=>"A4")){
		$defaultArgs = array("orientation", "unit", "size");
		foreach($defaultArgs as $parameter)
			${$parameter} = null;
			
		if(!is_array($arguments))
			throw new Exception('Pdf error: Parameter harus dalam array.');
		
		foreach($arguments as $key=>$value){
			if(in_array($key, $defaultArgs))
				${$key} = $value;
		}
		parent::__construct($orientation, $unit, $size);
		$this->T128[] = array(2, 1, 2, 2, 2, 2);           //0 : [ ]               // composition des caractères
		$this->T128[] = array(2, 2, 2, 1, 2, 2);           //1 : [!]
		$this->T128[] = array(2, 2, 2, 2, 2, 1);           //2 : ["]
		$this->T128[] = array(1, 2, 1, 2, 2, 3);           //3 : [#]
		$this->T128[] = array(1, 2, 1, 3, 2, 2);           //4 : [$]
		$this->T128[] = array(1, 3, 1, 2, 2, 2);           //5 : [%]
		$this->T128[] = array(1, 2, 2, 2, 1, 3);           //6 : [&]
		$this->T128[] = array(1, 2, 2, 3, 1, 2);           //7 : [']
		$this->T128[] = array(1, 3, 2, 2, 1, 2);           //8 : [(]
		$this->T128[] = array(2, 2, 1, 2, 1, 3);           //9 : [)]
		$this->T128[] = array(2, 2, 1, 3, 1, 2);           //10 : [*]
		$this->T128[] = array(2, 3, 1, 2, 1, 2);           //11 : [+]
		$this->T128[] = array(1, 1, 2, 2, 3, 2);           //12 : [,]
		$this->T128[] = array(1, 2, 2, 1, 3, 2);           //13 : [-]
		$this->T128[] = array(1, 2, 2, 2, 3, 1);           //14 : [.]
		$this->T128[] = array(1, 1, 3, 2, 2, 2);           //15 : [/]
		$this->T128[] = array(1, 2, 3, 1, 2, 2);           //16 : [0]
		$this->T128[] = array(1, 2, 3, 2, 2, 1);           //17 : [1]
		$this->T128[] = array(2, 2, 3, 2, 1, 1);           //18 : [2]
		$this->T128[] = array(2, 2, 1, 1, 3, 2);           //19 : [3]
		$this->T128[] = array(2, 2, 1, 2, 3, 1);           //20 : [4]
		$this->T128[] = array(2, 1, 3, 2, 1, 2);           //21 : [5]
		$this->T128[] = array(2, 2, 3, 1, 1, 2);           //22 : [6]
		$this->T128[] = array(3, 1, 2, 1, 3, 1);           //23 : [7]
		$this->T128[] = array(3, 1, 1, 2, 2, 2);           //24 : [8]
		$this->T128[] = array(3, 2, 1, 1, 2, 2);           //25 : [9]
		$this->T128[] = array(3, 2, 1, 2, 2, 1);           //26 : [:]
		$this->T128[] = array(3, 1, 2, 2, 1, 2);           //27 : [;]
		$this->T128[] = array(3, 2, 2, 1, 1, 2);           //28 : [<]
		$this->T128[] = array(3, 2, 2, 2, 1, 1);           //29 : [=]
		$this->T128[] = array(2, 1, 2, 1, 2, 3);           //30 : [>]
		$this->T128[] = array(2, 1, 2, 3, 2, 1);           //31 : [?]
		$this->T128[] = array(2, 3, 2, 1, 2, 1);           //32 : [@]
		$this->T128[] = array(1, 1, 1, 3, 2, 3);           //33 : [A]
		$this->T128[] = array(1, 3, 1, 1, 2, 3);           //34 : [B]
		$this->T128[] = array(1, 3, 1, 3, 2, 1);           //35 : [C]
		$this->T128[] = array(1, 1, 2, 3, 1, 3);           //36 : [D]
		$this->T128[] = array(1, 3, 2, 1, 1, 3);           //37 : [E]
		$this->T128[] = array(1, 3, 2, 3, 1, 1);           //38 : [F]
		$this->T128[] = array(2, 1, 1, 3, 1, 3);           //39 : [G]
		$this->T128[] = array(2, 3, 1, 1, 1, 3);           //40 : [H]
		$this->T128[] = array(2, 3, 1, 3, 1, 1);           //41 : [I]
		$this->T128[] = array(1, 1, 2, 1, 3, 3);           //42 : [J]
		$this->T128[] = array(1, 1, 2, 3, 3, 1);           //43 : [K]
		$this->T128[] = array(1, 3, 2, 1, 3, 1);           //44 : [L]
		$this->T128[] = array(1, 1, 3, 1, 2, 3);           //45 : [M]
		$this->T128[] = array(1, 1, 3, 3, 2, 1);           //46 : [N]
		$this->T128[] = array(1, 3, 3, 1, 2, 1);           //47 : [O]
		$this->T128[] = array(3, 1, 3, 1, 2, 1);           //48 : [P]
		$this->T128[] = array(2, 1, 1, 3, 3, 1);           //49 : [Q]
		$this->T128[] = array(2, 3, 1, 1, 3, 1);           //50 : [R]
		$this->T128[] = array(2, 1, 3, 1, 1, 3);           //51 : [S]
		$this->T128[] = array(2, 1, 3, 3, 1, 1);           //52 : [T]
		$this->T128[] = array(2, 1, 3, 1, 3, 1);           //53 : [U]
		$this->T128[] = array(3, 1, 1, 1, 2, 3);           //54 : [V]
		$this->T128[] = array(3, 1, 1, 3, 2, 1);           //55 : [W]
		$this->T128[] = array(3, 3, 1, 1, 2, 1);           //56 : [X]
		$this->T128[] = array(3, 1, 2, 1, 1, 3);           //57 : [Y]
		$this->T128[] = array(3, 1, 2, 3, 1, 1);           //58 : [Z]
		$this->T128[] = array(3, 3, 2, 1, 1, 1);           //59 : [[]
		$this->T128[] = array(3, 1, 4, 1, 1, 1);           //60 : [\]
		$this->T128[] = array(2, 2, 1, 4, 1, 1);           //61 : []]
		$this->T128[] = array(4, 3, 1, 1, 1, 1);           //62 : [^]
		$this->T128[] = array(1, 1, 1, 2, 2, 4);           //63 : [_]
		$this->T128[] = array(1, 1, 1, 4, 2, 2);           //64 : [`]
		$this->T128[] = array(1, 2, 1, 1, 2, 4);           //65 : [a]
		$this->T128[] = array(1, 2, 1, 4, 2, 1);           //66 : [b]
		$this->T128[] = array(1, 4, 1, 1, 2, 2);           //67 : [c]
		$this->T128[] = array(1, 4, 1, 2, 2, 1);           //68 : [d]
		$this->T128[] = array(1, 1, 2, 2, 1, 4);           //69 : [e]
		$this->T128[] = array(1, 1, 2, 4, 1, 2);           //70 : [f]
		$this->T128[] = array(1, 2, 2, 1, 1, 4);           //71 : [g]
		$this->T128[] = array(1, 2, 2, 4, 1, 1);           //72 : [h]
		$this->T128[] = array(1, 4, 2, 1, 1, 2);           //73 : [i]
		$this->T128[] = array(1, 4, 2, 2, 1, 1);           //74 : [j]
		$this->T128[] = array(2, 4, 1, 2, 1, 1);           //75 : [k]
		$this->T128[] = array(2, 2, 1, 1, 1, 4);           //76 : [l]
		$this->T128[] = array(4, 1, 3, 1, 1, 1);           //77 : [m]
		$this->T128[] = array(2, 4, 1, 1, 1, 2);           //78 : [n]
		$this->T128[] = array(1, 3, 4, 1, 1, 1);           //79 : [o]
		$this->T128[] = array(1, 1, 1, 2, 4, 2);           //80 : [p]
		$this->T128[] = array(1, 2, 1, 1, 4, 2);           //81 : [q]
		$this->T128[] = array(1, 2, 1, 2, 4, 1);           //82 : [r]
		$this->T128[] = array(1, 1, 4, 2, 1, 2);           //83 : [s]
		$this->T128[] = array(1, 2, 4, 1, 1, 2);           //84 : [t]
		$this->T128[] = array(1, 2, 4, 2, 1, 1);           //85 : [u]
		$this->T128[] = array(4, 1, 1, 2, 1, 2);           //86 : [v]
		$this->T128[] = array(4, 2, 1, 1, 1, 2);           //87 : [w]
		$this->T128[] = array(4, 2, 1, 2, 1, 1);           //88 : [x]
		$this->T128[] = array(2, 1, 2, 1, 4, 1);           //89 : [y]
		$this->T128[] = array(2, 1, 4, 1, 2, 1);           //90 : [z]
		$this->T128[] = array(4, 1, 2, 1, 2, 1);           //91 : [{]
		$this->T128[] = array(1, 1, 1, 1, 4, 3);           //92 : [|]
		$this->T128[] = array(1, 1, 1, 3, 4, 1);           //93 : [}]
		$this->T128[] = array(1, 3, 1, 1, 4, 1);           //94 : [~]
		$this->T128[] = array(1, 1, 4, 1, 1, 3);           //95 : [DEL]
		$this->T128[] = array(1, 1, 4, 3, 1, 1);           //96 : [FNC3]
		$this->T128[] = array(4, 1, 1, 1, 1, 3);           //97 : [FNC2]
		$this->T128[] = array(4, 1, 1, 3, 1, 1);           //98 : [SHIFT]
		$this->T128[] = array(1, 1, 3, 1, 4, 1);           //99 : [Cswap]
		$this->T128[] = array(1, 1, 4, 1, 3, 1);           //100 : [Bswap]                
		$this->T128[] = array(3, 1, 1, 1, 4, 1);           //101 : [Aswap]
		$this->T128[] = array(4, 1, 1, 1, 3, 1);           //102 : [FNC1]
		$this->T128[] = array(2, 1, 1, 4, 1, 2);           //103 : [Astart]
		$this->T128[] = array(2, 1, 1, 2, 1, 4);           //104 : [Bstart]
		$this->T128[] = array(2, 1, 1, 2, 3, 2);           //105 : [Cstart]
		$this->T128[] = array(2, 3, 3, 1, 1, 1);           //106 : [STOP]
		$this->T128[] = array(2, 1);                       //107 : [END BAR]

		for ($i = 32; $i <= 95; $i++) {                                            // jeux de caractères
			$this->ABCset .= chr($i);
		}
		$this->Aset = $this->ABCset;
		$this->Bset = $this->ABCset;
		
		for ($i = 0; $i <= 31; $i++) {
			$this->ABCset .= chr($i);
			$this->Aset .= chr($i);
		}
		for ($i = 96; $i <= 127; $i++) {
			$this->ABCset .= chr($i);
			$this->Bset .= chr($i);
		}
		for ($i = 200; $i <= 210; $i++) {                                           // controle 128
			$this->ABCset .= chr($i);
			$this->Aset .= chr($i);
			$this->Bset .= chr($i);
		}
		$this->Cset="0123456789".chr(206);

		for ($i=0; $i<96; $i++) {                                                   // convertisseurs des jeux A & B
			@$this->SetFrom["A"] .= chr($i);
			@$this->SetFrom["B"] .= chr($i + 32);
			@$this->SetTo["A"] .= chr(($i < 32) ? $i+64 : $i-32);
			@$this->SetTo["B"] .= chr($i);
		}
		for ($i=96; $i<107; $i++) {                                                 // contrôle des jeux A & B
			@$this->SetFrom["A"] .= chr($i + 104);
			@$this->SetFrom["B"] .= chr($i + 104);
			@$this->SetTo["A"] .= chr($i);
			@$this->SetTo["B"] .= chr($i);
		}
		
		$this->ci =& get_instance();
	}
	
	function Code128($x, $y, $code, $w, $h) {
		$Aguid = "";                                                                      // Création des guides de choix ABC
		$Bguid = "";
		$Cguid = "";
		for ($i=0; $i < strlen($code); $i++) {
			$needle = substr($code,$i,1);
			$Aguid .= ((strpos($this->Aset,$needle)===false) ? "N" : "O"); 
			$Bguid .= ((strpos($this->Bset,$needle)===false) ? "N" : "O"); 
			$Cguid .= ((strpos($this->Cset,$needle)===false) ? "N" : "O");
		}

		$SminiC = "OOOO";
		$IminiC = 4;

		$crypt = "";
		while ($code > "") {
																						// BOUCLE PRINCIPALE DE CODAGE
			$i = strpos($Cguid,$SminiC);                                                // forçage du jeu C, si possible
			if ($i!==false) {
				$Aguid [$i] = "N";
				$Bguid [$i] = "N";
			}

			if (substr($Cguid,0,$IminiC) == $SminiC) {                                  // jeu C
				$crypt .= chr(($crypt > "") ? $this->JSwap["C"] : $this->JStart["C"]);  // début Cstart, sinon Cswap
				$made = strpos($Cguid,"N");                                             // étendu du set C
				if ($made === false) {
					$made = strlen($Cguid);
				}
				if (fmod($made,2)==1) {
					$made--;                                                            // seulement un nombre pair
				}
				for ($i=0; $i < $made; $i += 2) {
					$crypt .= chr(strval(substr($code,$i,2)));                          // conversion 2 par 2
				}
				$jeu = "C";
			} else {
				$madeA = strpos($Aguid,"N");                                            // étendu du set A
				if ($madeA === false) {
					$madeA = strlen($Aguid);
				}
				$madeB = strpos($Bguid,"N");                                            // étendu du set B
				if ($madeB === false) {
					$madeB = strlen($Bguid);
				}
				$made = (($madeA < $madeB) ? $madeB : $madeA );                         // étendu traitée
				$jeu = (($madeA < $madeB) ? "B" : "A" );                                // Jeu en cours

				$crypt .= chr(($crypt > "") ? $this->JSwap[$jeu] : $this->JStart[$jeu]); // début start, sinon swap

				$crypt .= strtr(substr($code, 0,$made), $this->SetFrom[$jeu], $this->SetTo[$jeu]); // conversion selon jeu

			}
			$code = substr($code,$made);                                           // raccourcir légende et guides de la zone traitée
			$Aguid = substr($Aguid,$made);
			$Bguid = substr($Bguid,$made);
			$Cguid = substr($Cguid,$made);
		}                                                                          // FIN BOUCLE PRINCIPALE

		$check = ord($crypt[0]);                                                   // calcul de la somme de contrôle
		for ($i=0; $i<strlen($crypt); $i++) {
			$check += (ord($crypt[$i]) * $i);
		}
		$check %= 103;

		$crypt .= chr($check) . chr(106) . chr(107);                               // Chaine cryptée complète

		$i = (strlen($crypt) * 11) - 8;                                            // calcul de la largeur du module
		$modul = $w/$i;
		

		for ($i=0; $i<strlen($crypt); $i++) {                                      // BOUCLE D'IMPRESSION
			$c = $this->T128[ord($crypt[$i])];
			for ($j=0; $j<count($c); $j++) {
				$this->Rect($x,$y,$c[$j]*$modul,$h,"F");
				$x += ($c[$j++]+$c[$j])*$modul;
			}
		}
		return ;
	}
	
	public function SetHeader($funcName = null, $arguments = null){
		$this->headerFunction = $funcName;
		$this->headerArguments = $arguments;
		return $this;
	}
	
	public function SetFooter($funcName = null, $arguments = null){
		$this->footerFunction = $funcName;
		$this->footerArguments = $arguments;
		return $this;
	}
	
	function SetLineHeight($height){
		$this->lineHeight = $height;
		return $this;
	}
	
	function SetHeights($h){
		$this->heights=$h;
		return $this;
	}
	
	function SetWidths($w){
		$this->widths=$w;
		return $this;
	}
	
	function SetLineDotted($black = null, $white = null){
        if(!is_null($black))
            $s = sprintf('[%.3F %.3F] 0 d',((int) $black) * $this->k, ((int) $white) * $this->k);
        else $s = '[] 0 d';
        $this->_out($s);
    }
	
	function SetPaddings($left, $top = null, $right = null, $bottom = null){
		$this->paddingL = $left;
		$this->paddingT = empty($top) ? $this->paddingL : $top;
		$this->paddingR = empty($right) ? $this->paddingL : $right;
		$this->paddingB = empty($bottom) ? $this->paddingT : $bottom;
		return $this;
	}

	function RowWidths($widths){
		$this->widths = $widths;
		return $this;
	}

	function RowBorders($borders){
		$this->borders = $borders;
		return $this;
	}

	function RowAligns($aligns){
		$this->aligns = $aligns;
		return $this;
	}

	function RowColors($colors){
		$this->colors = $colors;
		return $this;
	}

	function RowBgColors($colors){
		$this->bgColor = $colors;
		$this->bgColorR = isset($this->bgColor[0]) ? $this->bgColor[0] : $this->bgColorR;
		$this->bgColorG = isset($this->bgColor[1]) ? $this->bgColor[1] : $this->bgColorG;
		$this->bgColorB = isset($this->bgColor[2]) ? $this->bgColor[2] : $this->bgColorB;
		return $this;
	}

	function RowContents($contents){
		$lines = 0;
		foreach($contents as $key=>$cell)
			$lines = max($lines, $this->linesCount($this->widths[$key],$cell));

		$currentLineHeight = ($this->lineHeight * $lines) + $this->paddingT + $this->paddingB;
		$this->CheckPageBreak($currentLineHeight);
		
		/* Set content cells */
		foreach($contents as $key=>$cell){
			$cellWidth = $this->widths[$key];
			$cellBorder = isset($this->borders[$key]) ? $this->borders[$key] : 0;
			$cellAlign = isset($this->aligns[$key]) ? $this->aligns[$key] : 'L';
			$cellColor = isset($this->colors[$key]) ? $this->colors[$key] : '0';
			
			$x = $this->GetX();
			$y = $this->GetY();
			if($cellBorder !== 0) {
				if($cellBorder === 1)
					$this->Rect($x, $y, $cellWidth, $currentLineHeight);
				else {
					if(strpos($cellBorder, 'L') !== false)
						$this->Rect($x, $y, 0, $currentLineHeight);
					if(strpos($cellBorder, 'R') !== false)
						$this->Rect($x + $cellWidth, $y, 0, $currentLineHeight);
					if(strpos($cellBorder, 'T') !== false)
						$this->Rect($x, $y, $cellWidth, 0);
					if(strpos($cellBorder, 'B') !== false)
						$this->Rect($x, $currentLineHeight + $y, $cellWidth, 0);
				}
			}
			$this->SetFillColor($this->bgColorR, $this->bgColorG, $this->bgColorB);
			$this->SetXY($x + $this->paddingL, $y + $this->paddingT);
			$this->MultiCell(
				$cellWidth - $this->paddingL - $this->paddingR,
				$this->lineHeight,
				$cell,
				0,
				$cellAlign,
				$cellColor
			);
			$this->SetXY($x + $cellWidth, $y);
		}
		$this->Ln($currentLineHeight);
		return $this;
	}

	public function CheckPageBreak($h){
		if($this->GetY() + $h > $this->PageBreakTrigger){
			$this->AddPage($this->CurOrientation);
			return true;
		}
		return false;
	}

	public function isPageBreak($h){
		return $this->GetY() + $h > $this->PageBreakTrigger;
	}

	protected function linesCount($width, $text){
		$charWidth = $this->CurrentFont['cw'];
		if($width == 0)
			$width = $this->w - $this->rMargin - $this->x;

		$widthMax = ($width - (2 * $this->cMargin) - $this->paddingL - $this->paddingR) * 1000 / $this->FontSize;
		$text = str_replace("\r", '', $text);
		$length = strlen($text);
		$length -= ($length > 0 && $text[$length - 1] == "\n") / 1;
		
		$a = 0;
		$b = 0;
		$currentWidth = 0;
		$space = -1;
		$lines = 1;
		while($a < $length) {
			$char = $text[$a];
			if($char == "\n") {
				$a++;
				$space = -1;
				$b = $a;
				$currentWidth = 0;
				$lines ++;
				continue;
			}
			if($char == " ") $space = $a;
			
			$currentWidth += $charWidth[$char];
			if($currentWidth > $widthMax){
				if($space == -1){
					if($a == $b) $a++;
				}
				else $a = $space + 1;
				$space = -1;
				$b = $a;
				$currentWidth = 0;
				$lines++;
			}
			else $a++;
		}
		return $lines;
	}
	
	function Header(){
		if($this->PageNo() == 1){
			if(empty($this->headerFunction))
				return;
			
			$header = $this->headerFunction;
			$currentModule = $this->ci->app->getCurrentModule();
			$this->module = Modules::$registry[$currentModule];
			if(!class_exists($currentModule))
				return;
			if(method_exists($this->module, $header)){
				if(is_callable(array($this->module, $header), true)){
					if(!empty($this->headerArguments))
						$this->module->$header($this, $this->headerArguments);
					else
						$this->module->$header($this);
					$this->validateHeader = true;
				}
			}
		}
		else{
			if($this->validateHeader){
				$header = $this->headerFunction;
				if(!empty($this->headerArguments))
					$this->module->$header($this, $this->headerArguments);
				else
					$this->module->$header($this);
			}
		}
	}

	function Footer(){
		if($this->PageNo() == 1){
			if(empty($this->footerFunction))
				return;
			
			$footer = $this->footerFunction;
			$currentModule = $this->ci->app->getCurrentModule();
			$this->module = Modules::$registry[$currentModule];
			if(!class_exists($currentModule))
				return;
			if(method_exists($this->module, $footer)){
				if(is_callable(array($this->module, $footer), true)){
					if(!empty($this->footerArguments))
						$this->module->$footer($this, $this->footerArguments);
					else
						$this->module->$footer($this);
					$this->validateFooter = true;
				}
			}
		}
		else{
			if($this->validateHeader){
				$footer = $this->footerFunction;
				if(!empty($this->footerArguments))
					$this->module->$footer($this, $this->footerArguments);
				else
					$this->module->$footer($this);
			}
		}
	}
}
?>