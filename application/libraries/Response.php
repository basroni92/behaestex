<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response {
	private $format = 'json';
	private $result;
	private $message;
	private $record;
	private $records;
	private $total_records;
	private $options;
	private $callback = array();
	private $eventResult;
	private $eventClass;
	private $eventMethod;
	private $eventArgs;
	private $skip;
	
	public function __construct($result = "ERROR"){
		$this->ci =& get_instance();
		if(!empty($this->ci->response)){
			$hasil = $this->ci->response->get_evet();
			$this->set_event($hasil['eventResult'], $hasil['eventClass'], $hasil['eventMethod'], $hasil['eventArgs'], $hasil['skip']);	
		}
		$this->set_result($result);
		return $this;
	}
	
	public function set_object(){
		$this->format = 'array';
		return $this;
	}
	
	public function set_result($result = "ERROR"){
		$this->result = $result == "OK" ? "OK" : "ERROR";
		return $this;
	}
	
	public function set_message($message){
		$this->message .= $message;
		return $this;
	}
	
	public function set_record($record){
		$this->record = $record;
		return $this;
	}
	
	public function set_records($records){
		$this->records = $records;
		return $this;
	}
	
	public function set_total_records($total_records){
		$this->total_records = $total_records;
		return $this;
	}
	
	public function set_options($options){
		$this->options = $options;
		return $this;
	}
	
	public function set_callback($callback){
		$this->callback[] = $callback;
		return $this;
	}
	
	public function set_event($result = "ERROR", $eventClass, $eventMethod, $eventArgs = null, $skip = false){
		$this->eventResult = $result;
		$this->eventClass = $eventClass;
		$this->eventMethod = $eventMethod;
		$this->eventArgs = $eventArgs;
		$this->skip = $skip;
		return $this;
	}
	
	public function get_message(){
		return $this->message;
	}
	
	public function get_evet(){
		$hasil = array(
			'eventResult'	=> $this->eventResult,
			'eventClass'	=> $this->eventClass,
			'eventMethod'	=> $this->eventMethod,
			'eventArgs'		=> $this->eventArgs,
			'skip'			=> $this->skip
		);
		return $hasil;
	}
	
	public function get_record(){
		return $this->record;
	}
	
	public function return($echo = false){
		$return = new stdClass();
		$return->Result = $this->result;
		if(!empty($this->eventClass) && !empty($this->eventMethod)){
			$allowed = false;
			if(!$this->skip){
				if($this->eventResult == $this->result)
					$allowed = true;
			} else
				$allowed = true;
			
			if($allowed){
				$events = $this->eventClass;
				$method = $this->eventMethod;
				if(!class_exists($events))
					Modules::load(strtolower($events)."/".ucfirst(strtolower($events)));
				$events = Modules::$registry[strtolower($events)];
				if(method_exists($events, $method)){
					if(is_callable(array($events, $method), true)){
						if(!empty($this->eventArgs))
							$events->$method($this->eventArgs);
						else
							$events->$method();
					}
				}
			}
		}
		
		if(!empty($this->message))
			$return->Message = $this->message;
		else{
			if($this->result == "ERROR")
				$return->Message = "Undefined Message.";
		}
		if(!empty($this->record))
			$return->Record = $this->record;
		if(!empty($this->records))
			$return->Records = $this->records;
		if(!empty($this->total_records) || $this->total_records == 0)
			$return->TotalRecordCount = $this->total_records;
		if(!empty($this->options))
			$return->Options = $this->options;
		if(count($this->callback) > 0){
			if(count($this->callback) == 1) 
				$return->Callback = $this->callback[0];
			else 
				$return->Callback = $this->callback;
		}
		
		if($this->format == 'array')
			return $return;
		else{
			if($echo){
				echo json_encode($return);
				return;
			}else
				return json_encode($return);
		}
	}
}
