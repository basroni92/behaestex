<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$hook['post_system'][] = array(
    'class' => 'LogHook',
    'function' => 'queries',
    'filename' => 'LogHook.php',
    'filepath' => 'hooks'
);
?>