<?php

class AccessMiddleware {
    protected $module;
    protected $method;
    protected $ci;
	protected $session;
	private $current_acl = array();
	private $current_allow_acl = array();
	private $current_access = "";
	
	
    public function __construct($controller, $ci) {
        $this->module = $controller->router->module;
        $this->method = $controller->router->method;
        $this->ci = $ci;
		$this->current_access = explode("_", strtolower($this->ci->router->fetch_method()))[0];
		if($this->current_access === 'index')
			$this->current_access = 'tampil';
		
		if(!isset($this->ci->session))
			$this->ci->load->library('session');
		$this->session = $this->ci->session;
    }
	
    public function run(){
		if(!$this->session->has_userdata('user'))
			return $this->unauthorized();
		
		if(!isset($this->ci->db))
			$this->ci->load->library('database');
		
		/* Get module and method data */
		$moduleData = $this->ci->db->select("id, nama_menu")->get_where("menu", ['file_menu'=>$this->module,'status_menu'=>'1']);
		if($moduleData->num_rows() == 0)
			return $this->notfound();
		
		if($this->ci->db
			->select("REPLACE(COLUMN_NAME, '_akses', '') AS METHOD", false)
			->from("INFORMATION_SCHEMA.COLUMNS")
			->where("TABLE_SCHEMA", $this->ci->db->database)
			->where("TABLE_NAME", "akses")
			->like("COLUMN_NAME", "_akses", "before")
			->where("COLUMN_TYPE", "tinyint(1)")
			->order_by("ORDINAL_POSITION")
			->having("METHOD", $this->current_access)
			->get()
			->num_rows() == 0
		) return $this->notfound();
		$moduleData = $moduleData->row();
		$this->ci->db
			->select("
				a.tampil_akses AS tampil, a.tambah_akses AS tambah, a.edit_akses AS edit, a.hapus_akses AS hapus, 
				a.cetak_akses AS cetak, a.download_akses AS download, a.upload_akses AS upload,
				a.approve1_akses AS approve1, a.approve2_akses AS approve2, a.approve3_akses AS approve3,
				a.approve4_akses AS approve4, a.approve5_akses AS approve5")
			->from("akses a")
			->join("user b", "b.id=a.id_user AND b.status_user='1'", "INNER")
			->join("menu c", "c.id=a.id_menu AND c.status_menu='1'", "INNER")
			->join("gmenu d", "d.id=c.id_gmenu", "INNER")
			->where("a.id_menu", $moduleData->id)
			->where("a.id_user", $this->session->user['id_user']);
		$methodData = $this->ci->db->get();
		if($methodData->num_rows() == 0)
			return $this->forbidden();
		
		$this->current_acl = array_keys($methodData->row_array());
		$this->current_allow_acl = array_keys($methodData->row_array(), "1");
		if(in_array($this->current_access, $this->current_acl)){
			if(!in_array($this->current_access, $this->current_allow_acl))
				return $this->notfound();
		}else{
			log_message("error", "Method {$this->method} Module {$this->module} is Undefined.");
			return $this->notfound();
		}
		return true;
    }
	
	protected function unauthorized(){
		if($this->ci->input->is_ajax_request()) {
			$this->ci->output
				->set_status_header(401, 'Unauthorized')
				->set_content_type('application/json')
				->set_output(json_encode(['error'=>'Unauthorized'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			exit();
			return false;
		}
		//redirect(base_url('/login'));
	}
	
	protected function forbidden(){
		if($this->ci->input->is_ajax_request()) {
			$this->ci->output
				->set_status_header(403, 'Forbidden')
				->set_content_type('application/json')
				->set_output(json_encode(['error'=>'Forbidden'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			exit();
			return false;
		}
		//redirect(base_url());
	}
	
	protected function notfound(){
		if($this->ci->input->is_ajax_request()) {
			$this->ci->output
				->set_status_header(404, 'Not Found')
				->set_content_type('application/json')
				->set_output(json_encode(['error'=>'Unauthorized'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			exit();
			return false;
		}
		//redirect(base_url());
	}
}
