<?php

class AuthMiddleware {
    protected $controller;
    protected $ci;
	protected $session;
	
    public function __construct($controller, $ci) {
        $this->controller = $controller;
        $this->ci = $ci;
		
		if(!isset($this->ci->session))
			$this->ci->load->library('session');
		$this->session = $this->ci->session;
    }
	
    public function run(){
		if(!$this->session->has_userdata('user'))
			return $this->unauthorized();
    }
	
	protected function unauthorized(){
		if($this->ci->input->is_ajax_request()) {
			$this->ci->output
				->set_status_header(401, 'Unauthorized')
				->set_content_type('application/json')
				->set_output(json_encode(['error'=>'Unauthorized'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			exit();
			return false;
		}
		redirect(base_url('/login'));
	}
}