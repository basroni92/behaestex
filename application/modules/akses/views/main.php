<style>
	input[name="pass_user"] {
		text-transform: none;
	}
	span.nav-tab-close {
		padding: .1rem .4rem;
		border: 1px solid #e3e3e3;
		border-radius: .4rem;
		margin: -.2rem -.7rem -.5rem .5rem;
		display: inline-block;
	}
	span.nav-tab-close:hover {
		border-color: #ccc;
		background-color: #f5f5f5;
	}
	
	div.menu-list {
		float: left;
		border-radius: 2px;
		padding: 8px;
		overflow-y: auto;
		max-height: 450px;
		background-color: rgba(0, 0, 0, 0.05);
		border: 1px solid #c8ced3;
	}
	div.menu-list ul {
		-webkit-transition-duration: 1s;
		-moz-transition-duration: 1s;
		-ms-transition-duration: 1s;
		-o-transition-duration: 1s;
		transition-duration: 1s;
		list-style: none;
		padding-left: 1.5em;
		margin: 0;
	}
	div.menu-list ul.access {
		padding-top: .2em;
		padding-left: 2.8em;
	}
	div.menu-list ul > li {
		padding: 3px;
	}
	div.menu-list ul.access > li {
		display: inline-block;
		margin-right: 8px;
	}
	div.menu-list input.checkbox-menu {
		display: none;
	}
	div.menu-list input.checkbox-menu:checked ~ ul {
		display: none;
	}
	div.menu-list input ~ label {
		cursor: pointer;
		font-weight: 400;
	}
	div.menu-list input.checkbox-menu ~ label.label-menu {
		padding-left: 1em;
	}
	div.menu-list input.checkbox-select ~ label.label-select {
		position: absolute;
		margin-left: 10px;
	}
	div.menu-list input.checkbox-menu ~ label.label-menu:before {
		content: '';
		width: 0;
		height: 0;
		position: absolute;
		margin-left: -4em;
		margin-top: 0.4em;
		border-width: 6px;
		border-style: solid;
		border-radius: 0;
		border-top-color: transparent;
		border-right-color: #8693a1;
		border-bottom-color: #8693a1;
		border-left-color: transparent;
	}
	div.menu-list input.checkbox-menu:checked ~ label.label-menu:before {
		margin-left: -3.6em;
		border-width: 8px;
		border-top-color: transparent;
		border-right-color: transparent;
		border-bottom-color: transparent;
		border-left-color: #8693a1;
	}
</style>
<ul id="user-tab-navs" class="nav nav-tabs" role="tablist">
	<li class="nav-item">
		<a class="card-header nav-link active" id="user-tab" data-toggle="tab" href="#user-tab-content" role="tab" aria-controls="user-tab-content" aria-selected="true">
			<i class="fa fa-bars"></i>  <strong>Data User</strong>
		</a>
	</li>
</ul>
<div id="user-tab-contents" class="tab-content">
	<div class="tab-pane fade show active" id="user-tab-content" role="tabpanel" aria-labelledby="user-tab">
		<table id="table-view" style="width:100%"></table>
	</div>
</div>

<!-- Modal Akses -->
<div id="form-akses-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">Akses Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				<span aria-hidden="true"><i class="fa fa-close"></i></span>
			</div>
			<div class="modal-body">
				<form>
					<input type="hidden" name="id_menu" value="">
					<div class="form-group row">
						<label for="nama_user" class="col-md-12 col-lg-3">USER</label>
						<div class="col-md-12 col-lg-9">
							<input type="hidden" name="id_user" value="">
							<input type="text" name="nama_user" disabled="disabled" class="form-control disabled">
						</div>
					</div>
					<div class="form-group">
						<table id="table-user-access" style="width:100%">
							<thead>
								<tr><th class="jtable-column-header" style="width:100%;">Daftar Akses</th></tr>
							</thead>
							<tbody>
								<tr class="jtable-data-row"><td style="padding:0"><div class="menu-list col-12" /></td></tr>
							</tbody>
						</table>
					</div>	
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<button class="btn btn-warning btn-update">Update</button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Add -->

<script type="text/javascript">
	var userTabItem = $('#user-tab'),
		userTabNavs = $('#user-tab-navs'),
		userTabContents = $('#user-tab-contents'),
		userFormModal = $("#form-modal"),
		tableView = $("#table-view"),
		tableViewFilter = $("#table-view_filter"),
		formAksesModal = $("#form-akses-modal"),
		formAksesTable = $("#table-user-access"),
		current_module = '<?=base_url().$this->app->getCurrentModule()?>',
		acl_list = <?=$this->app->get_allow_acl(true);?>,
		lastIds = [],
		createTabUser = function(data){
			var navItemExist = userTabNavs.find('a[data-id="' + data.id + '"]');
			if(navItemExist.length > 0)
				return navItemExist.tab('show');
			
			var id = 'tab-' + data.id,
				navItemContainer = $('<li class="nav-item">').appendTo(userTabNavs),
				navItem = $('<a id="n-' + id + '" href="#c-' + id + '" aria-controls="c-' + id + '" data-toggle="tab" role="tab" class="card-header nav-link" data-id="' + data.id + '"><strong>Akses ' + data.nama_user + '</strong></a>').appendTo(navItemContainer),
				navItemClose = $('<span class="nav-tab-close"><i class="fa fa-close"></i></span>').appendTo(navItem),
				contentItemContainer = $('<div id="c-' + id + '" aria-labelledby="n-' + id + '" class="tab-pane fade" role="tabpanel"><table id="t-' + id + '-menu" style="width:100%"></table></div>').appendTo(userTabContents),
				tableAccessMenu = contentItemContainer.find('#t-' + id + '-menu');
			navItem.on('shown.bs.tab', e => {
				var lastId = lastIds.indexOf(data.id_user);
				if(lastId >= 0) lastIds.splice(lastId, 1);
				lastIds.push(data.id_user);
			});
			navItemClose.click(e => {
				e.stopPropagation();
				e.preventDefault();
				
				var lastId = lastIds.indexOf(data.id_user);
				if(lastId >= 0) lastIds.splice(lastId, 1);
				
				navItemContainer.remove();
				contentItemContainer.remove();
				if(lastIds.length > 0){
					var lastIdItem = lastIds[lastIds.length - 1];
						lastIdNav = userTabNavs.find('a[data-id="' + lastIdItem + '"]');
					lastIdNav.tab('show');
				}
				else userTabItem.tab('show');
				return false;
			});
			tableAccessMenu.dataTable(optionsAccess(data, 0));
			navItem.tab('show');
		},
		createListItem = function(records, type = 'menu', menu = null){
			var menu_list_item = '<ul class="' + type + '">';
			if(type == 'menu') {
				for(var i in records) {
					var record = records[i],
						randId = randString(5) + '_' + record.id,
						randId2 = randString(5) + '_' + record.id;
						menu_list_item += '<li>\
						<input type="checkbox" checked id="' + randId + '" class="checkbox-menu" />\
						<input type="checkbox" id="' + randId2 + '" class="checkbox-select" />\
						<label class="label-select" for="' + randId2 + '"></label>\
						<label class="label-menu" for="' + randId + '">' + record.label + '</label>';
					if(parseInt(record.ok) == 0)
						menu_list_item += createListItem(record.childs);
					else
						menu_list_item += createListItem(JSON.parse(record.access), 'access', record);
					menu_list_item += '</li>';
				}
			}
			else {
				menu_list_item += '<input type="hidden" name="nama_toko[' + (menu.id).replace(/^[M]/, '') + ']" value="' + menu.label + '">';
				for(var i in records) {
					var record = records[i],
						randId = randString(5) + '_' + i;
						menu_list_item += '<li>\
						<input type="checkbox" name="id_toko[' + (menu.id).replace(/^[M]/, '') + '][' + i + ']" \
							id="' + randId + '"' + (record == 1 ? ' checked' : '') + ' class="checkbox-select">\
						<label for="' + randId + '">' + i + '</label>';
					menu_list_item += '</li>';
				}
			}
			menu_list_item += '</ul>';
			return menu_list_item;
		},
		selectAccess = function(el){
			var $el = $(el);
			if($el.hasClass('indeterminate')){
				$el.removeClass('indeterminate').prop('checked', true);
				$el.siblings('ul').find('input.checkbox-select')
					.removeClass('indeterminate').prop('checked', true);
			}
			else
				$el.siblings('ul').find('input.checkbox-select')
					.prop('checked', $el.is(':checked'));
			$el = $el.parent().parent().siblings('input.checkbox-select');
			while($el.length > 0) {
				var $el_childs = $el.siblings('ul').find('input.checkbox-select'),
					$el_childs_checked = $el.siblings('ul').find('input.checkbox-select:checked');
				$el.removeClass('indeterminate');
				if ($el_childs_checked.length == 0) {
					$el.prop('indeterminate', false);
					$el.prop('checked', false);
				} else if ($el_childs.length == $el_childs_checked.length) {
					$el.prop('indeterminate', false);
					$el.prop('checked', true);
				} else {
					$el.prop('checked', false);
					$el.prop('indeterminate', true);
					$el.addClass('indeterminate');
				}
				$el = $el.parent().parent().siblings('input.checkbox-select');
			}
		},
		defaultAction = (data) => {
			let all_action = '', action_histori = '';
			if(is_allow('approve1', acl_list))
				all_action += '<a class="btn btn-primary btn-sm tombol-reset" title="Reset Password Random"><span class="btn-histori"><i class="fa fa-asterisk text-white"></i></span></a> ';
			if(is_allow('approve2', acl_list))
				all_action += '<a class="btn btn-dark btn-sm tombol-akses ' + (data.status_user != '1' ? 'disabled' : '') + '" title="Akses User"><i class="fa fa-random text-white"></i></a> ';
			action_histori = '<a class="btn btn-dark btn-sm tombol-histori" title="Histori Data"><span class="btn-histori"><i class="fa fa-history text-white"></i></span></a>';
			return all_action + action_histori;
		},
		optionsAccess = function(data) {
			var dataParent = data;
			return {
				dom: "<'col-sm-12'f>" +
					"<'row'<'col-12 col-sm-12 col-md-12'<'table-responsive'tr>>>" +
					"<'row'<'col-12 col-sm-12 col-md-3 col-lg-2'l><'col-12 col-sm-12 col-md-4 col-lg-6'i><'col-12 col-sm-12 col-md-5 col-lg-4'p>>",
				processing: true,
				serverSide: true,
				sPaginationType: 'full_numbers',
				aaSorting: [0, 'asc'],
				ajax: {
					url: current_module + '/approve2/access', 
					type: 'POST',
					data: function(data){
						let params = $.extend(true, data, {'user': dataParent.id});
						return params;
					}
				},
				initComplete: function(){},
				columns: [
					{title: 'Menu', width: '35%', data: 'nama_menu'},
					{title: 'Group Menu', width: '30%', data: 'nama_gmenu',
						render: (data, type, row) => {
							if(type === 'display')
								return '<i class="nav-icon icon-'+(row.icon_gmenu === '' ? 'list' : row.icon_gmenu.toLowerCase())+'"/>&nbsp;&nbsp;'+data;
							return data;
						}
					},
					{title: '', width: '35%', data: null, orderable: false, searchable: false, className: 'text-center', 
						render: (data, type, row) => {
							if(type === 'display') {
								return '<a class="btn btn-warning btn-sm tombol-edit' + (is_allow('approve2', acl_list) ? '' : ' disabled') + '" title="Edit Akses Menu ' + row.nama_menu + '" data-toggle="modal" data-target="#form-akses-modal"><i class="fa fa-edit text-white"></i></a>\
								<a class="btn btn-dark btn-sm tombol-histori" title="Histori Data"><i class="fa fa-history text-white"></i></a>';
							}
							return data;
						}
					}
				],
				createdRow: (row, data, index, cells) => {
					$(row).data('record', $.extend({}, dataParent, data));
					
					var colInput = $(cells).last();
					colInput.children('.tombol-histori').click(e => {
						e.preventDefault();
						show_histori(data.history_id);
					});
				}
			}
		},
		optionsView = {
			dom: "<'#row-header.row'<'col-sm-12 col-md-6'f><'col-sm-12 col-md-6 text-right refresh'>>" +
				"<'row'<'col-12 col-sm-12 col-md-12'<'table-responsive'tr>>>" +
				"<'row'<'col-12 col-sm-12 col-md-3 col-lg-2'l><'col-12 col-sm-12 col-md-4 col-lg-6'i><'col-12 col-sm-12 col-md-5 col-lg-4'p>>",
			processing: true,
			serverSide: true,
			sPaginationType: 'full_numbers',
			aaSorting: [0, 'asc'],
			ajax: {url: current_module + '/index/get_data', type: 'POST'},
			initComplete: function(){
				let api = this.api(),
					table = $(this),
					tableId = table[0].id;
				
				$('<button class="btn btn-link"><i class="fa fa-refresh"></i></button>')
					.off()
					.click(e => api.ajax.reload())
					.appendTo($('.refresh'));
					
				// Search Auto Focus
				$('#'+tableId+'_filter input[type="search"]')[0].focus();
				
				// Refresh View
				$('#refresh-view').off().on('click', function(e){
					e.preventDefault();
					api.ajax.reload();
				});
			},
			columns: [
				{title: 'USER', width: '30%', data: 'nama_user'},
				{title: 'PEGAWAI', width: '30%', data: 'nama'},
				{title: 'JABATAN', width: '20%', data: 'jabatan'},
				{title: '', data: null, orderable: false, searchable: false, className: 'text-center', 
					render: (data, type, row) => {
						return type === 'display' ? defaultAction(row) : data;
					}
				}
			],
			createdRow: (row, data, index, cells) => {
				var colInput = $(cells).last();
				colInput.children('.tombol-edit').data('record', data)
					.attr({'data-toggle': 'modal', 'data-target': '#form-modal'});
				colInput.children('.tombol-reset').click(e => {
					e.preventDefault();
					confirm_dialog(
						{url: current_module + '/approve1/reset_password', data: {id: data.id}},
						'Password user <span class="text-danger">' + data.nama_user + '</span> akan direset, apakah anda yakin ?',
						{value : 'Reset', class : 'warning'}
					);
				});
				colInput.children('.tombol-akses').click(e => {
					e.preventDefault();
					createTabUser(data);
				});
				colInput.children('.tombol-histori').click(e => {
					e.preventDefault();
					show_histori(data.history_id);
				});
			}
		};
	tableView.DataTable(optionsView);
	
	/* Form Akses */
	formAksesModal.on({
		'show.bs.modal': e => {
			var data = $(e.relatedTarget).closest('tr').data('record'),
				menu_list = formAksesModal.find('div.menu-list');
			if(!data) {
				formAksesModal.modal('toggle');
				return show_message('Data tidak valid.', 'error');
			}
			
			console.log(data)
		
			formAksesModal.find('#modal-title').html('Edit Akses Menu ' + data.nama_user);
			formAksesModal.find('[name="id_menu"]').val(data.id);
			formAksesModal.find('[name="id_user"]').val(data.id_user);
			formAksesModal.find('[name="nama_user"]').val(data.nama_user);
			
			$.ajax({
				url: current_module + '/approve2/access_list',
				type: 'POST',
				data: {user: data.id_user, menu: data.id_menu},
				dataType: 'json',
				success: function(response){
					menu_list.html(createListItem(response));
					menu_list.find('input.checkbox-select')
						.change(function(){
							selectAccess(this);
						});
					menu_list.find('input.checkbox-select:checked')
						.each(function(a, b){selectAccess(b)});
				},
				error: function(err){
					if($error_http[err.status] != undefined) $error_http[err.status]();
					else show_error(err.status + ' - ' + err.statusText, opts = {})
				}
			});
		},
		'hidden.bs.modal': e => {
			userFormModal.find('#modal-title').html('Akses Data');
			userFormModal.find('[name="id_menu"], [name="id_user"], [name="nama_user"]').val('');
		}
	});
	formAksesModal.find('.btn-update').click(e => {
		load_url(current_module + '/approve2/access_save', new FormData(formAksesModal.find('form')[0]));
	});
</script>