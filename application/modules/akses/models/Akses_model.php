<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Akses_model extends CI_Model {
	function __construct () {
        parent::__construct();
	}
	
	function getDataAccessMenu($user, $offset, $page_size, $sort, $filter = null, $search = null, $jenis = null){
		$user = (int) $user;
		$this->db
			->select("SQL_CALC_FOUND_ROWS
				a.id,
				a.nama_menu,
				b.nama_gmenu,
				b.icon_gmenu,
				a.file_menu,
				a.status_menu,
				MD5(CONCAT('user', '-', CONCAT('{$user}_menu_', a.id))) AS history_id,
				CONCAT(a.nama_menu, ' ', b.nama_gmenu, ' ', a.file_menu) AS search", false)
			->from("menu a")
			->join("gmenu b", "b.id=a.id_gmenu", "INNER")
			->join("(
				SELECT a.id_menu
				FROM (
					SELECT a.id_menu
					FROM akses a
					INNER JOIN menu b ON b.id=a.id_menu AND b.status_menu='1'
					INNER JOIN gmenu c ON c.id=b.id_gmenu
					WHERE a.id_user = {$user}
					GROUP BY a.id_menu
				) a
				GROUP BY a.id_menu
			) d", "d.id_menu=a.id", "LEFT");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getDataAccessListMenu($user, $menu) {
		$menu = (int) $menu;
		$data_menu = $this->getMenu(array("id_menu"=>$menu));
		if($data_menu->num_rows() == 0)
			return false;
		$data_menu = $data_menu->row();
		
		$query_methods = "SELECT NULL AS module, NULL AS method";
		foreach($this->getModuleAccess($data_menu->file_menu) as $method)
			$query_methods .= " UNION ALL SELECT '{$data_menu->file_menu}' AS module, '{$method}' AS method";
		
		$methods = $this->getAllMethods();
		$query_list_methods = "";
		if($methods->num_rows() > 0)
			foreach($methods->result() as $key=>$row)
				$query_list_methods .= $query_list_methods == "" ? "SELECT {$key} AS urut, '{$row->METHOD}' AS method" : " UNION ALL SELECT {$key}, '{$row->METHOD}'";
		
		return $this->db->query("
			SELECT
				a.id_toko AS id,
				a.nama_toko AS label,
				0 AS parent_id,
				'' AS icon,
				1 AS ok,
				CONCAT('{\"', GROUP_CONCAT(DISTINCT TRIM(CONCAT(a.method, '\":', (b.id_toko IS NOT NULL))) ORDER BY a.urut SEPARATOR ',\"'), '}') AS access
			FROM (
				SELECT
					IF(c.jenis_gmenu = '1', a.id_toko, 0) AS id_toko,
					IF(c.jenis_gmenu = '1', a.nama_toko, 'PABRIK') AS nama_toko,
					e.urut,
					d.method
				FROM toko a, menu b
				INNER JOIN gmenu c ON c.id_gmenu = b.id_gmenu
				INNER JOIN ({$query_methods}) d ON d.module = b.file_menu
				INNER JOIN ({$query_list_methods}) e ON e.method = d.method
				WHERE a.status_toko = '1'
					AND b.status_menu = '1'
					AND b.jenis_menu = '0'
			) a
			LEFT JOIN (
				SELECT
					a.id_toko,
					CAST(CAST(a.tampil_akses AS CHAR) AS SIGNED) AS `tampil`,
					CAST(CAST(a.tambah_akses AS CHAR) AS SIGNED) AS `tambah`,
					CAST(CAST(a.hapus_akses AS CHAR) AS SIGNED) AS `hapus`,
					CAST(CAST(a.edit_akses AS CHAR) AS SIGNED) AS `edit`,
					CAST(CAST(a.cetak_akses AS CHAR) AS SIGNED) AS `cetak`,
					CAST(CAST(a.upload_akses AS CHAR) AS SIGNED) AS `upload`,
					CAST(CAST(a.download_akses AS CHAR) AS SIGNED) AS `download`,
					CAST(CAST(a.approve1_akses AS CHAR) AS SIGNED) AS `approve1`,
					CAST(CAST(a.approve2_akses AS CHAR) AS SIGNED) AS `approve2`,
					CAST(CAST(a.approve3_akses AS CHAR) AS SIGNED) AS `approve3`,
					CAST(CAST(a.approve4_akses AS CHAR) AS SIGNED) AS `approve4`,
					CAST(CAST(a.approve5_akses AS CHAR) AS SIGNED) AS `approve5`
				FROM akses a
				INNER JOIN menu b ON b.id_menu = a.id_menu
					AND b.status_menu = '1'
				INNER JOIN gmenu c ON c.id_gmenu = b.id_gmenu
				INNER JOIN toko d ON d.id_toko = IF(c.jenis_gmenu = '0', d.id_toko, a.id_toko)
					AND d.status_toko = '1'
				WHERE a.id_user = {$user}
					AND a.id_menu = {$menu}
				GROUP BY a.id_menu, a.id_toko
			) b ON b.id_toko = a.id_toko
				AND IF(a.METHOD = 'tampil', b.`tampil` = 1,
					IF(a.METHOD = 'tambah', b.`tambah` = 1,
					IF(a.METHOD = 'hapus', b.`hapus` = 1,
					IF(a.METHOD = 'edit', b.`edit` = 1,
					IF(a.METHOD = 'cetak', b.`cetak` = 1,
					IF(a.METHOD = 'upload', b.`upload` = 1,
					IF(a.METHOD = 'download', b.`download` = 1,
					IF(a.METHOD = 'approve1', b.`approve1` = 1,
					IF(a.METHOD = 'approve2', b.`approve2` = 1,
					IF(a.METHOD = 'approve3', b.`approve3` = 1,
					IF(a.METHOD = 'approve4', b.`approve4` = 1,
					IF(a.METHOD = 'approve5', b.`approve5` = 1,
					b.`tampil` = 1))))))))))))
			GROUP BY a.id_toko
			ORDER BY a.nama_toko")->result_array();
	}
	
	function getModuleAccess($module){
		$all_methods = array();
		$module = strtolower($module);
		$module_file = ucwords($module);
		$module_path = APPPATH . "modules/{$module}/controllers/{$module_file}.php";
		if(file_exists($module_path)){
			if(!class_exists($module)) require_once $module_path;
			if(class_exists($module)) {
				$classReflection = new ReflectionClass($module);
				$publicMethods = $classReflection->getMethods(ReflectionMethod::IS_PUBLIC);
				if(!empty($publicMethods)){
					foreach($publicMethods as $method) {
						if(
							$method->name != '__construct' &&
							$method->name != 'get_instance' &&
							$method->name != $module &&
							!preg_match("/^(_)/i", $method->name)
						) {
							$methodNameArr = explode("_", $method->name);
							$methodName = $methodNameArr[0] == 'index' ? 'tampil' : $methodNameArr[0];
							$all_methods[] = $methodName;
						}
					}
				}
			}
		}
		return $all_methods;
	}
	
	function getAllMethods(){
		return $this->db
			->select("REPLACE(COLUMN_NAME, '_akses', '') AS METHOD", false)
			->from("INFORMATION_SCHEMA.COLUMNS")
			->where("TABLE_SCHEMA", $this->db->database)
			->where("TABLE_NAME", "akses")
			->like("COLUMN_NAME", "_akses", "before")
			->where("COLUMN_TYPE", "tinyint(1)")
			->order_by("ORDINAL_POSITION")
			->get();
	}
	
	function createTemporaryAccess($user, $menu) {
		if(!$this->db->simple_query("DROP TEMPORARY TABLE IF EXISTS akses_temp"))
			return false;
		return $this->db->simple_query("
			CREATE TEMPORARY TABLE IF NOT EXISTS akses_temp (
				id int(5) NOT NULL AUTO_INCREMENT,
				id_menu int(3) NOT NULL DEFAULT 0,
				id_user int(5) NOT NULL DEFAULT 0,
				id_pegawai int(5) NOT NULL DEFAULT 0,
				id_toko int(3) NOT NULL DEFAULT 0,
				tampil_akses tinyint(1) NOT NULL DEFAULT 0,
				tambah_akses tinyint(1) NOT NULL DEFAULT 0,
				hapus_akses tinyint(1) NOT NULL DEFAULT 0,
				edit_akses tinyint(1) NOT NULL DEFAULT 0,
				cetak_akses tinyint(1) NOT NULL DEFAULT 0,
				download_akses tinyint(1) NOT NULL DEFAULT 0,
				upload_akses tinyint(1) NOT NULL DEFAULT 0,
				approve1_akses tinyint(1) NOT NULL DEFAULT 0,
				approve2_akses tinyint(1) NOT NULL DEFAULT 0,
				approve3_akses tinyint(1) NOT NULL DEFAULT 0,
				approve4_akses tinyint(1) NOT NULL DEFAULT 0,
				approve5_akses tinyint(1) NOT NULL DEFAULT 0,
				PRIMARY KEY (id)
			) ENGINE=INNODB
		");
	}
	
	function insertTemporaryAccess($user, $menu, $all_methods) {
		if(count($all_methods) == 0)
			return true;
		
		$user = (int) $user;
		$menu = (int) $menu;
		$query_methods = "";
		foreach($all_methods as $id=>$arrMethods)
			foreach($arrMethods as $method=>$value)
				$query_methods .= $query_methods == "" ? "SELECT '{$id}' AS id_toko, '{$method}' AS method, {$value} AS value" : " UNION ALL SELECT '{$id}', '{$method}', {$value}";
		
		return $this->db->query("
			INSERT INTO akses_temp (
				id_toko, id_user, id_menu, tampil_akses, tambah_akses, hapus_akses, edit_akses, cetak_akses, download_akses, upload_akses, approve1_akses, approve2_akses, approve3_akses, approve4_akses, approve5_akses
			)
			SELECT
				id_toko,
				{$user} AS id_user,
				{$menu} AS id_menu,
				MAX(IF(method = 'tampil_akses', value, 0)) AS tampil_akses,
				MAX(IF(method = 'tambah_akses', value, 0)) AS tambah_akses,
				MAX(IF(method = 'hapus_akses', value, 0)) AS hapus_akses,
				MAX(IF(method = 'edit_akses', value, 0)) AS edit_akses,
				MAX(IF(method = 'cetak_akses', value, 0)) AS cetak_akses,
				MAX(IF(method = 'download_akses', value, 0)) AS download_akses,
				MAX(IF(method = 'upload_akses', value, 0)) AS upload_akses,
				MAX(IF(method = 'approve1_akses', value, 0)) AS approve1_akses,
				MAX(IF(method = 'approve2_akses', value, 0)) AS approve2_akses,
				MAX(IF(method = 'approve3_akses', value, 0)) AS approve3_akses,
				MAX(IF(method = 'approve4_akses', value, 0)) AS approve4_akses,
				MAX(IF(method = 'approve5_akses', value, 0)) AS approve5_akses
			FROM ({$query_methods}) a
			GROUP BY id_toko
		");
	}
	
	function insertNewAccess($user, $menu){
		$user = (int) $user;
		$menu = (int) $menu;
		
		$this->db->simple_query("SET SESSION sql_mode = ''");
		$insert_history = $this->db->query("
			INSERT INTO riwayat (
				unik_riwayat, mode_riwayat, nama_table, id_table, id_user
			)
			SELECT
				MD5(CONCAT('user-', d.id)) AS unik_riwayat,
				CONCAT(
					'Tambah ', IF(d.menu, CONCAT(b.nama_menu, ' '), ''), 'cabang ',
					IF(a.id_toko = 0, 'PABRIK', IFNULL(c.nama_toko, 'NO CABANG'))
				) AS mode_riwayat,
				'user' AS nama_table,
				d.id AS id_table,
				{$this->session->user['id_user']} AS id_user
			FROM (
				SELECT
					a.id_menu,
					a.id_user,
					a.id_toko
				FROM akses_temp a
				LEFT JOIN akses b ON b.id_menu = a.id_menu
					AND b.id_toko = a.id_toko
					AND b.id_user = a.id_user
				WHERE ISNULL(b.id_akses)
			) a
			INNER JOIN menu b ON b.id_menu = a.id_menu
			LEFT JOIN toko c ON c.id_toko = a.id_toko
			JOIN (
				SELECT '{$user}' AS id, TRUE AS menu UNION ALL
				SELECT '{$user}_menu_{$menu}', FALSE
			) d");
		if(!$insert_history) return false;
		
		return $this->db->query("
			INSERT INTO akses (
				id_menu, id_user, id_pegawai, id_toko, tampil_akses, tambah_akses, hapus_akses, edit_akses, cetak_akses, download_akses, upload_akses, approve1_akses, approve2_akses, approve3_akses, approve4_akses, approve5_akses
			)
			SELECT
				a.id_menu,
				a.id_user,
				b.id_pegawai,
				a.id_toko,
				a.tampil_akses,
				a.tambah_akses,
				a.hapus_akses,
				a.edit_akses,
				a.cetak_akses,
				a.download_akses,
				a.upload_akses,
				a.approve1_akses,
				a.approve2_akses,
				a.approve3_akses,
				a.approve4_akses,
				a.approve5_akses
			FROM akses_temp a
			INNER JOIN `user` b ON b.id_user = a.id_user
			LEFT JOIN akses c ON c.id_menu = a.id_menu
				AND c.id_toko = a.id_toko
				AND c.id_user = a.id_user
			WHERE ISNULL(c.id_akses)");
	}
	
	function updateCurrentAccess($user, $menu) {
		$user = (int) $user;
		$menu = (int) $menu;
		
		$this->db->simple_query("SET SESSION sql_mode = ''");
		$insert_history = $this->db->query("
			INSERT INTO riwayat (
				unik_riwayat, mode_riwayat, nama_table, id_table, id_user
			)
			SELECT
				MD5(CONCAT('user-', d.id)) AS unik_riwayat,
				CONCAT(
					'Update ', IF(d.menu, CONCAT(b.nama_menu, ' '), ''), 'cabang ',
					IF(a.id_toko = 0, 'PABRIK', IFNULL(c.nama_toko, 'NO CABANG')),
					' [', TRIM(BOTH ',' FROM CONCAT(a.tampil_label, a.tambah_label, a.hapus_label, a.edit_label, a.cetak_label, a.download_label, a.upload_label, a.approve1_label, a.approve2_label, a.approve3_label, a.approve4_label, a.approve5_label)), ']'
				) AS mode_riwayat,
				'user' AS nama_table,
				d.id AS id_table,
				{$this->session->user['id_user']} AS id_user
			FROM (
				SELECT
					a.id_menu,
					a.id_user,
					a.id_toko,
					IF(a.tampil_akses <> b.tampil_akses, 'tampil,', '') AS tampil_label,
					IF(a.tambah_akses <> b.tambah_akses, 'tambah,', '') AS tambah_label,
					IF(a.hapus_akses <> b.hapus_akses, 'hapus,', '') AS hapus_label,
					IF(a.edit_akses <> b.edit_akses, 'edit,', '') AS edit_label,
					IF(a.cetak_akses <> b.cetak_akses, 'cetak,', '') AS cetak_label,
					IF(a.download_akses <> b.download_akses, 'download,', '') AS download_label,
					IF(a.upload_akses <> b.upload_akses, 'upload,', '') AS upload_label,
					IF(a.approve1_akses <> b.approve1_akses, 'approve1,', '') AS approve1_label,
					IF(a.approve2_akses <> b.approve2_akses, 'approve2,', '') AS approve2_label,
					IF(a.approve3_akses <> b.approve3_akses, 'approve3,', '') AS approve3_label,
					IF(a.approve4_akses <> b.approve4_akses, 'approve4,', '') AS approve4_label,
					IF(a.approve5_akses <> b.approve5_akses, 'approve5,', '') AS approve5_label
				FROM akses a
				INNER JOIN akses_temp b ON b.id_menu = a.id_menu
					AND b.id_toko = a.id_toko
					AND b.id_user = a.id_user
				WHERE a.tampil_akses <> b.tampil_akses
					OR a.tambah_akses <> b.tambah_akses
					OR a.hapus_akses <> b.hapus_akses
					OR a.edit_akses <> b.edit_akses
					OR a.cetak_akses <> b.cetak_akses
					OR a.download_akses <> b.download_akses
					OR a.upload_akses <> b.upload_akses
					OR a.approve1_akses <> b.approve1_akses
					OR a.approve2_akses <> b.approve2_akses
					OR a.approve3_akses <> b.approve3_akses
					OR a.approve4_akses <> b.approve4_akses
					OR a.approve5_akses <> b.approve5_akses
			) a
			INNER JOIN menu b ON b.id_menu = a.id_menu
			LEFT JOIN toko c ON c.id_toko = a.id_toko
			JOIN (
				SELECT '{$user}' AS id, TRUE AS menu UNION ALL
				SELECT '{$user}_menu_{$menu}', FALSE
			) d");
		if(!$insert_history) return false;
		
		return $this->db->query("
			UPDATE akses a
			INNER JOIN akses_temp b ON b.id_menu = a.id_menu
				AND b.id_toko = a.id_toko
				AND b.id_user = a.id_user
			SET a.tampil_akses = b.tampil_akses,
				a.tambah_akses = b.tambah_akses,
				a.hapus_akses = b.hapus_akses,
				a.edit_akses = b.edit_akses,
				a.cetak_akses = b.cetak_akses,
				a.download_akses = b.download_akses,
				a.upload_akses = b.upload_akses,
				a.approve1_akses = b.approve1_akses,
				a.approve2_akses = b.approve2_akses,
				a.approve3_akses = b.approve3_akses,
				a.approve4_akses = b.approve4_akses,
				a.approve5_akses = b.approve5_akses
			WHERE a.tampil_akses <> b.tampil_akses
				OR a.tambah_akses <> b.tambah_akses
				OR a.hapus_akses <> b.hapus_akses
				OR a.edit_akses <> b.edit_akses
				OR a.cetak_akses <> b.cetak_akses
				OR a.download_akses <> b.download_akses
				OR a.upload_akses <> b.upload_akses
				OR a.approve1_akses <> b.approve1_akses
				OR a.approve2_akses <> b.approve2_akses
				OR a.approve3_akses <> b.approve3_akses
				OR a.approve4_akses <> b.approve4_akses
				OR a.approve5_akses <> b.approve5_akses");
	}
	
	function deleteCurrentAccess($user, $menu, $group = "TOKO") {
		$user = (int) $user;
		$menu = (int) $menu;
		$group = $group == "TOKO" ? "TOKO" : "MENU";
		$item_name = $group == "TOKO" ? "MENU" : "TOKO";
		
		$this->db->simple_query("SET SESSION sql_mode = ''");
		$insert_history = $this->db->query("
			INSERT INTO riwayat (
				unik_riwayat, mode_riwayat, nama_table, id_table, id_user
			)
			SELECT
				MD5(CONCAT('user-', d.id)) AS unik_riwayat,
				CONCAT(
					'Hapus ', IF(d.menu, CONCAT(b.nama_menu, ' '), ''), 'cabang ',
					IF(a.id_toko = 0, 'PABRIK', IFNULL(c.nama_toko, 'NO CABANG'))
				) AS mode_riwayat,
				'user' AS nama_table,
				d.id AS id_table,
				{$this->session->user['id_user']} AS id_user
			FROM (
				SELECT
					a.id_menu,
					a.id_user,
					a.id_toko
				FROM akses a
				LEFT JOIN akses_temp b ON b.id_menu = a.id_menu
					AND b.id_toko = a.id_toko
					AND b.id_user = a.id_user
				WHERE a.id_user = {$user}
					AND a.id_menu = {$menu}
					AND ISNULL(b.id)
			) a
			INNER JOIN menu b ON b.id_menu = a.id_menu
			LEFT JOIN toko c ON c.id_toko = a.id_toko
			JOIN (
				SELECT '{$user}' AS id, TRUE AS menu UNION ALL
				SELECT '{$user}_menu_{$menu}', FALSE
			) d");
		if(!$insert_history) return false;
		
		return $this->db->query("
			DELETE a.*
			FROM akses a
			LEFT JOIN akses_temp b ON b.id_menu = a.id_menu
				AND b.id_toko = a.id_toko
				AND b.id_user = a.id_user
			WHERE a.id_user = {$user}
				AND a.id_menu = {$menu}
				AND ISNULL(b.id)");
	}
}
