<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Akses extends MX_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Akses_model', 'model');
		$this->load->model('user/User_model', 'user');
    }
	
	protected function _middleware(){
        return array('auth', 'access');
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    public function index($args = null){
		# Data Parameters
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[$order][data]")." ".$order_by;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		
		switch($args[0]){
			case 'get_data':
				$filter['status_user'] = '1';
				$data_result = $this->user->getAllData($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			default:
				$this->load->view("main");
		}
	}
	
	public function approve1($args = null){
		$id = (int) $this->input->post('id');
		
		# Cek Status
		$rs_data = $this->user->getRowData(['a.id'=>$id]);
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$rs_data = $rs_data->row();
		
		if($rs_data->status_user == "2")
			return $this->response->set_message("User Sudah Non Aktif.")->return(true);
		
		switch($args[0]){
			case 'reset_password':
				$this->db->trans_begin();
				$newPassword = get_random_string(8);
				if(!$this->user->resetPasswordUser($id, $newPassword)){
					$this->db->trans_rollback();
					return $this->response->set_message("Error Reset Password User.")->return(true);
				}
				# Histori
				$this->app->save_histori('RESET PASSWORD', 'user', $id);
				
				$this->db->trans_commit();
				$this->response
					->set_result('OK')
					->set_message("User <b>{$rs_data->nama_user}</b> password berhasil direset <br> [New Password : $newPassword].")
					->set_callback("                                    
						$('#main-dialog-confirm').modal('toggle');
						tableView.DataTable().ajax.reload(null, false);
					")
					->return(true);
				return;
				break;
			default:
				return $this->response->set_message("Parameter Tidak Valid.")->return(true);
		}
	}
	
	public function approve2($args = null) {
		# Data Parameters
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[{$order}][data]") . " " . $order_by;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		
		switch($args[0]){
			case 'access':
				$user = (int) $this->input->post('user');
				$jenis = (string) (int) $this->input->post('jenis');
				$filter['status_menu'] = "1";
				$data_result = $this->model->getDataAccessMenu($user, $offset, $page_size, $sort, $filter, $search, $jenis);
				echo json_encode($data_result);
				break;
			case 'access_list':
				$user = (int) $this->input->post('user');
				$menu = (int) $this->input->post('menu');
				
				if(!(bool) $user)
					return $this->response->set_message("Unknown user.")->return(true);
				
				if(!(bool) $menu)
					return $this->response->set_message("Unknown menu.")->return(true);
				
				$data_result = $this->model->getDataAccessListMenu($user, $menu);
				echo json_encode($data_result);
				break;
			case 'access_save':
				$id_menu = (int) $this->input->post("id_menu");
				$id_user = (int) $this->input->post("id_user");
				
				$id_toko = (array) $this->input->post("id_toko");
				$nama_toko = (array) $this->input->post("nama_toko");
				
				/* Cek user */
				if(!(bool) $id_user)
					return $this->response->set_message("Unknown user id.")->return(true);
				else {
					$data_user = $this->model->getRowData(['id_user'=>$id_user]);
					if($data_user->num_rows() == 0)
						return $this->response->set_message("User tidak valid.")->return(true);
					$data_user = $data_user->row();
					
					if($data_user->status_user == "0")
						return $this->response->set_message("User Belum Aktif.")->return(true);
					if($data_user->status_user == "2")
						return $this->response->set_message("User Sudah Non Aktif.")->return(true);
					$nama_user = $data_user->nama_user;
				}
				
				/* cek menu */
				if(!(bool) $id_menu)
					return $this->response->set_message("Unknown menu id.")->return(true);
				else {
					$data_menu = $this->model->getMenu(array("id_menu"=>$id_menu));
					if($data_menu->num_rows() == 0)
						return $this->response->set_message("Menu tidak valid.")->return(true);
					$data_menu = $data_menu->row();
					
					if($data_menu->status_menu == "0")
						return $this->response->set_message("Menu Belum Aktif.")->return(true);
					if($data_menu->status_menu == "2")
						return $this->response->set_message("Menu Sudah Non Aktif.")->return(true);
					$nama_menu = $data_menu->nama_menu;
					
					$data_gmenu = $this->model->getGMenu(array("id_gmenu"=>$data_menu->id_gmenu));
					if($data_gmenu->num_rows() == 0)
						return $this->response->set_message("Group menu tidak valid.")->return(true);
					$data_gmenu = $data_gmenu->row();
				}
				
				/* Cek toko */
				$id_item_arr = array_keys($id_toko);
				$curr_toko = array_intersect_key($nama_toko, array_flip($id_item_arr));
				
				$all_toko = $this->app->form_filter_cabang();
				$data_toko = array_intersect_key($all_toko, array_flip($id_item_arr));
				if(!(bool) $data_gmenu->jenis_gmenu)
					$data_toko = array(0 => "PABRIK");
				
				if(count($curr_toko) <> count($data_toko)){
					$not_valid_toko = array_diff_key($curr_toko, $data_toko);
					if(count($not_valid_toko) > 0)
						return $this->response->set_message("Terdapat toko tidak valid. [" . implode(", ", $not_valid_toko) . "]")->return(true);
				}
				
				/* Cek Methods */
				$not_valid_methods = array();
				$data_all_access = array();
				$all_methods = array_column($this->model->getAllMethods()->result_array(), "METHOD");
				foreach($id_toko as $id=>$methods) {
					$methods = array_keys($methods);
					$not_valid_menu_methods = array();
					$menu_methods = $this->model->getModuleAccess($data_menu->file_menu);
					foreach($all_methods as $method)
						$data_all_access[$id]["{$method}_akses"] = (int) in_array($method, $methods);
					foreach($methods as $method)
						if(!in_array($method, $menu_methods) || !in_array($method, $all_methods))
							$not_valid_menu_methods[] = $method;
					if(count($not_valid_menu_methods) > 0)
						$not_valid_methods[] = $data_toko[$id] . " [" . implode(", ", $not_valid_menu_methods) . "]";
				}
				if(count($not_valid_methods) > 0)
					return $this->response->set_message("Terdapat method tidak valid." . implode(", ", $not_valid_methods))->return(true);
				
				/* Update */
				$this->db->trans_begin();
				if(!$this->model->createTemporaryAccess($id_user, $id_menu)){
					$this->db->trans_rollback();
					$this->response->set_message("Error prepare temporary access.")->return(true);
					return;
				}
				if(!$this->model->insertTemporaryAccess($id_user, $id_menu, $data_all_access)){
					$this->db->trans_rollback();
					$this->response->set_message("Error insert temporary access.")->return(true);
					return;
				}
				if(!$this->model->insertNewAccess($id_user, $id_menu)){
					$this->db->trans_rollback();
					$this->response->set_message("Error insert new access.")->return(true);
					return;
				}
				if(!$this->model->updateCurrentAccess($id_user, $id_menu)){
					$this->db->trans_rollback();
					$this->response->set_message("Error update current access.")->return(true);
					return;
				}
				if(!$this->model->deleteCurrentAccess($id_user, $id_menu)){
					$this->db->trans_rollback();
					$this->response->set_message("Error delete current access.")->return(true);
					return;
				}
				$this->db->trans_commit();
				$this->response
					->set_result("OK")
					->set_message("Akses pengguna berhasil diperbaharui")
					->set_callback("
						formAksesModal.modal('toggle');
						$(\"#t-tab-{$id_user}-menu\").DataTable().ajax.reload();
						$(\"#t-tab-{$id_user}-widget\").DataTable().ajax.reload();
						$(\"#t-tab-{$id_user}-service\").DataTable().ajax.reload();
					")
					->return(true);
				break;
			default:
				return $this->response->set_message("Parameter Tidak Valid.")->return(true);
		}
	}
}
