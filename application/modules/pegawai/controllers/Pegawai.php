<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Pegawai extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('Pegawai_model', 'model');
    }
	
	protected function _middleware(){
        return array('auth', 'access');
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    public function index($args = null){
		# Data Parameters
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[$order][data]")." ".$order_by;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		
		switch($args[0]){
			case 'get_data':
				$data_result = $this->model->getAllData($offset, $page_size, $sort, null, $search);
				echo json_encode($data_result);
				break;
			default:
				$this->load->view("main");
		}
    }
	
	public function tambah($args = null){
		$nama 			= strtoupper(trim($this->input->post("nama")));
		$tempat_lahir 	= strtoupper(trim($this->input->post("tempat_lahir")));
		$tanggal_lahir 	= trim($this->input->post("tanggal_lahir"));
		$jenis_kelamin 	= trim($this->input->post("jenis_kelamin"));
		$jabatan 		= strtoupper(trim($this->input->post("jabatan")));
		$gaji_pokok 	= (double) $this->input->post("gaji_pokok");
		$tunjangan 		= (double) $this->input->post("tunjangan");
		
		if($nama == '')
			return $this->response->set_message("Nama Pegawai Kosong.")->return(true);
		
		if($tempat_lahir == '')
			return $this->response->set_message("Tempat Lahir Kosong.")->return(true);
		
		if($tanggal_lahir == '')
			return $this->response->set_message("Tanggal Lahir Kosong.")->return(true);
		$tanggal_lahir = date("Y-m-d", strtotime($tanggal_lahir));
		
		if($jenis_kelamin == '')
			return $this->response->set_message("Jenis Kelamin Kosong.")->return(true);
		if(!in_array($jenis_kelamin, ['0', '1']))
			return $this->response->set_message("Jenis Kelamin Tidak Valid [0 / 1].")->return(true);
		
		if($jabatan == '')
			return $this->response->set_message("Jabatan Kosong.")->return(true);
		
		if($gaji_pokok == 0)
			return $this->response->set_message("Gaji Pokok Kosong.")->return(true);
		
		if($gaji_pokok < 0)
			return $this->response->set_message("Gaji Pokok Minus.")->return(true);
		
		if($tunjangan < 0)
			return $this->response->set_message("Tunjangan Minus.")->return(true);
		
		$this->db->trans_begin();
		$data = compact('nama','tempat_lahir','tanggal_lahir','jenis_kelamin','jabatan','gaji_pokok','tunjangan');
		if(!$this->model->addData($data)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Insert Pegawai.")->return(true);
		}
		$id = $this->db->insert_id();
		$this->app->save_histori('ADD PEGAWAI', 'pegawai', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Pegawai Berhasil Ditambahkan.")
			->set_callback("
				$('#form-modal').modal('toggle');
				tableView.ajax.reload();
			")
			->return(true);
		return;
	}
	
    public function edit($args = null){
		$id 			= (int) $this->input->post('id');
		$nama 			= strtoupper(trim($this->input->post("nama")));
		$tempat_lahir 	= strtoupper(trim($this->input->post("tempat_lahir")));
		$tanggal_lahir 	= trim($this->input->post("tanggal_lahir"));
		$jenis_kelamin 	= trim($this->input->post("jenis_kelamin"));
		$jabatan 		= strtoupper(trim($this->input->post("jabatan")));
		$gaji_pokok 	= (double) $this->input->post("gaji_pokok");
		$tunjangan 		= (double) $this->input->post("tunjangan");
		
		# Cek Status
		$rs_data = $this->model->getRowData(compact('id'));
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		
		if($nama == '')
			return $this->response->set_message("Nama Pegawai Kosong.")->return(true);
		
		if($tempat_lahir == '')
			return $this->response->set_message("Tempat Lahir Kosong.")->return(true);
		
		if($tanggal_lahir == '')
			return $this->response->set_message("Tanggal Lahir Kosong.")->return(true);
		$tanggal_lahir = date("Y-m-d", strtotime($tanggal_lahir));
		
		if($jenis_kelamin == '')
			return $this->response->set_message("Jenis Kelamin Kosong.")->return(true);
		if(!in_array($jenis_kelamin, ['0', '1']))
			return $this->response->set_message("Jenis Kelamin Tidak Valid [0 / 1].")->return(true);
		
		if($jabatan == '')
			return $this->response->set_message("Jabatan Kosong.")->return(true);
		
		if($gaji_pokok == 0)
			return $this->response->set_message("Gaji Pokok Kosong.")->return(true);
		
		if($gaji_pokok < 0)
			return $this->response->set_message("Gaji Pokok Minus.")->return(true);
		
		if($tunjangan < 0)
			return $this->response->set_message("Tunjangan Minus.")->return(true);
		
		$this->db->trans_begin();
		$data = compact('nama','tempat_lahir','tanggal_lahir','jenis_kelamin','jabatan','gaji_pokok','tunjangan');
		if(!$this->model->editData($data, $id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Update Pegawai.")->return(true);
		}
		$this->app->save_histori('EDIT PEGAWAI', 'pegawai', $id);
		$this->db->trans_commit();
		
		$this->response
			->set_result('OK')
			->set_message("Pegawai Berhasil Diperbaharui.")
			->set_callback("
				$('#form-modal').modal('toggle');
				tableView.ajax.reload(null, false);
			")
			->return(true);
		return;
	}
	
	public function hapus($args = null){
		$id = (int) $this->input->post('id');
		
		# Cek Status
		$rs_data = $this->model->getRowData(compact('id'));
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		
		$this->db->trans_begin();
		if(!$this->model->hapusData($id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Delete Pegawai.")->return(true);
		}
		# Histori
		$this->app->save_histori('DEL PEGAWAI', 'pegawai', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Pegawai Berhasil Di Hapus.")
			->set_callback("                                    
				$('#main-dialog-confirm').modal('toggle');
				tableView.ajax.reload(null, false);
			")
			->return(true);
		return;
	}	
}
