<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Pegawai_model extends CI_Model {
	function __construct () {
        parent::__construct();
	}
	
	function getAllData($offset, $page_size, $sort, $filter = null, $search = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS
				a.id,
				a.nama,
				a.tempat_lahir,
				a.tanggal_lahir,
				a.jenis_kelamin,
				IF(a.jenis_kelamin = '0', 'PEREMPUAN', 'LAKI - LAKI') AS jk,
				a.jabatan,
				a.gaji_pokok,
				a.tunjangan,
				CONCAT(
					a.nama, ' ',
					a.tempat_lahir,  ' ',
					a.tanggal_lahir, ' ',
					DATE_FORMAT(a.tanggal_lahir, '%d-%m-%Y'), ' ',
					IF(a.jenis_kelamin = '0', 'Perempuan', 'LAKI - LAKI'), ' ',
					a.jabatan
				) AS search,
				MD5(CONCAT('pegawai', '-', a.id)) AS history_id", false)
			->from("pegawai a");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getRowData($filters = null){
		$this->db->from("pegawai a");
		if(!empty($filters)){
			foreach($filters as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->order_by('a.nama')->get();
	}
	
	function addData($data){
		return $this->db->insert('pegawai', $data);
	}
	
	function editData($data, $id){
		return $this->db->where("id", $id)->update('pegawai', $data);
	}	
	
	function hapusData($id){
		return $this->db->delete('pegawai', compact('id'));
	}
}
