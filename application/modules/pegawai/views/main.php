<div class="card">
	<div class="card-header">
		<i class="fa fa-bars"></i>  <strong>Data Pegawai</strong>
		<div class="btn-group float-right show">
			<span><a id="refresh-view" class="card-header-action btn" title="Muat Ulang"><i class="fa fa-refresh"></i></a></span>
		</div>
	</div>
	<div class="card-body">
		<table id="table-view" style="width:100%"></table>
	</div>
</div>

<!-- Modal Add -->
<div id="form-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="FormGroupMenu" aria-hidden="true">
	<div class="modal-dialog modal-success modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">Tambah Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				<span aria-hidden="true"><i class="fa fa-close"></i></span>
			</div>
			<div class="modal-body">
				<form id="form-tambah" class="">
					<div class="form-group row">
						<label for="nama" class="col-md-12 col-lg-3">NAMA</label>
						<div class="col-md-12 col-lg-9">
							<input type="hidden" id="id" name="id">
							<input type="text" id="nama" name="nama" maxlength="20" onclick="this.select()" class="form-control" placeholder="NAMA PEGAWAI">
						</div>
					</div>
					<div class="form-group row">
						<label for="tempat_lahir" class="col-md-12 col-lg-3">TEMPAT LAHIR</label>
						<div class="col-md-12 col-lg-9">
							<input type="text" id="tempat_lahir" name="tempat_lahir" maxlength="20" onclick="this.select()" class="form-control" placeholder="TEMPAT LAHIR">
						</div>
					</div>
					<div class="form-group row">
						<label for="tanggal_lahir" class="col-md-12 col-lg-3">TANGGAL LAHIR</label>
						<div class="col-md-12 col-lg-9">
							<input type="date" id="tanggal_lahir" name="tanggal_lahir" value="<?=date('Y-m-d');?>" maxlength="10" onclick="this.select()" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="jenis_kelamin" class="col-md-12 col-lg-3">JENIS KELAMIN</label>
						<div class="col-md-12 col-lg-9">
							<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
								<option value=""> - - - </option>
								<option value="0">PEREMPUAN</option>
								<option value="1">LAKI - LAKI</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="jabatan" class="col-md-12 col-lg-3">JABATAN</label>
						<div class="col-md-12 col-lg-9">
							<input type="text" id="jabatan" name="jabatan" maxlength="20" onclick="this.select()" class="form-control" placeholder="JABATAN">
						</div>
					</div>
					<div class="form-group row">
						<label for="gaji_pokok" class="col-md-12 col-lg-3">GAJI POKOK</label>
						<div class="col-md-12 col-lg-9">
							<input type="hidden" id="gaji_pokok" name="gaji_pokok">
							<input type="text" id="edit_gaji_pokok" maxlength="20" class="form-control text-right" onwheel="this.blur()">
						</div>
					</div>
					<div class="form-group row">
						<label for="tunjangan" class="col-md-12 col-lg-3">TUNJANGAN</label>
						<div class="col-md-12 col-lg-9">
							<input type="hidden" id="tunjangan" name="tunjangan">
							<input type="text" id="edit_tunjangan" maxlength="20" class="form-control text-right" onwheel="this.blur()">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-success" id="btn-simpan">Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Add -->

<script type="text/javascript">
	var current_module = '<?=base_url().$this->app->getCurrentModule()?>',
		acl_list = <?=$this->app->get_allow_acl(true);?>,
		defaultAction = (data) => {
			let all_action = '', action_histori = '';
			if(is_allow('edit', acl_list))
				all_action += '<a class="btn btn-warning btn-sm tombol-edit" title="Edit Data"><i class="fa fa-edit text-white"></i></a> ';
			if(is_allow('hapus', acl_list))
				all_action += '<a class="btn btn-danger btn-sm tombol-hapus" title="Hapus Data"><span class="btn-hapus"><i class="fa fa-trash text-white"></i></span></a> ';
			action_histori = '<a class="btn btn-dark btn-sm tombol-histori" title="Histori Data"><span class="btn-histori"><i class="fa fa-history text-white"></i></span></a>';
			return all_action + action_histori;
		},
		optionsView = {
			dom: "<'#row-header.row'<'col-sm-12 col-md-3 col-lg-2'><'col-sm-12 col-md-3 col-lg-5'><'col-sm-12 col-md-6 col-lg-5'f>>" +
				"<'row'<'col-12 col-sm-12 col-md-12'<'table-responsive'tr>>>" +
				"<'row'<'col-12 col-sm-12 col-md-3 col-lg-2'l><'col-12 col-sm-12 col-md-4 col-lg-6'i><'col-12 col-sm-12 col-md-5 col-lg-4'p>>",
			processing: true,
			serverSide: true,
			sPaginationType: 'full_numbers',
			aaSorting: [0, 'asc'],
			ajax: {url: current_module+'/index/get_data', type: 'POST'},
			initComplete: function(){
				if(is_allow('tambah', acl_list)){
					let button_add = $("button[data-target='#form-modal']");
					if(button_add.length > 0)
						button_add.remove();
					$('<button/>')
						.addClass('btn btn-success btn-block')
						.attr({'data-toggle':'modal', 'data-target':'#form-modal'})
						.html('<i class="fa fa-plus"></i> Tambah Data')
						.appendTo($('#row-header > div:first'));
				}
				let table = $(this),
					tableId = table[0].id;
					
				// Search Auto Focus
				$('#'+tableId+'_filter input[type="search"]')[0].focus();
				
				// Refresh View
				$('#refresh-view').off().on('click', function(e){
					e.preventDefault();
					table.DataTable().ajax.reload();
				});
			},
			columns: [
				{title: 'Nama', width: '25%', data: 'nama'},
				{title: 'Tempat,&nbsp;Tgl Lahir', data: 'tempat_lahir',
					render: (data, type, row) => {
						if(type !== 'display') return data;
						return data+', '+row.tanggal_lahir.split('-').reverse().join('-');
					}
				},
				{title: 'Jenis&nbsp;Kelamin', width: '10%', data: 'jk'},
				{title: 'Jabatan', width: '15%', data: 'jabatan'},
				{title: 'Gaji&nbsp;Pokok', data: 'gaji_pokok', className:'dt-body-right',
					render: (data, type) => {
						if(type !== 'display') return data;
						return parseFloat(data).formatMoney(2,',','.');
					}
				},
				{title: 'Tunjangan', data: 'tunjangan', className:'dt-body-right',
					render: (data, type) => {
						if(type !== 'display') return data;
						return parseFloat(data).formatMoney(2,',','.');
					}
				},
				{title: '', data: null, orderable: false, searchable: false, className: 'text-center', 
					render: (data, type, row) => {
						return type === 'display' ? defaultAction(row) : data;
					}
				}
			],
			createdRow: (row, data, index) => {
				$(row).find('a.tombol-edit').data(data);
				$(row).find('a.tombol-hapus').data(data);
				$(row).find('a.tombol-histori').data('history_id', data.history_id);
			}
		},
		tableView = $('#table-view').DataTable(optionsView),
		event = {
			// All Event Function
			acl_list : {
				tambah: function(){
					$("#btn-simpan").off().on('click', function(e){
						e.preventDefault();
						let url = current_module+'/tambah',
							data = {
								'nama': $('#nama').val(),
								'tempat_lahir': $('#tempat_lahir').val(),
								'tanggal_lahir': $('#tanggal_lahir').val(),
								'jenis_kelamin': $('#jenis_kelamin').val(),
								'jabatan': $('#jabatan').val(),
								'gaji_pokok': $('#gaji_pokok').val(),
								'tunjangan': $('#tunjangan').val(),
							};
						load_url(url, data);
					});
				},
				edit: function(){
					tableView.on('click', 'a.tombol-edit', function(e){
						e.preventDefault();
						let data = $(this).data(),
							form = $("#form-modal");
						$('#form-modal div:first').removeClass('modal-success').addClass('modal-warning');
						form.find('#modal-title').html('Edit Data');
						form.find('#id').val(data.id);
						form.find('#nama').val(data.nama);
						form.find('#tempat_lahir').val(data.tempat_lahir);
						form.find('#tanggal_lahir').val(data.tanggal_lahir);
						form.find('#jenis_kelamin').val(data.jenis_kelamin);
						form.find('#jabatan').val(data.jabatan);
						form.find('#gaji_pokok').val(data.gaji_pokok);
						form.find('#edit_gaji_pokok').val(parseFloat(data.gaji_pokok).formatMoney(2,'.',','));
						form.find('#tunjangan').val(data.tunjangan);
						form.find('#edit_tunjangan').val(parseFloat(data.tunjangan).formatMoney(2,'.',','));
						let btn_simpan = form.find('#btn-simpan'),
							btn_edit = form.find('#btn-simpan')
								.clone(true)
								.removeClass('btn-success')
								.addClass('btn-warning')
								.attr('id', 'btn-update')
								.html("Update")
								.appendTo(form.find('.modal-footer'));
						form.find('#btn-simpan').remove();
						btn_edit.off().click(function(e){
							e.preventDefault();
							let url = current_module+'/edit',
								data = {
									'id': $('#id').val(),
									'nama': $('#nama').val(),
									'tempat_lahir': $('#tempat_lahir').val(),
									'tanggal_lahir': $('#tanggal_lahir').val(),
									'jenis_kelamin': $('#jenis_kelamin').val(),
									'jabatan': $('#jabatan').val(),
									'gaji_pokok': $('#gaji_pokok').val(),
									'tunjangan': $('#tunjangan').val(),
								};
							load_url(url, data);
						});
						form.modal('toggle');
						
						form.on('hidden.bs.modal', function(e){
							form.find('#modal-title').html('Tambah Data');
							$('#form-modal div:first').removeClass('modal-warning').addClass('modal-success');
							$('#id, #nama, #tempat_lahir, #tanggal_lahir, #jenis_kelamin, #jabatan, #gaji_pokok, #edit_gaji_pokok, #tunjangan, #edit_tunjangan').val('');
							btn_simpan.appendTo(form.find('.modal-footer'));
							btn_edit.remove();
							event.acl_list.tambah();
						});
					});
				},
				hapus: function(){
					tableView.on('click', 'a.tombol-hapus', function(e){
						e.preventDefault();
						let data = $(this).data(),
							body = 'Data <span class="text-primary">'+data.nama+'</span> akan dihapus, apakah anda yakin ?',
							action = {
								url: current_module+'/hapus',
								data: {id: data.id}
							};
						confirm_dialog(action, body, {value:'Hapus', class:'danger'});
					});
				},
			},			
			histori: function(){
				tableView.on('click', 'a.tombol-histori', function(e){
					e.preventDefault();
					show_histori($(this).data('history_id'));
				});
			},
			show_form : function(){
				eventNumbers('1234567890.', 'edit_gaji_pokok');
				eventNumbers('1234567890.', 'edit_tunjangan');
				$('#form-modal').on('shown.bs.modal', function(){
					let elements = $(this);
					event_focus(elements);
				});
				$('#form-modal').on('hidden.bs.modal', function(e){
					$('#id, #nama, #tempat_lahir, #tanggal_lahir, #jenis_kelamin, #jabatan, #gaji_pokok, #edit_gaji_pokok, #tunjangan, #edit_tunjangan').val('');
				});
			}
		};
		
	// All Event Call
	event.acl_list.tambah();
	event.acl_list.edit();
	event.acl_list.hapus();
	
	event.histori();
	event.show_form();
</script>