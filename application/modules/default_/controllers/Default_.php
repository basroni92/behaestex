<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Default_ extends MX_Controller {
	
    public function __construct() {
        parent::__construct();
		$this->load->model('Default_model', 'model');
    }
	
	protected function _middleware(){
        return array('auth');
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    function index($args = null){
		$this->load->library('parser');
		$this->load->library('Menu');
		$data = array(
			"page_title"	=> $this->menu->getCurrentMenu(),
			"menu_list"		=> $this->menu->get_user_menus(),
			"access_list"	=> $this->menu->get_user_menus(false),
		);
		$data = array_merge($data, $this->session->user, (array) $this->model->getProfile($this->session->user['id_user']));
		return $this->parser->parse('main', $data);
    }
	
	function change_password($args = null) {
		$pass_lama 		= trim($this->input->post('password_lama'));
		$pass_baru 		= trim($this->input->post('password_baru'));
		$confirm_pass 	= trim($this->input->post('password_baru_repeat'));
		if($pass_lama == "")
			return $this->response->set_message("Password Lama Kosong.")->return(true);
		
		if($pass_baru == "")
			return $this->response->set_message("Password Baru Kosong.")->return(true);
		
		if(strlen($pass_baru) < 8)
			return $this->response->set_message("Password Baru minimal harus 8 karakter.")->return(true);
		
		if($confirm_pass == "")
			return $this->response->set_message("Konfirmasi Password Baru Kosong.")->return(true);
		
		$this->load->model('login/Login_model');
		$user_data = $this->Login_model->user($this->session->user['nama_user'], $pass_lama);
		if($user_data->num_rows() == 0)
			return $this->response->set_message("Password Lama Salah.")->return(true);
		
		if($pass_lama == $pass_baru)
			return $this->response->set_message("Password Baru dan Password Lama Tidak Boleh Sama.")->return(true);
		
		if($pass_baru <> $confirm_pass)
			return $this->response->set_message("Password Baru dan Konfirmasi Password Baru Tidak Sama.")->return(true);
		
		/** Change Password di Database **/
		$pass_user = md5($pass_baru);
		if(!$this->model->editUser(compact('pass_user'),$this->session->user['nama_user']))
			return $this->response->set_message("Kesalahan Ketika Merubah Password.")->return(true);
		$this->response
			->set_result("OK")
			->set_message("Perubahan Password Berhasil.")
			->return(true);
		return;
	}
	
	function get_histori($args = null) {
		# Data Parameters
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[$order][data]")." ".$order_by;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		
		$unik_riwayat = $this->input->post("unik_riwayat");
		$filter = ['unik_riwayat'=>$unik_riwayat];
		$data_result = $this->model->getAllHistori($offset, $page_size, $sort, $filter, $search);
		echo json_encode($data_result);
	}
	
	function user_menus($args = null) {
		$this->load->library('Menu');
		echo json_encode((array) $this->menu->get_user_menus(false));
	}
}
