<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Default_model extends CI_Model {
	function __construct () {
        parent::__construct();
	}
	
	function getAllHistori($offset, $page_size, $sort, $filter = null, $search = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS
				a.id,
				a.mode_riwayat AS riwayat,
				DATE_FORMAT(a.tgl_riwayat, '%Y-%m-%d') AS tanggal,
				DATE_FORMAT(a.tgl_riwayat, '%H:%i:%s') AS jam,
				a.tgl_riwayat,
				a.unik_riwayat,
				b.nama_user AS pengguna,
				CONCAT(a.mode_riwayat, ' ', DATE_FORMAT(a.tgl_riwayat, '%Y-%m-%d'), ' ', DATE_FORMAT(a.tgl_riwayat, '%d-%m-%Y'), ' ', DATE_FORMAT(a.tgl_riwayat, '%H:%i:%s'), ' ', b.nama_user) AS search", false)
			->from("riwayat a")
			->join("user b", "b.id=a.id_user", "INNER");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getProfile($id){
		$this->db
			->select("a.nama_user, b.nama")
			->from("user a")
			->join("pegawai b", "b.id=a.id_pegawai", "INNER")
			->where("a.id", $id);
		return $this->db->get()->row();
	}
	
	function editUser($data, $user){
		return $this->db
			->where("nama_user", $user)
			->update('user', $data);
	}
}
