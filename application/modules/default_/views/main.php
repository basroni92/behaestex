<!DOCTYPE html>
<html lang="en">

<head>
	<base href="./">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<meta name="author" content="Prodesign Team">
	<title>{page_title}</title>
	<!-- Icons-->
	<link href="<?=base_url()?>assets/img/brand/logo.png" rel="icon">
	<link href="<?=base_url()?>assets/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
	<!-- Main styles for this application-->
	<link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/toastr.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/css/jquery-ui.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
	
	<script>
		(function(funcName, baseObj) {
			funcName = funcName || "docReady";
			baseObj = baseObj || window;
			var readyList = [];
			var readyFired = false;
			var readyEventHandlersInstalled = false;
		
			async function ready() {
				if (!readyFired) {
					readyFired = true;
					for (var i = 0; i < readyList.length; i++) {
						await readyList[i].fn.call(window, readyList[i].ctx);
					}
					readyList = [];
				}
			}
		
			function readyStateChange() {
				if ( document.readyState === "complete" ) {
					ready();
				}
			}
		
			baseObj[funcName] = function(callback, context) {
				if (typeof callback !== "function") {
					throw new TypeError("callback for docReady(fn) must be a function");
				}
				if (readyFired) {
					setTimeout(function() {callback(context);}, 1);
					return;
				} else {
					readyList.push({fn: callback, ctx: context});
				}
				if (document.readyState === "complete") {
					setTimeout(ready, 1);
				} else if (!readyEventHandlersInstalled) {
					if (document.addEventListener) {
						document.addEventListener("DOMContentLoaded", ready, false);
						window.addEventListener("load", ready, false);
					} else {
						document.attachEvent("onreadystatechange", readyStateChange);
						window.attachEvent("onload", ready);
					}
					readyEventHandlersInstalled = true;
				}
			}
		})("docReady", window);
		var $BASE_URL = '<?=base_url();?>',
			$LIST_MODULES = <?=json_encode($access_list)?>;
	</script>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<header class="app-header navbar">
		<button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
			<span class="navbar-toggler-icon"></span>
		</button> 
		<button class="navbar-toggler sidebar-toggler d-md-down-none" id="menu_toggle_up" type="button" data-toggle="sidebar-lg-show">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="<?=base_url()?>">
			<img class="navbar-brand-full" src="<?=base_url()?>assets/img/brand/logo.png" style="width: auto" width="89" height="25" >
			<img class="navbar-brand-minimized" src="<?=base_url()?>assets/img/brand/logo.png" style="width: auto" width="89" height="25">
		</a>
		<ul class="nav navbar-nav ml-5 pl-2">
			<li class="nav-item nav-item-branch dropdown d-md-down-none font-weight-bold"></li>
		</ul>
		<ul class="nav navbar-nav ml-auto">
			<li class="nav-item dropdown">
				<a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					<img id="img-avatar" class="img-avatar" src="<?=base_url()?>assets/img/avatars/user.png" data-toggle="tooltip" data-placement="left" title="{pegawai}">
				</a>
				<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item" href="#" data-toggle="modal" data-target=".change-password-modal">
						<i class="fa fa-key"></i> Ubah Password
					</a>
					<a class="dropdown-item" href="<?=base_url()?>logout">
						<i class="fa fa-sign-out"></i> Logout
					</a>
				</div>
			</li>
		</ul>
	</header>
	<div class="app-body">
		<div class="sidebar left_col" id="sidebar-menu">
			<div class="sidebar-nav-branch"></div>
			<nav class="sidebar-nav">
				<div class="nav-search">
					<i class="nav-icon icon-magnifier nav-search-icon"></i>
					<i class="nav-icon icon-close nav-search-clear d-none"></i>
					<input type="text" placeholder="Search Menu..." class="nav-search-input">
				</div>
				<h3 class="nav-title">Applications</h3>
				<ul class="nav"></nav>
			</nav>
			<button class="sidebar-minimizer brand-minimizer" id="menu_toggle_down" type="button"></button>
		</div>
		<main class="main">
			<div class="container-fluid p-2 p-lg-4">
				<div class="animated fadeIn right_col" id="main-content"></div>
			</div>
		</main>
		
		<!-- Change password dialog -->
		<div class="modal fade change-password-modal" id="change-password-modal" tabindex="-1" role="dialog" aria-labelledby="changePasswordModal" aria-hidden="true">
			<div class="modal-dialog modal-md card-body">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Atur Password</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" id="change-password-form">
							<div class="form-group row">
								<label for="input-change-old-password" class="col-form-label col-md-5">Password Lama </label>
								<div class="col-md-7">
									<input id="input-change-old-password" name="password_lama" type="password" class="form-control" placeholder="Password Lama" required="" maxlength="30" onclick="this.select()">
								</div>
							</div> 
							<div class="form-group row">
								<label for="input-change-new-password" class="col-form-label col-md-5">Password Baru</label>
								<div class="col-md-7">
									<input id="input-change-new-password" name="password_baru" type="password" class="form-control" placeholder="Password Baru" required="" maxlength="30" onclick="this.select()">
								</div>
							</div>
							<div class="form-group row">
								<label for="input-change-new-password-repeat" class="col-form-label col-md-5">Ulangi Password Baru</label>
								<div class="col-md-7">
									<input id="input-change-new-password-repeat" name="password_baru_repeat" type="password" class="form-control" placeholder="Ulangi Password Baru" required="" maxlength="30" onclick="this.select()">
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" onclick="change_password()">Simpan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="loading"><img src="<?=base_url();?>assets/images/loading.gif"></div>
	<iframe id="iframe-download-excel" class="d-none"></iframe>
	<footer></footer>
	<!-- Bootstrap and necessary plugins-->
	<script src="<?=base_url()?>assets/node_modules/jquery/dist/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery-ui.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/pace-progress/pace.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/@coreui/coreui/dist/js/coreui.min.js"></script>
	<!-- Plugins and scripts required by this view-->
	<script src="<?=base_url()?>assets/js/chartjs.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script> 
	<script src="<?=base_url()?>assets/js/dataTables.checkboxes.min.js"></script>
	<script src="<?=base_url()?>assets/js/dataTables.scroller.min.js"></script>
	<script src="<?=base_url()?>assets/js/toastr.min.js"></script>
	<script src="<?=base_url()?>assets/js/addons.min.js"></script>
	<script src="<?=base_url()?>assets/js/main.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.inputmask.min.js"></script>
	<script src="<?=base_url()?>assets/js/babel.min.js"></script>
</body>

</html>