<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Payroll_model extends CI_Model {
	function __construct () {
        parent::__construct();
	}
	
	function getAllPerhitungan($offset, $page_size, $sort, $filter = null, $search = null){
		$bulan = (int) $filter['bulan'];
		$tahun = (int) $filter['tahun'];
		unset($filter['bulan'], $filter['tahun']);
		$this->db->query("SET @tahun := ?, @bulan := ?;", [$tahun, $bulan]);
		$this->db
			->select("SQL_CALC_FOUND_ROWS
					a.id_pegawai,
					@bulan AS bulan,
					@tahun AS tahun,
					a.nama,
					a.jabatan,
					IF(ISNULL(b.id), '0', '1') AS info,
					COUNT(IF(a.info = 'nwnp', 1, NULL)) AS total_nwnp,
					a.gaji_pokok,
					a.tunjangan,
					SUM(a.upah_lembur) AS upah_lembur,
					ROUND(COUNT(IF(a.info = 'nwnp', 1, NULL)) * a.gaji_pokok / 30, 2) * -1 AS potongan_nwnp,
					ROUND((a.gaji_pokok + a.tunjangan) * 3 / 100, 2) * -1 AS potongan_bpjs,
					a.gaji_pokok + a.tunjangan + SUM(a.upah_lembur) - ROUND(COUNT(IF(a.info = 'nwnp', 1, NULL)) * a.gaji_pokok / 30, 2) - ROUND((a.gaji_pokok + a.tunjangan) * 3 / 100, 2) AS total_gaji,
					CONCAT(a.nama, ' ', a.jabatan) AS search
				FROM (
					SELECT 
						a.id_pegawai,
						a.tanggal,
						b.nama,
						b.jabatan,
						b.gaji_pokok,
						b.tunjangan,
						ROUND(b.gaji_pokok / 173, 2) AS upah_lembur_perjam,
						IF(a.lembur <= 4,
							ROUND(b.gaji_pokok / 173, 2) * a.lembur,
							(ROUND(b.gaji_pokok / 173, 2) * 4) + ((ROUND(b.gaji_pokok / 173, 2) * 2) * (a.lembur - 4))
						) AS upah_lembur,
						a.lembur,
						a.st,
						IF((a.st = 0 AND !(DATE_FORMAT(a.tanggal,'%w') = 0 OR DATE_FORMAT(a.tanggal,'%w') = 6)), 
							'nwnp', IF(a.st = 0, 'libur', 'masuk')
						) AS info
					FROM (
						SELECT 
							a.id_pegawai, 
							DATE(CONCAT(@tahun, LPAD(@bulan,2,0), LPAD(a.tgl, 2, 0))) AS tanggal,
							MAX(a.lembur) AS lembur,
							MAX(a.st) AS st
						FROM (
							SELECT a.id_pegawai, a.tgl, 0 AS lembur, 0 AS st
							FROM (
								SELECT b.id AS id_pegawai, a.tgl
								FROM (
									SELECT a.n+(b.n*10) AS tgl
									FROM (
										SELECT 0 AS n UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 
									) a
									JOIN (
										SELECT 0 AS n UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3
									) b 
									HAVING tgl BETWEEN 1 AND DAY(LAST_DAY(CONCAT(@tahun, '-', LPAD(@bulan,2,0), '-01')))
									ORDER BY tgl
								) a
								JOIN pegawai b
								ORDER BY b.id, a.tgl
								LIMIT 10000000000000000000
							) a
							UNION ALL
							SELECT
								a.id_pegawai,
								DAY(a.tgl_absensi) AS tgl,
								HOUR(TIMEDIFF(TIME('16:30:00'),a.jam_keluar)) AS lembur,
								1 AS st
							FROM absensi a
							WHERE DATE_FORMAT(a.tgl_absensi, '%Y%m') = CONCAT(@tahun, LPAD(@bulan, 2, 0))
							LIMIT 10000000000000000000
						) a
						GROUP BY a.id_pegawai, a.tgl
						ORDER BY a.id_pegawai, a.tgl
						LIMIT 10000000000000000000
					) a
					INNER JOIN pegawai b ON b.id=a.id_pegawai
					LIMIT 10000000000000000000
				) a", false)
			->join("payroll b", "b.bulan='$bulan' AND b.tahun='$tahun' AND b.id_pegawai=a.id_pegawai", "LEFT")
			->group_by("a.id_pegawai");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getAllGaji($offset, $page_size, $sort, $filter = null, $search = null){
		$bulan = (int) $filter['bulan'];
		$tahun = (int) $filter['tahun'];
		unset($filter['bulan'], $filter['tahun']);
		$this->db
			->select("SQL_CALC_FOUND_ROWS 
				a.id,
				a.id_pegawai,
				a.bulan,
				a.tahun,
				b.nama,
				b.jabatan,
				a.alpha,
				a.gaji_pokok,
				a.tunjangan,
				a.lembur,
				a.nwnp,
				a.bpjs,
				a.total_penerimaan,
				CONCAT(b.nama, ' ', b.jabatan) AS search", false)
			->from("payroll a")
			->join("pegawai b", "b.id=a.id_pegawai", "INNER")
			->where("a.bulan", $bulan)
			->where("a.tahun", $tahun);
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getAllDetailData($bulan, $tahun, $id_pegawai){
		$this->db->query("SET @tahun := ?, @bulan := ?, @id_pegawai := ?;", [$tahun, $bulan, $id_pegawai]);
		$this->db
			->select("SQL_CALC_FOUND_ROWS
					a.id_pegawai,
					a.tanggal,
					a.jam_masuk,
					a.jam_keluar,
					a.lembur,
					ROUND(b.gaji_pokok / 173, 2) AS upah_lembur_perjam,
					IF(a.lembur <= 4,
						ROUND(b.gaji_pokok / 173, 2) * a.lembur,
						(ROUND(b.gaji_pokok / 173, 2) * 4) + ((ROUND(b.gaji_pokok / 173, 2) * 2) * (a.lembur - 4))
					) AS upah_lembur,
					ROUND((IF((a.st = 0 AND !(DATE_FORMAT(a.tanggal,'%w') = 0 OR DATE_FORMAT(a.tanggal,'%w') = 6)), 1, 0) * b.gaji_pokok / 30), 2) * -1 AS potongan_nwnp,
					IF((a.st = 0 AND !(DATE_FORMAT(a.tanggal,'%w') = 0 OR DATE_FORMAT(a.tanggal,'%w') = 6)), 
						'nwnp', IF(a.st = 0, 'libur', 'masuk')
					) AS info
				FROM (
					SELECT 
						a.id_pegawai, 
						DATE(CONCAT(@tahun, LPAD(@bulan,2,0), LPAD(a.tgl, 2, 0))) AS tanggal,
						MAX(a.jam_masuk) AS jam_masuk,
						MAX(a.jam_keluar) AS jam_keluar,
						MAX(a.lembur) AS lembur,
						MAX(a.st) AS st
					FROM (
						SELECT 
							a.id_pegawai, 
							a.tgl, 
							TIME('00:00:00') AS jam_masuk,
							TIME('00:00:00') AS jam_keluar,
							0 AS lembur, 
							0 AS st
						FROM (
							SELECT b.id AS id_pegawai, a.tgl
							FROM (
								SELECT a.n+(b.n*10) AS tgl
								FROM (
									SELECT 0 AS n UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 
								) a
								JOIN (
									SELECT 0 AS n UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3
								) b 
								HAVING tgl BETWEEN 1 AND DAY(LAST_DAY(CONCAT(@tahun, '-', LPAD(@bulan,2,0), '-01')))
								ORDER BY tgl
							) a
							JOIN pegawai b
							WHERE b.id = @id_pegawai
							ORDER BY b.id, a.tgl
							LIMIT 10000000000000000000
						) a
						UNION ALL
						SELECT
							a.id_pegawai,
							DAY(a.tgl_absensi) AS tgl,
							a.jam_masuk,
							a.jam_keluar,
							HOUR(TIMEDIFF(TIME('16:30:00'),a.jam_keluar)) AS lembur,
							1 AS st
						FROM absensi a
						WHERE DATE_FORMAT(a.tgl_absensi, '%Y%m') = CONCAT(@tahun, LPAD(@bulan, 2, 0))
							AND a.id_pegawai = @id_pegawai
						LIMIT 10000000000000000000
					) a
					GROUP BY a.id_pegawai, a.tgl
					ORDER BY a.id_pegawai, a.tgl
					LIMIT 10000000000000000000
				) a", false)
			->join("pegawai b", "b.id=a.id_pegawai", "INNER");
		return $this->app->paramGetData(0, 0, "a.tanggal ASC");
	}
	
	function getRowPayroll($filters = null){
		$this->db->from("payroll a");
		if(!empty($filters)){
			foreach($filters as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->get();
	}
	
	function addData($data){
		return $this->db->insert('payroll', $data);
	}
	
	function hapusData($id){
		return $this->db->delete('payroll', compact('id'));
	}
}
