<div class="card" id="list-payroll">
	<div class="card-header">
		<i class="fa fa-bars"></i> <strong>Data Payroll</strong>
	</div>
	<div class="card-body p-1 p-md-2 p-lg-3">
		<div class="row">
			<div class="col-12">
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#perhitungan" role="tab" aria-controls="perhitungan" aria-selected="true">
							<i class="fa fa-calculator"></i> Perhitungan Gaji
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#gaji" role="tab" aria-controls="gaji" aria-selected="false">
							<i class="fa fa-credit-card"></i> Payroll Approval
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane p-1 p-md-2 p-lg-3 active" id="perhitungan" role="tabpanel">
						<div class="d-flex">
							<button type="button" class="btn btn-link ml-auto" id="refresh-perhitungan" title="MUAT ULANG"><i class="fa fa-refresh"></i></button>
						</div>
						<hr class="mt-0 mb-1 mb-lg-2" style="width: 100%"/>
						<table id="table-perhitungan" style="width: 100%"></table>
					</div>
					<div class="tab-pane p-1 p-md-2 p-lg-3 " id="gaji" role="tabpanel">
						<div class="d-flex">
							<button type="button" class="btn btn-link ml-auto" id="refresh-gaji" title="MUAT ULANG"><i class="fa fa-refresh"></i></button>
						</div>
						<hr class="mt-0 mb-1 mb-lg-2" style="width: 100%"/>
						<table id="table-gaji" style="width: 100%"></table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Print Dialog -->
<div id="print-dialog" class="modal fade" tabIndex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">Print PDF Format</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
				<span aria-hidden="true"><i class="fa fa-close"></i></span>
			</div>
			<div class="modal-body p-1 p-md-2 p-lg-3">
				<iframe width="100%" height="600"></iframe>
			</div>
			<div class="modal-footer">
				<button class="btn btn-outline-dark" type="button" data-dismiss="modal"><i class="fa fa-close"></i> TUTUP</button>
			</div>
		</div>

	</div>
</div>
<!-- End Modal Print Dialog -->

<!-- Modal Detail Data -->
<div id="detail-data" class="modal fade" tabIndex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">DETAIL DATA</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
				<span aria-hidden="true"><i class="fa fa-close"></i></span>
			</div>
			<div class="modal-body p-1 p-md-2 p-lg-3">
				<div class="row">
					<div class="col-12 col-lg-6">
						<div class="form-group row">
							<label for="periode" class="col-12 col-lg-3">PERIODE</label>
							<div class="col-12 col-lg-9">
								<input type="text" id="periode" class="form-control" readOnly>
							</div>
						</div>
					</div>
					<div class="col-12  col-lg-6">
						<div class="form-group row">
							<label for="nama" class="col-12 col-lg-3">NAMA</label>
							<div class="col-12 col-lg-9">
								<input type="hidden" id="id_pegawai" readOnly>
								<input type="text" id="nama" class="form-control" readOnly>
							</div>
						</div>
					</div>
					<div class="col-12  col-lg-6">
						<div class="form-group row">
							<label for="jabatan" class="col-12 col-lg-3">JABATAN</label>
							<div class="col-12 col-lg-9">
								<input type="text" id="jabatan" class="form-control" readOnly>
							</div>
						</div>
					</div>
					<div class="col-12  col-lg-6">
						<div class="form-group row">
							<label for="gaji_pokok" class="col-12 col-lg-3">GAJI POKOK</label>
							<div class="col-12 col-lg-9">
								<input type="text" id="gaji_pokok" class="form-control text-right" readOnly>
							</div>
						</div>
					</div>
					<div class="col-12  col-lg-6">
						<div class="form-group row">
							<label for="tunjangan" class="col-12 col-lg-3">TUNJANGAN</label>
							<div class="col-12 col-lg-9">
								<input type="text" id="tunjangan" class="form-control text-right" readOnly>
							</div>
						</div>
					</div>
					<div class="col-12  col-lg-6">
						<div class="form-group row">
							<label for="potongan_bpjs" class="col-12 col-lg-3">BPJS</label>
							<div class="col-12 col-lg-9">
								<input type="text" id="potongan_bpjs" class="form-control text-right" readOnly>
							</div>
						</div>
					</div>
				</div>
				<table style="width:100%"></table>
			</div>
			<div class="modal-footer">
				<button class="btn btn-outline-dark" type="button" data-dismiss="modal"><i class="fa fa-close"></i> TUTUP</button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Detail Data -->

<script type="text/javascript">
	var current_module = '<?=base_url().$this->app->getCurrentModule()?>',
		acl_list = <?=$this->app->get_allow_acl(true);?>,
		filterBulan = <?=get_month_option(true,false)?>,
		filterTahun = <?=get_year_option(true)?>,
		listPayroll = $('#list-payroll'),
		listPerhitungan = listPayroll.find('#perhitungan'),
		listGaji = listPayroll.find('#gaji'),
		modalPrint = $('#print-dialog'),
		detailModal = $('#detail-data'),
		options = {
			perhitungan: {
				dom: "<'row'<'col-12 col-lg-6 pb-2 pb-lg-0'l><'col-12 col-lg-6'f>>" +
					"<'row'<'col-12'<'table-responsive'tr>>>" +
					"<'row'<'col-12 col-lg-6'i><'col-12 col-lg-6'p>>",
				processing: true,
				serverSide: true,
				sPaginationType: 'full_numbers',
				aaSorting: [0, 'asc'],
				ajax: {
					url: current_module+'/index/get_perhitungan', 
					type: 'POST',
					data: data => {
						let bulan = listPerhitungan.find('#filter_bulan').val() || "<?=date('m')/1;?>",
							tahun = listPerhitungan.find('#filter_tahun').val() || "<?=date('Y')?>",
							info = listPerhitungan.find('#filter_info').val() || "-";
						return {...data, bulan, tahun, info};
					}
				},
				initComplete: function(){
					let self = this,
						data = [
							{label: 'Bulan', id: 'filter_bulan', value: filterBulan, defaultValue: <?=date('m')/1?>},
							{label: 'Tahun', id: 'filter_tahun', value: filterTahun, defaultValue: <?=date('Y')?>},
							{label: 'Status', id: 'filter_info', value: {0:'CALCULATION',1:'APPROVED','-':'SEMUA'}, defaultValue: '-'},
						];
					// Create Filter
					create_filter(self[0].id, data);
					
					// Set Auto Focus
					listPerhitungan.find('input[type="search"]').trigger('focus');
				},
				columns: [
					{title: 'Nama', width: '25%', data: 'nama',
						render: (data, type, row) => {
							if(type !== 'display') return data;
							return '<p class="m-0">'+data.split(' ').join('&nbsp;')+'</p>\
								<p class="m-0 small-font text-primary">'+row.jabatan+'</p>';
						}
					},
					{title: 'Gaji&nbsp;Pokok', data: 'gaji_pokok', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'Tunjangan', data: 'tunjangan', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'Lembur', data: 'upah_lembur', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'BPJS', data: 'potongan_bpjs', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'NWNP', data: 'potongan_nwnp', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'Total&nbsp;Gaji', data: 'total_gaji', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return '<b>'+parseFloat(data).formatMoney(2,',','.')+'</b>';
						}
					},
					{title: 'Status', width: '10%', data: 'info',
						render: (data, type) => {
							if(type !== 'display') return data;
							if(data == '0')
								return '<i class="fa fa-calculator text-primary"></i>&nbsp;Calculation';
							else
								return '<i class="fa fa-check text-success"></i>&nbsp;Approved';
						}
					},
					{title: '', width: '7%', data: null, orderable: false, searchable: false, className: 'text-center', 
						render: (data, type, row) => {
							if(type != 'display') return data;
							let html = '';
							if(is_allow('approve1', acl_list))
								html += '<a class="btn btn-sm btn-brand btn-primary tombol-approve1 '+(row.info === '0' ? '' : 'disabled')+'" title="Approve Data"><span class="btn-approve"><i class="fa fa-lock text-white"></i></span></a>&nbsp;';
							html += '<a class="btn btn-sm btn-brand btn-instagram tombol-detail" title="Detail"><i class="fa fa-file-text-o text-white"></i></a>';
							return html;
						}
					},
				],
				createdRow: (row, data) => {
					let $row = $(row);
					$row.find('.tombol-approve1').off().click(e => {
						e.preventDefault();
						let body = 'Data Perhitungan Gaji Periode <span class="text-success">'+filterBulan[data.bulan]+' '+data.tahun+'</span> <span class="text-danger">'+data.nama+'</span> akan di approve, apakah anda yakin ?',
							action = {
								url: current_module+'/approve1',
								data: {
									bulan: data.bulan,
									tahun: data.tahun,
									id_pegawai: data.id_pegawai,
								}
							};
						confirm_dialog(action, body,  {value:'Approve', class:'primary'});
					});
					$row.find('.tombol-detail').off().click(e => {
						e.preventDefault();
						if($.fn.dataTable.isDataTable(detailModal.find('table'))){
							detailModal.find('table').DataTable().destroy();
							detailModal.find('table').empty();
						}
						let {bulan, tahun, id_pegawai} = data,
							option = {
								dom: "<'row'<'col-12'<'table-responsive'tr>>>",
								processing: true,
								serverSide: true,
								destroy: true,
								deferRender: true,
								filter: false,
								info: false,
								paging: false,
								lengthChange: false,
								ordering: false,
								ajax: {
									url: current_module+'/index/detail',
									type: 'POST',
									data: params => Object.assign(params, {bulan, tahun, id_pegawai})
								},
								initComplete: function(){
									let self = this,
										lembur = Array.from(self.api().data().map(res => parseInt(res.lembur))),
										upah_lembur = Array.from(self.api().data().map(res => parseFloat(res.upah_lembur))),
										pot_nwnp = Array.from(self.api().data().map(res => parseFloat(res.potongan_nwnp)));
										t_lembur = lembur.reduce((value, el) => value + el, 0),
										t_upah_lembur = upah_lembur.reduce((value, el) => value + el, 0),
										t_pot_nwnp = pot_nwnp.reduce((value, el) => value + el, 0),
										footer = $('\
											<tr role="row">\
												<td class="dt-body-center" colspan="3"><strong>TOTAL</strong></td>\
												<td class="dt-body-center font-weight-bold">'+parseInt(t_lembur).formatMoney(0,',','.')+' Jam</td>\
												<td class="dt-body-right">&nbsp;</td>\
												<td class="dt-body-right font-weight-bold">'+parseFloat(t_upah_lembur).formatMoney(2,',','.')+'</td>\
												<td class="dt-body-right font-weight-bold">'+parseFloat(t_pot_nwnp).formatMoney(2,',','.')+'</td>\
											</tr>');
									self.api().$('tr:last').after(footer);
								},
								columns: [
									{title: 'Tanggal', width: '12%', data: 'tanggal', 
										render: (data, type, row) => {
											if(type !== 'display') return data;
											return data.split('-').reverse().join('-');
										}
									},
									{title: 'Jam&nbsp;Masuk', width: '10%', data: 'jam_masuk', className: 'dt-body-center'},
									{title: 'Jam&nbsp;Keluar', width: '10%', data: 'jam_keluar', className: 'dt-body-center'},
									{title: 'Lembur', width: '10%', data: 'lembur', className: 'dt-body-center',
										render: (data, type, row) => {
											if(type !== 'display') return data;
											return (data != 0 ? data+' Jam' : '');
										}
									},
									{title: 'Upah&nbsp;Lembur&nbsp;(@jam)', data: 'upah_lembur_perjam', className:'dt-body-right',
										render: (data, type) => {
											if(type !== 'display') return data;
											return parseFloat(data).formatMoney(2,',','.');
										}
									},
									{title: 'Upah&nbsp;Lembur', data: 'upah_lembur', className:'dt-body-right',
										render: (data, type) => {
											if(type !== 'display') return data;
											return (parseFloat(data) != 0 ? parseFloat(data).formatMoney(2,',','.') : '');
										}
									},
									{title: 'Pot.&nbsp;NWNP', data: 'potongan_nwnp', className:'dt-body-right',
										render: (data, type) => {
											if(type !== 'display') return data;
											return (parseFloat(data) != 0 ? parseFloat(data).formatMoney(2,',','.') : '');
										}
									},
								],
								createdRow: (row, data) => {
									let detailRow = $(row);
									if(data.info == 'libur')
										detailRow.addClass('bg-danger');
									else if(data.info == 'nwnp')
										detailRow.addClass('bg-warning text-dark');
									else 
										detailRow.addClass('bg-info text-dark');
								},
								drawCallback: function(setting) {
									
								},
							};
						detailModal.find("#id_pegawai").val(id_pegawai);
						detailModal.find("#periode").val(filterBulan[bulan]+' '+tahun);
						detailModal.find("#nama").val(data.nama);
						detailModal.find("#jabatan").val(data.jabatan);
						detailModal.find("#gaji_pokok").val(parseFloat(data.gaji_pokok).formatMoney(2,',','.'));
						detailModal.find("#tunjangan").val(parseFloat(data.tunjangan).formatMoney(2,',','.'));
						detailModal.find("#potongan_bpjs").val(parseFloat(data.potongan_bpjs).formatMoney(2,',','.'));
						detailModal.find('table').DataTable(option);
						detailModal.modal('toggle');
					});
				}
			},
			gaji: {
				dom: "<'row'<'col-12 col-lg-6 pb-2 pb-lg-0'l><'col-12 col-lg-6'f>>" +
					"<'row'<'col-12'<'table-responsive'tr>>>" +
					"<'row'<'col-12 col-lg-6'i><'col-12 col-lg-6'p>>",
				processing: true,
				serverSide: true,
				sPaginationType: 'full_numbers',
				aaSorting: [0, 'asc'],
				ajax: {
					url: current_module+'/index/get_gaji', 
					type: 'POST',
					data: data => {
						let bulan = listGaji.find('#filter_bulan2').val() || "<?=date('m')/1;?>",
							tahun = listGaji.find('#filter_tahun2').val() || "<?=date('Y')?>";
						return {...data, bulan, tahun};
					}
				},
				initComplete: function(){
					let self = this,
						data = [
							{label: 'Bulan', id: 'filter_bulan2', value: filterBulan, defaultValue: <?=date('m')/1?>},
							{label: 'Tahun', id: 'filter_tahun2', value: filterTahun, defaultValue: <?=date('Y')?>},
						];
					// Create Filter
					create_filter(self[0].id, data);
					
					// Set Auto Focus
					listGaji.find('input[type="search"]').trigger('focus');
				},
				columns: [
					{title: 'Nama', width: '25%', data: 'nama',
						render: (data, type, row) => {
							if(type !== 'display') return data;
							return '<p class="m-0">'+data.split(' ').join('&nbsp;')+'</p>\
								<p class="m-0 small-font text-primary">'+row.jabatan+'</p>';
						}
					},
					{title: 'Gaji&nbsp;Pokok', data: 'gaji_pokok', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'Tunjangan', data: 'tunjangan', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'Lembur', data: 'lembur', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'BPJS', data: 'bpjs', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'NWNP', data: 'nwnp', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return parseFloat(data).formatMoney(2,',','.');
						}
					},
					{title: 'Total&nbsp;Gaji', data: 'total_penerimaan', className:'dt-body-right',
						render: (data, type) => {
							if(type !== 'display') return data;
							return '<b>'+parseFloat(data).formatMoney(2,',','.')+'</b>';
						}
					},
					{title: '', width: '7%', data: null, orderable: false, searchable: false, className: 'text-center', 
						render: (data, type, row) => {
							if(type != 'display') return data;
							let html = '';
							if(is_allow('hapus', acl_list))
								html += '<a class="btn btn-sm btn-brand btn-danger tombol-hapus" title="Hapus Data"><span class="btn-hapus"><i class="fa fa-trash text-white"></i></span></a>&nbsp;';
							html += '<a class="btn btn-sm btn-brand btn-success '+(is_allow('cetak', acl_list) ? 'tombol-cetak' : 'disabled')+'" title="Slip Gaji"><i class="fa fa-print text-white"></i></a>';
							return html;
						}
					},
				],
				createdRow: (row, data) => {
					let $row = $(row);
					$row.find('.tombol-hapus').off().click(e => {
						e.preventDefault();
						let body = 'Data Payroll Gaji <span class="text-primary">'+data.nama+'</span> akan di hapus, apakah anda yakin ?',
							action = {
								url: current_module+'/hapus',
								data: {id: data.id}
							};
						confirm_dialog(action, body, {value:'Hapus', class:'danger'});
					});
					$row.find('.tombol-cetak').off().click(e => {
						e.preventDefault();
						modalPrint.find('iframe').attr('src', current_module+'/cetak/'+data.id);
						modalPrint.modal('toggle');
					});
				},
			}
		},
		tablePerhitungan = listPerhitungan.find('#table-perhitungan').DataTable(options.perhitungan);
	// Tab Perhitungan Gaji
	listPayroll.find('li.nav-item:first > a').click(e => {
		e.preventDefault();
		if($.fn.dataTable.isDataTable(listPerhitungan.find('#table-perhitungan')))
			tablePerhitungan.ajax.reload();
		else 
			listPerhitungan.find('#table-perhitungan').DataTable(options.gaji);
	});
	
	// Tab Payroll Approval
	listPayroll.find('li.nav-item:last > a').click(e => {
		e.preventDefault();
		if($.fn.dataTable.isDataTable(listGaji.find('#table-gaji')))
			listGaji.find('#table-gaji').DataTable().ajax.reload();
		else 
			listGaji.find('#table-gaji').DataTable(options.gaji);
	});
	
	// Refresh Perhitungan Gaji
	listPerhitungan.find('#refresh-perhitungan').off().click(e => {
		e.preventDefault();
		tablePerhitungan.ajax.reload();
	});
	
	// Refresh Payroll Approval
	listGaji.find('#refresh-gaji').off().click(e => {
		e.preventDefault();
		listGaji.find('#table-gaji').DataTable().ajax.reload();
	});
</script>