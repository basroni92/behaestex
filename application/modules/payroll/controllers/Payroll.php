<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Payroll extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('Payroll_model', 'model');
		$this->load->model('pegawai/pegawai_model', 'pegawai');
    }
	
	protected function _middleware(){
        return array('auth', 'access');
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    public function index($args = null){
		# Data Parameters
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[$order][data]")." ".$order_by;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		$bulan 		= (int) $this->input->post("bulan");
		$tahun 		= (int) $this->input->post("tahun");
		
		switch($args[0]){
			case 'get_perhitungan':
				$info = trim($this->input->post("info"));
				$filter = compact('bulan', 'tahun', 'info');
				$data_result = $this->model->getAllPerhitungan($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			case 'get_gaji':
				$filter = compact('bulan', 'tahun');
				$data_result = $this->model->getAllGaji($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			case 'detail':
				$id_pegawai = (int) $this->input->post("id_pegawai");
				$data_result = $this->model->getAllDetailData($bulan, $tahun, $id_pegawai);
				echo json_encode($data_result);
				break;
			default:
				$this->load->view("main");
		}
    }
	
	public function hapus($args = null){
		$id = (int) $this->input->post('id');
		
		$rs_data = $this->model->getRowPayroll(compact('id'));
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		
		$this->db->trans_begin();
		if(!$this->model->hapusData($id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Hapus Data.")->return(true);
		}
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Data Berhasil Di Hapus.")
			->set_callback("                                    
				$('#main-dialog-confirm').modal('toggle');
				listGaji.find('#table-gaji').DataTable().ajax.reload(null, false);
			")
			->return(true);
		return;
	}
	
	public function approve1($args = null){
		$bulan 		= (int) $this->input->post('bulan');
		$tahun 		= (int) $this->input->post('tahun');
		$id_pegawai = (int) $this->input->post('id_pegawai');
		
		$rs_data = $this->model->getAllPerhitungan(0, 1, 1, compact('bulan','tahun','id_pegawai'));
		if($rs_data->recordsTotal == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$rs_data = (Object) $rs_data->data[0];
		
		# Cek Pegawai
		$rs_pegawai = $this->pegawai->getRowData(['id'=>$id_pegawai]);
		if($rs_pegawai->num_rows() == 0)
			return $this->response->set_message("Data Pegawai Tidak Valid.")->return(true);
		
		# Cek Data Payroll Jika Sudah Ada
		$rs_payroll = $this->model->getRowPayroll(compact('bulan','tahun','id_pegawai'));
		if($rs_payroll->num_rows() > 0)
			return $this->response->set_message("Data Sudah Ada Di Payroll Approval. <br>Silahkan Hapus Data Terlebih Dahulu.")->return(true);
		
		$this->db->trans_begin();
		$data = array(
			'bulan' 			=> $bulan,
			'tahun' 			=> $tahun,
			'id_pegawai' 		=> $id_pegawai,
			'alpha'				=> $rs_data->total_nwnp,
			'gaji_pokok'		=> $rs_data->gaji_pokok,
			'tunjangan' 		=> $rs_data->tunjangan,
			'lembur'			=> $rs_data->upah_lembur,
			'nwnp'				=> $rs_data->potongan_nwnp,
			'bpjs'				=> $rs_data->potongan_bpjs,
			'total_penerimaan'	=> $rs_data->total_gaji,
		);
		if(!$this->model->addData($data)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Approved Data.")->return(true);
		}
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Data Berhasil Di Approved.")
			->set_callback("                                    
				$('#main-dialog-confirm').modal('toggle');
				tablePerhitungan.ajax.reload(null, false);
			")
			->return(true);
		return;
	}
	
	public function cetak($args = null){
		$id = (int) $args[0];
		$rs_data = $this->model->getRowPayroll(compact('id'));
		if($rs_data->num_rows() == 0){
			echo "<script>alert('Data Tidak Valid.');</script>";
			return;
		}
		$rs_data = $rs_data->row();
		$data_pegawai = $this->pegawai->getRowData(['id'=>$rs_data->id_pegawai]);
		if($data_pegawai->num_rows() == 0){
			echo "<script>alert('Data Pegawai Tidak Valid.');</script>";
			return;
		}
		$data_pegawai = $data_pegawai->row();
		$rs_data->nama = $data_pegawai->nama;
		$rs_data->jabatan = $data_pegawai->jabatan;
		
		$this->load->library('pdf', array("orientation"=>"P", "unit"=>"mm", "size"=>[100,100]));
		$this->pdf->SetPaddings(0);
		$this->pdf->SetMargins(2, 2, 2);
		$this->pdf->SetHeader("_header_pdf", $rs_data);
		
		$this->pdf->AliasNbPages();
		$this->pdf->AddPage();
		
		$pdf = $this->pdf;
		
		$pdf->SetFont('times', '', 9);
		$pdf->RowWidths(array(96))
			->RowAligns(array('L'))
			->RowContents(array("A. Upah Tetap"));
		$pdf->RowWidths(array(37,3,56))
			->RowAligns(array('L','L','R'))
			->RowContents(array('- Gaji Pokok',':',"Rp. ".number_format($rs_data->gaji_pokok, 2, ',', '.')))
			->RowContents(array('- Tunjangan',':',"Rp. ".number_format($rs_data->tunjangan, 2, ',', '.')));
		$pdf->RowWidths(array(96))
			->RowAligns(array('L'))
			->RowContents(array("B. Upah Tidak Tetap"));
		$pdf->RowWidths(array(37,3,56))
			->RowAligns(array('L','L','R'))
			->RowContents(array('- Lembur',':',"Rp. ".number_format($rs_data->lembur, 2, ',', '.')));
		$pdf->RowWidths(array(96))
			->RowAligns(array('L'))
			->RowContents(array("C. Potongan"));
		$pdf->RowWidths(array(37,3,56))
			->RowAligns(array('L','L','R'))
			->RowContents(array('- BPJS',':',"Rp. ".number_format($rs_data->bpjs, 2, ',', '.')))
			->RowContents(array('- NWNP',':',"Rp. ".number_format($rs_data->nwnp, 2, ',', '.')));
		$pdf->SetY($pdf->GetY() + 3);
		$pdf->RowWidths(array(37,3,56))
			->RowAligns(array('L','L','R'))
			->RowContents(array('Total Penerimaan',':',"Rp. ".number_format($rs_data->total_penerimaan, 2, ',', '.')));
		$pdf->Output("I", "Slip_Gaji.pdf");
	}
	
	public function _header_pdf($pdf, $args = null){
		if($pdf->PageNo() == 1){
			$pdf->SetFont('Times', 'B', 12);
			$pdf->SetLineHeight(5)
				->RowAligns(array('C'))
				->RowContents(array('PT. Mekar Jaya'));
			$pdf->SetLineWidth(0.3);
			$pdf->SetY($pdf->GetY() + 5);
			
			
			$pdf->SetFont('Times', '', 9);
			$pdf->RowWidths(array(12,3,81))
				->RowAligns(array('L','L','L'))
				->RowContents(array('Nama',':',$args->nama))
				->RowContents(array('Jabatan',':',ucwords(strtolower($args->jabatan))))
				->RowContents(array('Periode',':',ucwords(strtolower(get_nama_bulan($args->bulan/1))).' '.$args->tahun));
			$pdf->SetY($pdf->GetY() + 3);
		}
	}
}
