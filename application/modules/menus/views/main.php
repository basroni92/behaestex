<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-header">
				<i class="fa fa-bars"></i>&nbsp;<strong>Data Menu</strong>
				<div class="btn-group float-right show">
					<span><a id="refresh" class="card-header-action btn" title="Muat Ulang"><i class="fa fa-refresh"></i></a></span>
				</div>
			</div>
			<div class="card-body">
				<table id="table-menu" width="100%"></table>
			</div>
		</div>
	</div>
</div>

<!-- MODAL ADD -->
<form id="form-modal">
	<div class="modal fade modal-success" id="add_" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="myModalMenu">Tambah Data</h5>
			<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
			<span aria-hidden="true"><i class="fa fa-close"></i></span>
		  </div>
		  <div class="modal-body">
				<div class="form-group row">
					<label class="col-md-3 col-form-label" for="text-input">Nama Menu</label>
					<div class="col-md-9">
					  <input class="form-control" id="nama_menu" type="text" name="nama_menu" maxlength="50" onclick="this.select()" placeholder="Nama Menu" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="id_gmenu" class="col-3 col-sm-3 col-md-3">Group Menu</label>
					<div class="col-9 col-sm-9 col-md-9 input-group">
						<input type="hidden" id="id_gmenu" name="id_gmenu">
						<input type="text" id="nama_gmenu" name="nama_gmenu" onclick="this.select()" class="form-control">
						<div class="input-group-append">
							<span id="find-group-menu" class="input-group-text bg-success pointer" data-toggle="modal" data-target="#find-group-menu-modal">
								<i class="fa fa-search"></i>
							</span>
						</div>
					</div>
					
				</div>
				<div class="form-group row">
					<label for="file_menu" class="col-3 col-sm-3 col-md-3">File Menu</label>
					<div class="col-9 col-sm-9 col-md-9 input-group">
						<input type="text" id="file_menu" name="file_menu" maxlength="50" onclick="this.select()" class="form-control">
						<div class="input-group-append">
							<span id="find-file-menu" class="input-group-text bg-success pointer" data-toggle="modal" data-target="#find-file-menu-modal">
								<i class="fa fa-search"></i>
							</span>
						</div>
					</div>
				</div>
		  </div>
		  <div id="view" class="modal-footer">
			<button type="button" id="save" class="btn btn-secondary" data-dismiss="modal">Batal</button>
			<button type="button" type="submit" id="btn_save" class="btn btn-success text-white add__">Simpan</button>
		  </div>
		</div>
	  </div>
	</div>
</form>	
<!--END MODAL ADD-->

<!-- MODAL EDIT -->
<form id="form-modal">
	<div class="modal fade modal-warning" id="edit_" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Edit Data</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
					<span aria-hidden="true"><i class="fa fa-close"></i></span>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-md-3 col-form-label" for="text-input">Nama Menu</label>
						<div class="col-md-9
						  <input type="hidden" id="eid_menu" name="eid_menu" value="">
						  <input class="form-control" id="enama_menu" type="text" name="enama_menu" onclick="this.select()" maxlength="50" placeholder="Nama Menu" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="eid_gmenu" class="col-3 col-sm-3 col-md-3">Group Menu</label>
						<div class="col-9 col-sm-9 col-md-9 input-group">
							<input type="hidden" id="eid_gmenu" name="eid_gmenu">
							<input type="text" id="enama_gmenu" name="enama_gmenu" onclick="this.select()" class="form-control">
							<div class="input-group-append">
								<span id="find-group-menu" class="input-group-text bg-warning pointer" data-toggle="modal" data-target="#find-group-menu-modal">
									<i class="fa fa-search"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label for="efile_menu" class="col-3 col-sm-3 col-md-3">File Menu</label>
						<div class="col-9 col-sm-9 col-md-9 input-group">
							<input type="text" id="efile_menu" name="efile_menu" maxlength="50" onclick="this.select()" class="form-control">
							<div class="input-group-append">
								<span id="find-file-menu-edit" class="input-group-text bg-warning pointer" data-toggle="modal" data-target="#find-file-menu-modal">
									<i class="fa fa-search"></i>
								</span>
							</div>
							<div class="invalid-feedback">Test</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" type="submit" id="btn_update" class="btn btn-warning text-white">Update</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!--END MODAL EDIT-->

<!-- Find Modal Group Menu -->
<div id="find-group-menu-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">
					<i class="fa fa-bars"></i> Group Menu
				</h5>
				<span class="pointer" title="Muat Ulang"><i class="fa fa-refresh"></i></span>
			</div>
			<div class="modal-body">
				<table id="table-group-menu" style="width:100%"></table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- End Find Modal Group Menu -->

<!-- Find Modal File Menu -->
<div id="find-file-menu-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">
					<i class="fa fa-bars"></i> File Menu
				</h5>
				<span class="pointer" title="Muat Ulang"><i class="fa fa-refresh"></i></span>
			</div>
			<div class="modal-body">
				<table id="table-file-menu" style="width:100%"></table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- End Find Modal File Menu -->
		
<script type="text/javascript">
	var tableMenu = $('#table-menu'),
		base_url = '<?=base_url()?>',
		current_module = base_url +'<?=$this->app->getCurrentModule()?>',
		acl_list = <?=$this->app->get_allow_acl(true);?>,
		defaultAction = (data) => {
			let all_action = '';
			if(is_allow('edit', acl_list))
				all_action += '<a class="btn btn-warning btn-sm item_edit '+(data.status_menu != '0' ? 'disabled' : '')+'" title="Edit Data"><i class="fa fa-edit text-white"></i></a> ';
			if(is_allow('hapus', acl_list))
				all_action += '<a class="btn btn-danger btn-sm item_delete '+(data.status_menu === '2' ? 'disabled' : '')+'" title="Non Aktif Data"><span class="btn-hapus"><i class="fa fa-trash text-white"></i></span></a> ';
			if(is_allow('approve1', acl_list))
				all_action += '<a class="btn btn-sm btn-primary item_approve1 '+(data.status_menu === '0' ? '' : 'disabled')+'" title="Aktif Data"><span class="btn-approve"><i class="fa fa-lock text-white"></i></span></a> ';			
			action_histori = '<a class="btn btn-dark btn-sm item_histori" title="Histori Data"><span class="btn-histori"><i class="fa fa-history text-white"></i></span></a>';
			return all_action + action_histori;
		},
		refresh = $('#refresh'),
		optionsMenu = {
			dom: "<'#row-header.row'<'col-sm-12 col-md-3 col-lg-2'><'col-sm-12 col-md-3 col-lg-5'><'col-sm-12 col-md-6 col-lg-5'f>>" +
				"<'row'<'col-12 col-sm-12 col-md-12'<'table-responsive'tr>>>" +
				"<'row'<'col-12 col-sm-12 col-md-3 col-lg-2'l><'col-12 col-sm-12 col-md-4 col-lg-6'i><'col-12 col-sm-12 col-md-5 col-lg-4'p>>",
			processing: true,
			serverSide: true,
			sPaginationType: 'full_numbers',
			aaSorting: [0, 'asc'],
			ajax: {
				url: current_module+'/index/result_data', 
				type: 'POST',
				data: function(data){
					let form = $('#table-menu_filter'),
						status_menu = form.find('#filter_status').val() || '-',
						params = $.extend(true, data, {status_menu});
					return params;
				}
			},
			initComplete: function(){
				if(is_allow('tambah', acl_list)){
					let button_add = $("button[data-target='#add_']");
					if(button_add.length > 0)
						button_add.remove();
					$('<button/>')
						.addClass('btn btn-success btn-block')
						.attr({'data-toggle':'modal', 'id':'addClick_', 'data-target':'#add_'})
						.html('<i class="fa fa-plus"></i> Tambah Data')
						.appendTo($('#row-header > div:first'));
				}
				let table = $(this),
					tableId = table[0].id,
					data = [{'label': 'Status', 'id':'filter_status', 'value': <?=get_status(true)?>}];
				
				// Create Filter
				create_filter(tableId, data);
				
				// Search Auto Focus
				$('#'+tableId+'_filter input[type="search"]')[0].focus();
				
				// Refresh View
				$('#refresh').off().on('click', function(e){
					e.preventDefault();
					table.DataTable().ajax.reload();
				});
			},
			columns: [
				{'title': 'Nama Menu', 'width': '25%', data: 'nama_menu'},
				{'title': 'Group Menu', 'width': '25%', data: 'nama_gmenu',
					render: (data, type, row) => {
						if(type === 'display')
							return '<i class="nav-icon icon-'+(row.icon_gmenu === '' ? 'list' : row.icon_gmenu.toLowerCase())+'"/>&nbsp;&nbsp;'+data;
						return data;
					}
				},
				{'title': 'File Menu', 'width': '20%', data: 'file_menu'},
				{'title': 'Status', 'width': '15%', data: 'nama_status_menu', 'searchable': false,
					render: (data, type, row) => {
						return type === 'display' ? displayStatusMaster(row.status_menu) : data;
					}
				},
				{'title': '', 'width': '15%', data: null, 'orderable': false, 'searchable': false, className: 'text-center', 
					render: (data, type, row) => {
						return type === 'display' ? defaultAction(row) : data;
					}
				}
			],
			createdRow: (row, data, index) => {
				$(row).find('.item_edit').data(data);
				$(row).find('.item_delete').data('menu_id', data.id);
				$(row).find('.item_approve1').data('menu_id', data.id);
				$(row).find('.item_histori').data('histori_id', data.histori);
			}
		},
		optionsFindGroupMenu = create_find({
			ajax: {url: current_module+'/index/data_find_group_menu'},
			columns: [
				{title: 'ID', data: 'id', className: 'pointer', visible:false},
				{title: 'GROUP MENU', data: 'nama_gmenu', className: 'pointer'}
			]
		}),
		optionsFindFileMenu = create_find({
			ajax: {url: current_module+'/index/data_file_menu'},
			columns: [{title: 'FILE MENU', data: 'file_menu', className: 'pointer'}]
		});
    tableMenu = tableMenu.DataTable(optionsMenu),
	action = {
		acl_list : {
			tambah: function(){
				//auto complete add
				$('#add_').on('shown.bs.modal', function(){
					$("#nama_gmenu" ).autocomplete({
						minLength: 2,
						autoFocus: true,
						source: function(request, response){
							$.ajax({
								url: current_module+'/index/data_group_menu',
								dataType: "json",
								type: "POST",
								data: {searchText: request.term},
								success: function(data){
									response($.map(data, function(item){
										return {id: item.id, label: item.name};
									}));
								}
							});
						},
						select: function(event, data){
							$('#id_gmenu').val(data.item.id);
							$('#nama_gmenu').val(data.item.label);
							return false;
						},
						change: function(event, data){
							if(data.item === null)
								$('#id_gmenu, #nama_gmenu').val('')
						}
					});
					$("#file_menu").autocomplete({
						minLength: 2,
						autoFocus: true,
						source: function(request, response){
							$.ajax({
								url: current_module+'/index/data_modul_menu',
								dataType: "json",
								type: "POST",
								data: {searchText: request.term},
								success: function(data){
									response($.map(data, function(item){
										return {id: item.id, label: item.name};
									}));
								}
							});
						},
						select: function(event, data){
							$(this).val(data.item.label);
							return false;
						},
						change: function(event, data){
							if(data.item === null)
								$(this).val('')
						}
					});
				});
				//close modal add
				$('#add_').on('hidden.bs.modal', function(){
					$('#nama_menu').val('');
					$('#file_menu').val('');
					$('#id_gmenu').val('');
					$('#nama_gmenu').val('');
				});
				//close modal show
				$('#show_').on('hidden.bs.modal', function(){
					alert("show");
					var e = document.getElementById("add_");
					e.id = "show_";
					$('#nama_menu').val('');
					$('#file_menu').val('');
				});
				//save
				$('#btn_save').on('click',function(){
					var id_gmenu 	= $('#id_gmenu').val(),
						nama_menu 	= $('#nama_menu').val(),
						file_menu   = $('#file_menu').val();					
					load_url(current_module+'/tambah', {id_gmenu, nama_menu, file_menu});
				});
			},
			edit: function(){
				//set value edit data
				tableMenu.on('click','.item_edit',function(){
					let data = $(this).data();
					let eid_menu 	= data.id,
						enama_menu 	= data.nama_menu,
						eid_gmenu	= data.id_gmenu,
						enama_gmenu	= data.nama_gmenu,
						efile_menu	= data.file_menu;
					
					$('#edit_').modal('show');
					$('#eid_menu').val(eid_menu);
					$('#enama_menu').val(enama_menu);
					$('#eid_gmenu').val(eid_gmenu);
					$('#enama_gmenu').val(enama_gmenu);
					$('#efile_menu').val(efile_menu);
					
					$('#edit_').on('hidden.bs.modal', function(){
						$('#eid_menu').val('');
						$('#enama_menu').val('');
						$('#eid_gmenu').val('');
						$('#enama_gmenu').val('');
						$('#efile_menu').val('');
					});
				});
				//edit
				$('#btn_update').on('click',function(){
					var eid_menu 		= $('#eid_menu').val(),
						eid_gmenu 		= $('#eid_gmenu').val(),
						enama_menu 		= $('#enama_menu').val(),
						efile_menu   	= $('#efile_menu').val();
					load_url(current_module+'/edit', {eid_menu,eid_gmenu,enama_menu,efile_menu});
					return false;
				});
				//auto complete edit
				$('#edit_').on('shown.bs.modal', function(){
					let elements = $(this);
					event_focus(elements);
					$("#enama_gmenu" ).autocomplete({
						minLength: 2,
						autoFocus: true,
						source: function(request, response){
							$.ajax({
								url: current_module+'/index/data_group_menu',
								dataType: "json",
								type: "POST",
								data: {searchText: request.term},
								success: function(data){
									response($.map(data, function(item){
										return {id: item.id, label: item.name};
									}));
								}
							});
						},
						select: function(event, data){
							$('#eid_gmenu').val(data.item.id);
							$('#enama_gmenu').val(data.item.label);
							return false;
						},
						change: function(event, data){
							if(data.item === null)
								$('#eid_gmenu, #enama_gmenu').val('')
						}
					});
					$("#efile_menu").autocomplete({
						minLength: 2,
						autoFocus: true,
						source: function(request, response){
							$.ajax({
								url: current_module+'/index/data_modul_menu',
								dataType: "json",
								type: "POST",
								data: {searchText: request.term},
								success: function(data){
									response($.map(data, function(item){
										return {id: item.id, label: item.name};
									}));
								}
							});
						},
						select: function(event, data){
							$(this).val(data.item.label);
							return false;
						},
						change: function(event, data){
							if(data.item === null)
								$(this).val('')
						}
					});
				});
			},
			hapus: function(){
				tableMenu.on('click','.item_delete',function(e){
					e.preventDefault();
					var id = $(this).data('menu_id')
						body = 'Data akan dihapus, apakah anda yakin ?',
						action = {
							'url': current_module+'/hapus',
							'data': {id}
						};
					confirm_dialog(action, body, {'value':'Hapus', 'class':'danger'});
				});
			},
			approve1: function(){
				tableMenu.on('click','.item_approve1',function(e){
					e.preventDefault();
					var id = $(this).data('menu_id'),
						body = 'Data akan diapprove, apakah anda yakin ?',
						action = {
							'url': current_module+'/approve1',
							'data': {id}
						};
					confirm_dialog(action, body, {'value':'Approve', 'class':'primary'});
				});
			},
		},
		setFocusFirstElementAdd : function(){
			$('#add_').on('shown.bs.modal', function () {
				let elements = $(this);
				event_focus(elements);
			})
		},
		findGroupMenu : function(){
			$('#find-group-menu, #find-group-menu-edit').off().on('click', function(e){
				e.preventDefault();
				let tableGroupMenu = $('#table-group-menu').DataTable(optionsFindGroupMenu);
			});
			
			$('#table-group-menu').off().on('click', 'tbody tr', function(e){
				e.preventDefault();
				let data = $(this).data();
				if($('#add_').length > 0) {
					$('#id_gmenu').val(data.id);
					$('#nama_gmenu').val(data.nama_gmenu);
				}
				if($('#edit_').length > 0) {
					$('#eid_gmenu').val(data.id);
					$('#enama_gmenu').val(data.nama_gmenu);
				}

				$('#find-group-menu-modal').modal('toggle');
			});
		},
		findFileMenu : function(){
			$('#find-file-menu, #find-file-menu-edit').off().on('click', function(e){
				e.preventDefault();
				let tableFileMenu = $('#table-file-menu').DataTable(optionsFindFileMenu);
			});
			
			$('#table-file-menu').off().on('click', 'tbody tr', function(e){
				e.preventDefault();
				let data = $(this).data();
				if($('#add_').length > 0) {
					$('#file_menu').val(data.file_menu);
				}
				if($('#edit_').length > 0) {
					$('#efile_menu').val(data.file_menu);
				}
				$('#find-file-menu-modal').modal('toggle');
			});
		},
		histori : function(){
			tableMenu.on('click', '.item_histori', function(e){
				e.preventDefault();
				var id_histori = $(this).data('histori_id');
				show_histori(id_histori);
			});
		},
	};
	
	action.acl_list.tambah();
	action.acl_list.edit();
	action.acl_list.hapus();
	action.acl_list.approve1();
	
	action.setFocusFirstElementAdd();
	action.findGroupMenu();
	action.findFileMenu();
	action.histori();
</script>