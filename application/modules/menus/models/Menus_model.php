<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Menus_model extends CI_Model {
	function __construct () {
        parent::__construct();
	}
	
	function getData($offset, $page_size, $sort, $filter = null, $search = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS
				a.id,
				a.id_gmenu,
				a.nama_menu,
				b.nama_gmenu,
				b.icon_gmenu,
				a.file_menu,
				a.status_menu,
				CASE a.status_menu
				WHEN '0' THEN 'BARU'
				WHEN '1' THEN 'AKTIF'
				WHEN '2' THEN 'NON AKTIF'
				END AS nama_status_menu,
				CONCAT(a.nama_menu, ' ', b.nama_gmenu, ' ', a.file_menu, ' ', IF(a.status_menu = '0', 'BARU', IF(a.status_menu = '1', 'AKTIF', 'NON AKTIF'))) AS search,
				MD5 (CONCAT('menu', '-', a.id)) AS histori", false)
			->from("menu a")
			->join("gmenu b", "b.id=a.id_gmenu", "INNER");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function groupMenu($offset, $page_size, $sort, $filter = null, $search = null, $autoComplete = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS a.id, a.nama_gmenu, a.status_gmenu, CONCAT(a.nama_gmenu, ' ', a.status_gmenu) AS search", false)
			->from("gmenu a");
		if(!empty($filter)){
			foreach($filter as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		if(!empty($autoComplete)){
			$this->db->like("a.nama_gmenu", $autoComplete);
			return $this->db->order_by('nama_gmenu')->get();
		}else{
			return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
		}
	}
	
	function getAllFileMenu($offset, $page_size, $sort, $filter = null, $search = null, $autoComplete = null){
		$this->load->library('Menu');
		$all_modules 	= array();
		$all_modules 	= $this->menu->get_all_menus();
		$query_module 	= "";
		foreach($all_modules as $module){
			$query_module .= $query_module <> "" ? " UNION ALL SELECT '{$module}'" : "SELECT '{$module}' AS file_menu";
		}
		$this->db
			->select("SQL_CALC_FOUND_ROWS UPPER(a.file_menu) AS file_menu, a.file_menu AS search", false)
			->from("({$query_module}) a")
			->join("menu b", "b.file_menu = CAST(a.file_menu AS CHAR) AND b.status_menu <> '2'", "LEFT");
			$this->db->where("b.file_menu", null);
		if(!empty($autoComplete)){
			$this->db->like("a.file_menu", $autoComplete);
			return $this->db->order_by('a.file_menu')->get();
		}else{
			return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
		}	
	}
	
	function getRowMenu($filters = null){
		$this->db->from("menu a");
		if(!empty($filters)){
			foreach($filters as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->get();
	}
	
	function getRowModule($filters = null){
		$this->load->library('Menu');
		$all_modules = $this->menu->get_all_menus();
		$query_module = "";
		foreach($all_modules as $module){
			$query_module .= $query_module <> "" ? " UNION ALL SELECT '{$module}'" : "SELECT '{$module}' AS file_menu";
		}
		
		$this->db->from("({$query_module}) a")
			->join("menu b", "b.file_menu = CAST(a.file_menu AS CHAR) AND b.status_menu <> '2'", "LEFT");
		if(!empty($filters)){
			foreach($filters as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->order_by('a.file_menu')->get();
	}
	
	function getRowGroupMenu($filter = null){
		$this->db->from("gmenu a");
		if(!empty($filter)){
			foreach($filter as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->order_by('nama_gmenu')->get();
	}
	
	function add($data){
		return $this->db->insert('menu', $data);
	}
	
	function edit($data, $id){
		return $this->db->where("id", $id)->update('menu', $data);
	}
	
	function delete_($id){
		return $this->db->set("status_menu", '2')->where("id", $id)->update('menu');
	}
	
	function approve($id){
		return $this->db->set("status_menu", '1')->where("id", $id)->update('menu');
	}
}
