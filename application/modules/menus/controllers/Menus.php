<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Menus extends MX_Controller {
	
    public function __construct() {
        parent::__construct();
		$this->load->model('Menus_model', 'model');
    }
	
	protected function _middleware(){
        return array('auth', 'access');
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    public function index($args = null){
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[$order][data]")." ".$order_by;
		$filter 	= null;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		
		switch($args[0]){
			case 'result_data':
				$filter['status_menu']	= addslashes(trim($this->input->post("status_menu")));
				$data_result = $this->model->getData($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			case 'data_find_group_menu':
				$filter	= array("a.status_gmenu"=>"1");
				$data_result = $this->model->groupMenu($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			case 'data_file_menu':
				$data_result = $this->model->getAllFileMenu($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			case 'data_group_menu':
				$autoComplete = trim($this->input->post("searchText"));
				$data_result = $this->getGroupMenu($autoComplete);
				echo json_encode($data_result);
				break;
			case 'data_modul_menu':
				$autoComplete = trim($this->input->post("searchText"));
				$data_result = $this->getLoadModuleMenu($autoComplete);
				echo json_encode($data_result);
				break;
			default:
				$data = array("lintasModulGroupMenu"=> $this->app->is_allow_access('tambah', 'group_menu'));
				$this->load->view("main", $data);
		}
    }
	
	public function tambah($args = null){
		$nama_menu	= trim(strtoupper($this->input->post('nama_menu')));
		$file_menu	= trim(strtoupper($this->input->post('file_menu')));
		$id_gmenu	= (int) $this->input->post('id_gmenu');
		
		if($nama_menu == "")
			return $this->response->set_message("Nama Menu Kosong.")->return(true);
		
		/** Cek Group Menu **/
		if($id_gmenu == 0)
			return $this->response->set_message("Group Menu Kosong.")->return(true);
		$data_gmenu = $this->model->getRowGroupMenu(array("id"=>$id_gmenu));
		if($data_gmenu->num_rows() == 0)
			return $this->response->set_message("Group Menu Tidak Valid.")->return(true);
		$data_gmenu = $data_gmenu->row();
		
		if($data_gmenu->status_gmenu == "0")
			return $this->response->set_message("Group Menu Belum Aktif.")->return(true);
		
		if($data_gmenu->status_gmenu == "2")
			return $this->response->set_message("Group Menu Sudah Non Aktif.")->return(true);
		
		if($file_menu == "")
			return $this->response->set_message("File Menu Kosong.")->return(true);
		$data_module = $this->model->getRowModule(array("a.file_menu"=>$file_menu));
		if($data_module->num_rows() == 0)
			return $this->response->set_message("File Menu Tidak Valid.")->return(true);
		
		$this->db->trans_begin();
		if(!$this->model->add(compact('nama_menu','file_menu','id_gmenu'))){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Insert Menu")->return(true);
		}
		$id = $this->db->insert_id();
		$this->app->save_histori('ADD MENU', 'menu', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Menu Berhasil Di Tambahkan.")
			->set_callback("
				$('#add_').modal('toggle');
				$('#id_gmenu, #nama_menu, #file_menu').val('');
				tableMenu.ajax.reload();
			")
			->return(true);
		return;
	}
	
    public function edit($args = null){
		$id			= (int)$this->input->post('eid_menu');
		$nama_menu	= trim(strtoupper($this->input->post('enama_menu')));
		$file_menu	= trim(strtoupper($this->input->post('efile_menu')));
		$id_gmenu	= (int) $this->input->post('eid_gmenu');
		
		$data_menu = $this->model->getRowMenu(compact('id'));
		if($data_menu->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$data_menu = $data_menu->row();
		
		if($data_menu->status_menu == "1")
			return $this->response->set_message("Menu Sudah Aktif.")->return(true);
		
		if($data_menu->status_menu == "2")
			return $this->response->set_message("Menu Sudah Non Aktif.")->return(true);
		
		if($nama_menu == "")
			return $this->response->set_message("Nama Menu Kosong.")->return(true);
		
		/** Cek Group Menu **/
		if($id_gmenu == 0)
			return $this->response->set_message("Group Menu Kosong.")->return(true);
		$data_gmenu = $this->model->getRowGroupMenu(array("id"=>$id_gmenu));
		if($data_gmenu->num_rows() == 0)
			return $this->response->set_message("Group Menu Tidak Valid.")->return(true);
		$data_gmenu = $data_gmenu->row();
		
		if($data_gmenu->status_gmenu == "0")
			return $this->response->set_message("Group Menu Belum Aktif.")->return(true);
		
		if($data_gmenu->status_gmenu == "2")
			return $this->response->set_message("Group Menu Sudah Non Aktif.")->return(true);
		
		/** Cek File Menu **/
		if($file_menu == "")
			return $this->response->set_message("File Menu Kosong.")->return(true);
		$data_module = $this->model->getRowModule(array("a.file_menu"=>$file_menu));
		if($data_module->num_rows() == 0)
			return $this->response->set_message("File Menu Tidak Valid.")->return(true);
		
		$this->db->trans_begin();
		if(!$this->model->edit(compact('nama_menu', 'file_menu', 'id_gmenu'), $id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Update Menu")->return(true);
		}
		$this->app->save_histori('EDIT MENU', 'menu', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Menu Berhasil Di Edit.")
			->set_callback("
				$('#edit_').modal('toggle');
				tableMenu.ajax.reload(null, false);
			")
			->return(true);
		return;
	}
	
	public function hapus($args = null){
		$id = (int)$this->input->post('id');
		$data_menu = $this->model->getRowMenu(compact('id'));
		if($data_menu->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$data_menu = $data_menu->row();
		
		if($data_menu->status_menu == "2")
			return $this->response->set_message("Menu Sudah Non Aktif.")->return(true);

		$this->db->trans_begin();
		if($data_menu->status_menu == '0'){
			$data=$this->model->delete_($id);
			if(!$data){
				$this->db->trans_rollback();
				return $this->response->set_message("Error Delete Menu.")->return(true);
			}
			/* history */
			$this->app->save_histori('DEL MENU', 'menu', $id);
			
			$this->db->trans_commit();
			$this->response
				->set_result('OK')
				->set_message("Menu Berhasil Di Non-Aktifkan.")
				->set_callback("
					$('#main-dialog-confirm').modal('toggle');
					tableMenu.ajax.reload(null, false);
				")
				->return(true);
			return;
		}else if($data_menu->status_menu == '1'){
			$data=$this->model->edit(array("status_menu"=>'0'), $id);
			if(!$data){
				$this->db->trans_rollback();
				return $this->response->set_message("Error Update Menu")->return(true);
			}
			/* history */
			$this->app->save_histori('CANCEL APP MENU', 'menu', $id);
			
			$this->db->trans_commit();
			$this->response
				->set_result('OK')
				->set_message("Menu Berhasil Di Batal Aktifkan.")
				->set_callback("
					$('#main-dialog-confirm').modal('toggle');
					tableMenu.ajax.reload(null, false);
				")
				->return(true);
			return;
		}
	}
	
	public function approve1($args = null){
		$id = (int) $this->input->post('id');
		
		$data_menu = $this->model->getRowMenu(compact('id'));
		if($data_menu->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$data_menu = $data_menu->row();
		
		if($data_menu->status_menu == "1")
			return $this->response->set_message("Menu Sudah Aktif.")->return(true);
		
		if($data_menu->status_menu == "2")
			return $this->response->set_message("Menu Sudah Non Aktif.")->return(true);
		
		$data_module = $this->model->getRowModule(array("a.file_menu"=>$data_menu->file_menu))->result_array();
		if(count($data_module)>=2)
			return $this->response->set_message("Data File Menu Sudah ada.")->return(true);
		
		$this->db->trans_begin();
		if(!$this->model->approve($id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Approve Menu.")->return(true);
		}
		
		/* history */
		$this->app->save_histori('APP MENU', 'menu', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Menu Berhasil Di Aktifkan.")
			->set_callback("                                    
				$('#main-dialog-confirm').modal('toggle');
				tableMenu.ajax.reload(null, false);
			")
			->return(true);
		return;
	}
			
	private function getLoadModuleMenu($autoComplete){
		$loadModuleData = $this->model->getAllFileMenu(null, null, null, null, null, $autoComplete);
		if(!$loadModuleData) return [];
		return array_map(function($loadModule){
			return (object) [
				"id" => $loadModule->file_menu,
				"name" => $loadModule->file_menu,
			];
		}, $loadModuleData->result());
	}
	
	private function getGroupMenu($autoComplete){
		$filter	= array("a.status_gmenu"=>"1");
		$groupMenuData = $this->model->groupMenu(null, null, null, $filter, null, $autoComplete);
		if(!$groupMenuData) return [];
		return array_map(function($groupMenu){
			return (object) [
				"id" => $groupMenu->id,
				"name" => $groupMenu->nama_gmenu,
			];
		}, $groupMenuData->result());
	}
}
