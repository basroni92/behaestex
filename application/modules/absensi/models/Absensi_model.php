<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Absensi_model extends CI_Model {
	function __construct () {
        parent::__construct();
	}
	
	function getAllData($offset, $page_size, $sort, $filter = null, $search = null){
		$bulan = (int) $filter['bulan'];
		$tahun = (int) $filter['tahun'];
		unset($filter['bulan'], $filter['tahun']);
		$this->db->query("SET @tahun := ?, @bulan := ?;", [$tahun, $bulan]);
		$this->db
			->select("SQL_CALC_FOUND_ROWS
					a.id_pegawai,
					a.nama,
					a.jabatan,
					MAX(IF(a.tgl=1 , a.info, '')) AS tgl1,
					MAX(IF(a.tgl=2 , a.info, '')) AS tgl2,
					MAX(IF(a.tgl=3 , a.info, '')) AS tgl3,
					MAX(IF(a.tgl=4 , a.info, '')) AS tgl4,
					MAX(IF(a.tgl=5 , a.info, '')) AS tgl5,
					MAX(IF(a.tgl=6 , a.info, '')) AS tgl6,
					MAX(IF(a.tgl=7 , a.info, '')) AS tgl7,
					MAX(IF(a.tgl=8 , a.info, '')) AS tgl8,
					MAX(IF(a.tgl=9 , a.info, '')) AS tgl9,
					MAX(IF(a.tgl=10, a.info, '')) AS tgl10,
					MAX(IF(a.tgl=11, a.info, '')) AS tgl11,
					MAX(IF(a.tgl=12, a.info, '')) AS tgl12,
					MAX(IF(a.tgl=13, a.info, '')) AS tgl13,
					MAX(IF(a.tgl=14, a.info, '')) AS tgl14,
					MAX(IF(a.tgl=15, a.info, '')) AS tgl15,
					MAX(IF(a.tgl=16, a.info, '')) AS tgl16,
					MAX(IF(a.tgl=17, a.info, '')) AS tgl17,
					MAX(IF(a.tgl=18, a.info, '')) AS tgl18,
					MAX(IF(a.tgl=19, a.info, '')) AS tgl19,
					MAX(IF(a.tgl=20, a.info, '')) AS tgl20,
					MAX(IF(a.tgl=21, a.info, '')) AS tgl21,
					MAX(IF(a.tgl=22, a.info, '')) AS tgl22,
					MAX(IF(a.tgl=23, a.info, '')) AS tgl23,
					MAX(IF(a.tgl=24, a.info, '')) AS tgl24,
					MAX(IF(a.tgl=25, a.info, '')) AS tgl25,
					MAX(IF(a.tgl=26, a.info, '')) AS tgl26,
					MAX(IF(a.tgl=27, a.info, '')) AS tgl27,
					MAX(IF(a.tgl=28, a.info, '')) AS tgl28,
					MAX(IF(a.tgl=29, a.info, '')) AS tgl29,
					MAX(IF(a.tgl=30, a.info, '')) AS tgl30,
					MAX(IF(a.tgl=31, a.info, '')) AS tgl31,
					CONCAT(a.nama, ' ', a.jabatan) AS search
				FROM (
					SELECT 
						a.id_pegawai,
						DAY(a.tanggal) AS tgl,
						b.nama,
						b.jabatan,
						a.lembur,
						IF((a.st = 0 AND !(DATE_FORMAT(a.tanggal,'%w') = 0 OR DATE_FORMAT(a.tanggal,'%w') = 6)), 
							2, IF(a.st = 0, 1, 0)
						) AS info
						-- 2: NWNP, '1': LIBUR, 0: MASUK
					FROM (
						SELECT 
							a.id_pegawai, 
							DATE(CONCAT(@tahun, LPAD(@bulan,2,0), LPAD(a.tgl, 2, 0))) AS tanggal,
							MAX(a.lembur) AS lembur,
							MAX(a.st) AS st
						FROM (
							SELECT a.id_pegawai, a.tgl, 0 AS lembur, 0 AS st
							FROM (
								SELECT b.id AS id_pegawai, a.tgl
								FROM (
									SELECT a.n+(b.n*10) AS tgl
									FROM (
										SELECT 0 AS n UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 
									) a
									JOIN (
										SELECT 0 AS n UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3
									) b 
									HAVING tgl BETWEEN 1 AND DAY(LAST_DAY(CONCAT(@tahun, '-', LPAD(@bulan,2,0), '-01')))
									ORDER BY tgl
								) a
								JOIN pegawai b
								ORDER BY b.id, a.tgl
								LIMIT 10000000000000000000
							) a
							UNION ALL
							SELECT
								a.id_pegawai,
								DAY(a.tgl_absensi) AS tgl,
								HOUR(TIMEDIFF(TIME('16:30:00'),a.jam_keluar)) AS lembur,
								1 AS st
							FROM absensi a
							WHERE DATE_FORMAT(a.tgl_absensi, '%Y%m') = CONCAT(@tahun, LPAD(@bulan, 2, 0))
							LIMIT 10000000000000000000
						) a
						GROUP BY a.id_pegawai, a.tgl
						ORDER BY a.id_pegawai, a.tgl
						LIMIT 10000000000000000000
					) a
					INNER JOIN pegawai b ON b.id=a.id_pegawai
					ORDER BY a.id_pegawai, a.tanggal
					LIMIT 10000000000000000000
				) a", false)
			->group_by("a.id_pegawai");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
}
