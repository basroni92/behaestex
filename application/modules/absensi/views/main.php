<div class="card">
	<div class="card-header">
		<i class="fa fa-bars"></i>  <strong>Data Absensi</strong>
		<div class="btn-group float-right show">
			<span><a id="refresh-view" class="card-header-action btn" title="Muat Ulang"><i class="fa fa-refresh"></i></a></span>
		</div>
	</div>
	<div class="card-body">
		<table id="table-view" style="width:100%"></table>
	</div>
</div>
<style>
	.info-warna {
		position: relative;
	}
	.info-warna > div {
		position: absolute;
		top: 0;
		right: 0;
	}
	.border_tipis {
		border: unset !important;
		box-shadow: 0 0 1px rgba(0,0,0,.5);
	}
	.box {
		display: inline-block;
		width: 25px;
		height: 25px;
	}
</style>
<script type="text/javascript">
	var current_module = '<?=base_url().$this->app->getCurrentModule()?>',
		acl_list = <?=$this->app->get_allow_acl(true);?>,
		filterBulan = <?=get_month_option(true,false)?>,
		filterTahun = <?=get_year_option(true)?>,
		callback = () => {
			let bulan = $('#filter_bulan').val()/1,
				tahun = $('#filter_tahun').val()/1;
			if($.fn.dataTable.isDataTable($('#table-view'))){
				$('#table-view').DataTable().destroy();
				$('#table-view').empty();
			}
			$('#table-view').DataTable(optionsView(bulan, tahun));
			return false;
		},
		optionsView = (bulan, tahun) => {
			let columns = [
					{title: 'Nama', width: '20%', data: 'nama',
						render: (data, type, row) => {
							if(type !== 'display') return data;
							return '<p class="m-0">'+data.split(' ').join('&nbsp;')+'</p>\
								<p class="m-0 small-font text-primary">'+row.jabatan+'</p>';
						}
					},
				],
				lastDay = new Date(tahun, bulan, 0).getDate();
			for(let i=1; i <= lastDay; i++){
				columns.push({title: ''+i, data: 'tgl'+i, className: 'dt-body-center tgl'+i,
					render: (data, type) => {
						if(type !== 'display') return data;
						return '';
					}
				});
			}
			return {
				dom: "<'#row-header.row'<'col-sm-12 col-md-3 col-lg-2'l><'col-sm-12 col-md-3 col-lg-5 info-warna'><'col-sm-12 col-md-6 col-lg-5'f>>" +
					"<'row'<'col-12 col-sm-12 col-md-12'<'table-responsive'tr>>>" +
					"<'row'<'col-12 col-sm-12 col-md-7 col-lg-7'i><'col-12 col-sm-12 col-md-5 col-lg-5'p>>",
				processing: true,
				serverSide: true,
				destroy: true,
				deferRender: true,
				sPaginationType: 'full_numbers',
				aaSorting: [0, 'asc'],
				ajax: {
					url: current_module+'/index/get_data', 
					type: 'POST',
					data: {bulan, tahun},
				},
				initComplete: function(){
					let self = this,
						id = self[0].id,
						data = [
							{label:'Bulan', id: 'filter_bulan', value: filterBulan, defaultValue: bulan, callback},
							{label:'Tahun', id: 'filter_tahun', value: filterTahun, defaultValue: tahun, callback},
						];
					// Create Filter
					create_filter(id, data);
					
					$('<div class="d-flex align-items-center"><span class="box bg-info"></span>&nbsp;:&nbsp;Masuk&nbsp;\
						<span class="box bg-danger"></span>&nbsp;:&nbsp;Hari Libur&nbsp;\
						<span class="box bg-warning"></span>&nbsp;:&nbsp;NWNP</div>')
						.appendTo($('.info-warna'));
					
					// Set Auto Focus
					$('#'+id+'_filter input[type="search"]').trigger('focus');
					
					// Refresh Box
					$('#refresh-view').off().click(e => self.api().ajax.reload());
				},
				columns,
				createdRow: (row, data) => {
					let $row = $(row);
					$row.find('td').addClass('border_tipis');
					for(let i=1; i <= lastDay; i++){
						if(data['tgl'+i] == '2')
							$row.find('.tgl'+i).addClass('bg-warning');
						else if(data['tgl'+i] == '1')
							$row.find('.tgl'+i).addClass('bg-danger');
						else
							$row.find('.tgl'+i).addClass('bg-info');
					}
				}
			}
		};
	$('#table-view').DataTable(optionsView("<?=date('m')/1;?>", "<?=date('Y')?>"));
</script>