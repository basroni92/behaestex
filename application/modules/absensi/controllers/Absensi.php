<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Absensi extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('Absensi_model', 'model');
    }
	
	protected function _middleware(){
        return array('auth', 'access');
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    public function index($args = null){
		# Data Parameters
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[$order][data]")." ".$order_by;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		$bulan 		= (int) $this->input->post("bulan");
		$tahun 		= (int) $this->input->post("tahun");
		
		switch($args[0]){
			case 'get_data':
				$filter = compact('bulan', 'tahun');
				$data_result = $this->model->getAllData($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			default:
				$this->load->view("main");
		}
    }
	
	public function tambah($args = null){
		
	}
}
