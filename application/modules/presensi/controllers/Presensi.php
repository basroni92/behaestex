<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Presensi extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('Presensi_model', 'model');
		$this->load->model('user/User_model', 'user');
    }
	
	protected function _middleware(){
        return array('auth', 'access');
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    public function index($args = null){
		# Data Parameters
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[$order][data]")." ".$order_by;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		
		switch($args[0]){
			case 'get_data':
				$bulan = (int) $this->input->post("bulan");
				$tahun = (int) $this->input->post("tahun");
				$filter = compact('bulan', 'tahun');
				$data_result = $this->model->getAllData($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			case 'get_pegawai':
				$data_result = $this->model->getAllPegawai($offset, $page_size, $sort, null, $search);
				echo json_encode($data_result);
				break;
			case 'get_suggest_pegawai':
				$params = addslashes(trim($this->input->post('searchText')));
				$data_result = $this->model->getSuggestPegawai($params);
				echo json_encode($data_result);
				break;
			default:
				$this->load->view("main");
		}
    }
	
	public function tambah($args = null){
		$id_pegawai		= (int) $this->input->post('id_pegawai');
		$tgl_absensi	= trim($this->input->post('tgl_absensi'));
		$jam_masuk		= trim($this->input->post('jam_masuk'));
		$jam_keluar		= trim($this->input->post('jam_keluar'));
		
		if($id_pegawai == 0)
			return $this->response->set_message("Nama Pegawai Kosong.")->return(true);
		$rs_pegawai = $this->user->getRowPegawai(['id'=>$id_pegawai]);
		if($rs_pegawai->num_rows() == 0)
			return $this->response->set_message("Nama Pegawai Tidak Valid.")->return(true);
		
		if($tgl_absensi == '')
			return $this->response->set_message("Tanggal Kosong.")->return(true);
		$tgl_absensi = date("Y-m-d", strtotime($tgl_absensi));
		
		# Cek Jika Data Sudah Ada
		$rs_cek = $this->model->getRowData(compact('id_pegawai','tgl_absensi'));
		if($rs_cek->num_rows() > 0)
			return $this->response->set_message("Data Sudah Ada.")->return(true);
		
		if($jam_masuk == '')
			return $this->response->set_message("Jam Masuk Kosong.")->return(true);
		$jam_masuk = date("H:i:s", strtotime($jam_masuk));
		
		if($jam_keluar == '')
			return $this->response->set_message("Jam Keluar Kosong.")->return(true);
		$jam_keluar = date("H:i:s", strtotime($jam_keluar));
		
		$this->db->trans_begin();
		if(!$this->model->addData(compact('id_pegawai','tgl_absensi','jam_masuk','jam_keluar'))){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Insert Presensi.")->return(true);
		}
		$id = $this->db->insert_id();
		$this->app->save_histori('ADD PRESENSI', 'absensi', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Presensi Berhasil Ditambahkan.")
			->set_callback("
				$('#form-modal').modal('toggle');
				tableView.ajax.reload();
			")
			->return(true);
		return;
	}
	
    public function edit($args = null){
		$id 			= (int) $this->input->post('id');
		$id_pegawai		= (int) $this->input->post('id_pegawai');
		$tgl_absensi	= trim($this->input->post('tgl_absensi'));
		$jam_masuk		= trim($this->input->post('jam_masuk'));
		$jam_keluar		= trim($this->input->post('jam_keluar'));
		
		$rs_data = $this->model->getRowData(compact('id'));
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		
		if($id_pegawai == 0)
			return $this->response->set_message("Nama Pegawai Kosong.")->return(true);
		$rs_pegawai = $this->user->getRowPegawai(['id'=>$id_pegawai]);
		if($rs_pegawai->num_rows() == 0)
			return $this->response->set_message("Nama Pegawai Tidak Valid.")->return(true);
		
		if($tgl_absensi == '')
			return $this->response->set_message("Tanggal Kosong.")->return(true);
		$tgl_absensi = date("Y-m-d", strtotime($tgl_absensi));
		
		# Cek Jika Data Sudah Ada
		$rs_cek = $this->model->getRowData(['id_pegawai'=>$id_pegawai,'tgl_absensi'=>$tgl_absensi,'id<>'=>$id]);
		if($rs_cek->num_rows() > 0)
			return $this->response->set_message("Data Sudah Ada.")->return(true);
		
		if($jam_masuk == '')
			return $this->response->set_message("Jam Masuk Kosong.")->return(true);
		$jam_masuk = date("H:i:s", strtotime($jam_masuk));
		
		if($jam_keluar == '')
			return $this->response->set_message("Jam Keluar Kosong.")->return(true);
		$jam_keluar = date("H:i:s", strtotime($jam_keluar));
		
		$this->db->trans_begin();
		if(!$this->model->editData(compact('id_pegawai','tgl_absensi','jam_masuk','jam_keluar'), $id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Update Presensi.")->return(true);
		}
		
		$this->app->save_histori('EDIT PRESENSI', 'absensi', $id);
		$this->db->trans_commit();
		
		$this->response
			->set_result('OK')
			->set_message("Presensi Berhasil Diperbaharui.")
			->set_callback("
				$('#form-modal').modal('toggle');
				tableView.ajax.reload(null, false);
			")
			->return(true);
		return;
	}
	
	public function hapus($args = null){
		$id = (int) $this->input->post('id');
		
		$rs_data = $this->model->getRowData(compact('id'));
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		
		$this->db->trans_begin();
		if(!$this->model->hapusData($id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Delete Presensi.")->return(true);
		}
		# Histori
		$this->app->save_histori('DEL PRESENSI', 'absensi', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Presensi Berhasil Di Hapus.")
			->set_callback("                                    
				$('#main-dialog-confirm').modal('toggle');
				tableView.ajax.reload(null, false);
			")
			->return(true);
		return;
	}
}
