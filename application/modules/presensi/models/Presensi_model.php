<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Presensi_model extends CI_Model {
	function __construct () {
        parent::__construct();
	}
	
	function getAllData($offset, $page_size, $sort, $filter = null, $search = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS
				a.id,
				a.id_pegawai,
				a.tgl_absensi,
				YEAR(a.tgl_absensi) AS tahun,
				MONTH(a.tgl_absensi) AS bulan,
				a.jam_masuk,
				a.jam_keluar,
				b.nama,
				b.jabatan,
				CONCAT(b.nama, ' ', b.nama)  AS search,
				MD5(CONCAT('absensi', '-', a.id)) AS history_id", false)
			->from("absensi a")
			->join("pegawai b", "b.id=a.id_pegawai", "INNER");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getAllPegawai($offset, $page_size, $sort, $filter = null, $search = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS a.id, a.nama, a.jabatan, CONCAT(a.nama, ' ', a.jabatan) AS search", false)
			->from("pegawai a");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getSuggestPegawai($search){
		$this->db
			->select("a.id, a.nama AS name, a.jabatan AS info", false)
			->from("pegawai a")
			->group_start()
				->like("a.nama", $search)
				->or_like("a.jabatan", $search)
			->group_end()
			->order_by("a.nama", "ASC")
			->limit(10);
		return $this->db->get()->result_array();
	}
	
	function getRowData($filters = null){
		$this->db->from("absensi a");
		if(!empty($filters)){
			foreach($filters as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->order_by('a.id')->get();
	}
	
	function addData($data){
		return $this->db->insert('absensi', $data);
	}
	
	function editData($data, $id){
		return $this->db->where("id", $id)->update('absensi', $data);
	}
	
	function hapusData($id){
		return $this->db->delete('absensi', compact('id'));
	}
}
