<style>
	input[name="pass_user"] {
		text-transform: none;
	}
</style>
<div class="card">
	<div class="card-header">
		<i class="fa fa-bars"></i>  <strong>Data User</strong>
		<div class="btn-group float-right show">
			<span><a id="refresh-view" class="card-header-action btn" title="Muat Ulang"><i class="fa fa-refresh"></i></a></span>
		</div>
	</div>
	<div class="card-body">
		<table id="table-view" style="width:100%"></table>
	</div>
</div>

<!-- Modal Add -->
<div id="form-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-success modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">Tambah Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				<span aria-hidden="true"><i class="fa fa-close"></i></span>
			</div>
			<div class="modal-body">
				<form id="form-tambah" class="">
					<div class="form-group row">
						<label for="nama_user" class="col-md-12 col-lg-3">USER</label>
						<div class="col-md-12 col-lg-9">
							<input type="hidden" id="id" name="id" value="">
							<input type="text" id="nama_user" name="nama_user" maxlength="50" onclick="this.select()" class="form-control" placeholder="NAMA USER">
						</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-md-12 col-lg-3">PEGAWAI</label>
						<div class="col-md-12 col-lg-9 input-group">
							<input type="hidden" id="id_pegawai" name="id_pegawai" value="">
							<input type="text" id="nama" name="nama" maxlength="50" class="form-control" onclick="this.select()" placeholder="NAMA PEGAWAI">
							<div class="input-group-append">
								<span id="find-pegawai" class="input-group-text bg-primary pointer" data-toggle="modal" data-target="#find-pegawai-modal">
									<i class="fa fa-search"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label for="jabatan" class="col-md-12 col-lg-3">JABATAN</label>
						<div class="col-md-12 col-lg-9">
							<input readonly type="text" id="jabatan" name="jabatan" maxlength="50" onclick="this.select()" class="form-control" placeholder="JABATAN">
						</div>
					</div>
					<div class="form-group row">
						<label for="pass_user" class="col-md-12 col-lg-3">PASSWORD</label>
						<div class="col-md-12 col-lg-9 input-group">
							<input type="text" id="pass_user" name="pass_user" maxlength="50" class="form-control" onclick="this.select()" placeholder="PASSWORD">
							<div class="input-group-append">
								<span class="input-group-text bg-dark pointer" title="Click Untuk Random" onclick="$('#pass_user').val(randString(8));">
									<i class="fa fa-refresh"></i>
								</span>
							</div>
						</div>
					</div>	
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-success" id="btn-simpan">Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Add -->

<!-- Find Modal Pegawai -->
<div id="find-pegawai-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">
					<i class="fa fa-bars"></i> Data Pegawai
				</h5>
				<span id="refresh-pegawai" aria-hidden="true" class="pointer" title="Muat Ulang"><i class="fa fa-refresh"></i></span>
			</div>
			<div class="modal-body">
				<table id="table-pegawai" style="width:100%"></table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- End Find Modal Pegawai -->

<script type="text/javascript">
	var current_module = '<?=base_url().$this->app->getCurrentModule()?>',
		acl_list = <?=$this->app->get_allow_acl(true);?>,
		defaultAction = (data) => {
			let all_action = '', action_histori = '';
			if(is_allow('edit', acl_list))
				all_action += '<a class="btn btn-warning btn-sm tombol-edit '+(data.status_user != '0' ? 'disabled' : '')+'" title="Edit Data"><i class="fa fa-edit text-white"></i></a> ';
			if(is_allow('hapus', acl_list))
				all_action += '<a class="btn btn-danger btn-sm tombol-hapus '+(data.status_user === '2' ? 'disabled' : '')+'" title="Non Aktif Data"><span class="btn-hapus"><i class="fa fa-trash text-white"></i></span></a> ';
			if(is_allow('approve1', acl_list)){
				if(data.status_user == '0')
					all_action += '<a class="btn btn-primary btn-sm tombol-approve1" title="Aktif Data"><span class="btn-approve"><i class="fa fa-lock text-white"></i></span></a> ';
				else if(data.status_user == '1')
					all_action += '<a class="btn btn-primary btn-sm tombol-reset" title="Reset Password Random"><span class="btn-histori"><i class="fa fa-asterisk text-white"></i></span></a> ';
				else
					all_action += '<a class="btn btn-primary btn-sm disabled" title="Aktif Data"><span class="btn-histori"><i class="fa fa-lock text-white"></i></span></a> ';
			}
			action_histori = '<a class="btn btn-dark btn-sm tombol-histori" title="Histori Data"><span class="btn-histori"><i class="fa fa-history text-white"></i></span></a>';
			return all_action + action_histori;
		},
		optionsView = {
			dom: "<'#row-header.row'<'col-sm-12 col-md-3 col-lg-2'><'col-sm-12 col-md-3 col-lg-5'><'col-sm-12 col-md-6 col-lg-5'f>>" +
				"<'row'<'col-12 col-sm-12 col-md-12'<'table-responsive'tr>>>" +
				"<'row'<'col-12 col-sm-12 col-md-3 col-lg-2'l><'col-12 col-sm-12 col-md-4 col-lg-6'i><'col-12 col-sm-12 col-md-5 col-lg-4'p>>",
			processing: true,
			serverSide: true,
			sPaginationType: 'full_numbers',
			aaSorting: [0, 'asc'],
			ajax: {
				url: current_module+'/index/get_data', 
				type: 'POST',
				data: function(data){
					let status_user = $('#table-view_filter').find('#filter_status').val() || '-',
						params = $.extend(true, data, {status_user});
					return params;
				}
			},
			initComplete: function(){
				if(is_allow('tambah', acl_list)){
					let button_add = $("button[data-target='#form-modal']");
					if(button_add.length > 0)
						button_add.remove();
					$('<button/>')
						.addClass('btn btn-success btn-block')
						.attr({'data-toggle':'modal', 'data-target':'#form-modal'})
						.html('<i class="fa fa-plus"></i> Tambah Data')
						.appendTo($('#row-header > div:first'));
				}
				let table = $(this),
					tableId = table[0].id,
					data = [{'label': 'Status', 'id':'filter_status', 'value': <?=get_status(true)?>}];
				// Create Filter
				create_filter(tableId, data);
				
				// Search Auto Focus
				$('#'+tableId+'_filter input[type="search"]')[0].focus();
				
				// Refresh View
				$('#refresh-view').off().on('click', function(e){
					e.preventDefault();
					table.DataTable().ajax.reload();
				});
			},
			columns: [
				{title: 'USER', width: '25%', data: 'nama_user'},
				{title: 'PEGAWAI', width: '25%', data: 'nama'},
				{title: 'JABATAN', width: '20%', data: 'jabatan'},
				{title: 'STATUS', width: '15%', data: 'status_user', 
					render: (data, type, row) => {
						return type === 'display' ? displayStatusMaster(row.status_user) : data;
					}
				},
				{title: '', data: null, orderable: false, searchable: false, className: 'text-center', 
					render: (data, type, row) => {
						return type === 'display' ? defaultAction(row) : data;
					}
				}
			],
			createdRow: (row, data, index) => {
				$(row).find('a.tombol-edit').data(data);
				$(row).find('a.tombol-hapus').data(data);
				$(row).find('a.tombol-approve1').data(data);
				$(row).find('a.tombol-reset').data(data);
				$(row).find('a.tombol-histori').data('history_id', data.history_id);
			}
		},
		tableView = $('#table-view').DataTable(optionsView),
		event = {
			// All Event Function
			acl_list : {
				tambah: function(){
					$("#btn-simpan").off().on('click', function(e){
						e.preventDefault();
						let url = current_module+'/tambah',
							data = new FormData($('#form-modal').find('form')[0]);
						load_url(url, data);
					});
				},
				edit: function(){
					tableView.on('click', 'a.tombol-edit', function(e){
						e.preventDefault();
						let data = $(this).data(),
							form = $("#form-modal");
						$('#form-modal div:first').removeClass('modal-success').addClass('modal-warning');
						form.find('#modal-title').html('Edit Data');
						form.find('#id').val(data.id);
						form.find('#nama_user').val(data.nama_user);
						form.find('#id_pegawai').val(data.id_pegawai);
						form.find('#nama').val(data.nama);
						form.find('#jabatan').val(data.jabatan);
						let btn_simpan = form.find('#btn-simpan'),
							btn_edit = form.find('#btn-simpan')
								.clone(true)
								.removeClass('btn-success')
								.addClass('btn-warning')
								.attr('id', 'btn-update')
								.html("Update")
								.appendTo(form.find('.modal-footer'));
						form.find('#btn-simpan').remove();
						btn_edit.off().click(function(e){
							e.preventDefault();
							let url = current_module+'/edit',
								data = new FormData($('#form-modal').find('form')[0]);
							load_url(url, data);
						});
						form.modal('toggle');
						
						form.on('hidden.bs.modal', function(e){
							form.find('#modal-title').html('Tambah Data');
							$('#form-modal div:first').removeClass('modal-warning').addClass('modal-success');
							$('#id, #nama_user, #id_pegawai, #nama, #jabatan, #pass_user').val('');
							btn_simpan.appendTo(form.find('.modal-footer'));
							btn_edit.remove();
							event.acl_list.tambah();
						});
					});
				},
				hapus: function(){
					tableView.on('click', 'a.tombol-hapus', function(e){
						e.preventDefault();
						let data = $(this).data();
							body = 'Data <span class="text-primary">'+data.nama_user+'</span> akan dihapus, apakah anda yakin ?',
							action = {
								url: current_module+'/hapus',
								data: {id: data.id}
							};
						confirm_dialog(action, body, {value:'Hapus', class:'danger'});
					});
				},
				approve1: function(){
					tableView.on('click', 'a.tombol-approve1', function(e){
						e.preventDefault();
						let data = $(this).data();
							body = 'Data <span class="text-danger">'+data.nama_user+'</span> akan disetujui, apakah anda yakin ?',
							action = {
								url: current_module+'/approve1/approve',
								data: {id: data.id}
							};
						confirm_dialog(action, body);
					});
				},
				reset_password: function(){
					tableView.on('click', 'a.tombol-reset', function(e){
						e.preventDefault();
						let data = $(this).data();
							body = 'Password user <span class="text-danger">'+data.nama_user+'</span> akan direset, apakah anda yakin ?',
							action = {
								url: current_module+'/approve1/reset_password',
								data: {id: data.id}
							};
						confirm_dialog(action, body, {value:'Reset', class:'primary'});
					});
				}
			},	
			histori: function(){
				tableView.on('click', 'a.tombol-histori', function(e){
					e.preventDefault();
					show_histori($(this).data('history_id'));
				});
			},
			findPegawai : function(){
				$('#find-pegawai').off().on('click', function(e){
					e.preventDefault();
					let options = create_find({
							ajax: {url: current_module+'/index/get_pegawai'},
							columns: [
								{title: 'NAMA PEGAWAI', width: '60%', data: 'nama'},
								{title: 'JABATAN', width: '40%', data: 'jabatan'},
							],
						}),
						tablePegawai = $('#table-pegawai').DataTable(options);
				});
				
				$('#table-pegawai').off().on('click', 'tbody tr', function(e){
					e.preventDefault();
					let data = $(this).data();
					$('#id_pegawai').val(data.id);
					$('#nama').val(data.nama);
					$('#jabatan').val(data.jabatan);
					$('#find-pegawai-modal').modal('toggle');
				});
			},
			show_form : function(){
				$('#form-modal').on('shown.bs.modal', function(){
					let elements = $(this);
					event_focus(elements);
					// Autocomplete Nama Pegawai
					$("#nama").autocomplete({
						minLength: 3,
						autoFocus: true,
						source: function(request, response){
							$.ajax({
								url: current_module+'/index/get_suggest_pegawai',
								dataType: "json",
								type: "POST",
								data: {searchText: request.term},
								success: function(data){
									response($.map(data, function(item){
										return {id: item.id, label: item.name, info: item.info};
									}));
								}
							});
						},
						select: function(event, data){
							$('#id_pegawai').val(data.item.id);
							$('#nama').val(data.item.label);
							$('#jabatan').val(data.item.info);
							return false;
						},
						change: function(event, data){
							if(data.item === null)
								$('#id_pegawai, #nama, #jabatan').val('');
						},
					}).autocomplete( "instance" )._renderItem = function(ul, item){
						return $("<li/>")
							.append('<div class="ui-menu-item-wrapper highlight">'+item.value+'<br/><small>'+item.info+'</small></div>')
							.appendTo(ul);
					};
				});
				
				$('#form-modal').on('hidden.bs.modal', function(e){
					$('#id, #nama_user, #id_pegawai, #nama, #jabatan, #pass_user').val('');
				});
			}
		};
		
	// All Event Call
	event.acl_list.tambah();
	event.acl_list.edit();
	event.acl_list.hapus();
	event.acl_list.approve1();
	event.acl_list.reset_password();
	
	event.histori();
	event.findPegawai();
	event.show_form();
</script>