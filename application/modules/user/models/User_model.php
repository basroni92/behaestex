<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class User_model extends CI_Model {
	function __construct () {
        parent::__construct();
	}
	
	function getAllData($offset, $page_size, $sort, $filter = null, $search = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS
				a.id,
				a.nama_user,
				a.id_pegawai,
				b.nama,
				b.jabatan,
				a.status_user,
				CONCAT(a.nama_user, ' ', b.nama, ' ', b.jabatan, ' ', IF(a.status_user='0', 'BARU', IF(a.status_user='1', 'AKTIF', 'NON AKTIF'))) AS search,
				MD5(CONCAT('user', '-', a.id)) AS history_id", false)
			->from("user a")
			->join("pegawai b", "b.id=a.id_pegawai", "INNER");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getAllPegawai($offset, $page_size, $sort, $filter = null, $search = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS a.id, a.nama, a.jabatan, CONCAT(a.nama, ' ', a.jabatan) AS search", false)
			->from("pegawai a")
			->join("user b", "b.id_pegawai=a.id AND b.status_user IN ('0', '1')", "LEFT")
			->where("ISNULL(b.id_pegawai) = TRUE");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getSuggestPegawai($search){
		$this->db
			->select("a.id, a.nama AS name, a.jabatan AS info", false)
			->from("pegawai a")
			->join("user b", "b.id_pegawai=a.id AND b.status_user IN ('0', '1')", "LEFT")
			->where("ISNULL(b.id_pegawai) = TRUE")
			->group_start()
				->like("a.nama", $search)
				->or_like("a.jabatan", $search)
			->group_end()
			->order_by("a.nama", "ASC")
			->limit(10);
		return $this->db->get()->result_array();
	}
		
	function getRowData($filters = null){
		$this->db
			->select("
				a.id,
				a.nama_user,
				a.id_pegawai,
				b.nama,
				b.jabatan,
				a.status_user,
				MD5(CONCAT('user', '-', a.id)) AS history_id", false)
			->from("user a")
			->join("pegawai b", "b.id=a.id_pegawai", "INNER");
		if(!empty($filters)){
			foreach($filters as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->order_by('a.nama_user')->get();
	}
	
	function getRowPegawai($filters = null){
		$this->db->from("pegawai");
		if(!empty($filters)){
			foreach($filters as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->order_by('nama')->get();
	}
		
	function get($conditions = array()){
		$this->db
			->select("a.id, a.nama_user, a.id_pegawai, b.nama, b.jabatan", false)
			->from('user a')
			->join('pegawai b', "b.id=a.id_pegawai", 'INNER');
		foreach($conditions as $col => $val){
			if(is_string($col))
				$this->db->where($col, $val);
			else $this->db->where($val);
		}
		return $this->db->order_by('a.nama_user')->get();
	}
	
	function add($data){
		return $this->db->insert('user', $data);
	}
	
	function editData($data, $id){
		return $this->db->where("id", $id)->update('user', $data);
	}
	
	function aktifData($id){
		return $this->db->where("id", $id)->update('user', ["status_user"=>'1']);
	}
	
	function batalData($id){
		return $this->db->where("id", $id)->update('user', ["status_user"=>'2']);
	}
	
	function resetPasswordUser($id, $password){
		return $this->db
			->set("pass_user", "MD5('$password')", false)
			->where("id", $id)
			->update('user');
	}
	
	function delete($id){
		return $this->db->delete('user', compact('id'));
	}
}
