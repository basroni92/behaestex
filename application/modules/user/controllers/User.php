<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class User extends MX_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('User_model', 'model');
    }
	
	protected function _middleware(){
        return array('auth', 'access');
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    public function index($args = null){
		# Data Parameters
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[$order][data]")." ".$order_by;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		
		switch($args[0]){
			case 'get_data':
				$filter['status_user'] = addslashes(trim($this->input->post("status_user")));
				$data_result = $this->model->getAllData($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			case 'get_pegawai':
				$data_result = $this->model->getAllPegawai($offset, $page_size, $sort, null, $search);
				echo json_encode($data_result);
				break;
			case 'get_suggest_pegawai':
				$params = addslashes(trim($this->input->post('searchText')));
				$data_result = $this->model->getSuggestPegawai($params);
				echo json_encode($data_result);
				break;
			default:
				$this->load->view("main");
		}
	}
	
	public function tambah($args = null){
		$nama_user 	= trim(strtoupper($this->input->post('nama_user')));
		$id_pegawai = (int) $this->input->post('id_pegawai');
		$pass_user 	= trim($this->input->post('pass_user'));
		
		if($nama_user == '')
			return $this->response->set_message("Nama User Kosong.")->return(true);
		# Cek Jika Nama User Sudah Ada
		$cek_ada = $this->model->getRowData(['nama_user'=>$nama_user, 'status_user<>'=>'2']);
		if($cek_ada->num_rows() > 0)
			return $this->response->set_message("Nama User Sudah Ada.")->return(true);
		
		if($id_pegawai == 0)
			return $this->response->set_message("Nama Pegawai Kosong.")->return(true);
		# Cek Jika Pegawai Sudah Ada
		$cek_ada = $this->model->getRowData(['a.id_pegawai'=>$id_pegawai, 'a.status_user<>'=>'2']);
		if($cek_ada->num_rows() > 0)
			return $this->response->set_message("Nama Pegawai Sudah Ada.")->return(true);
		
		# Cek Jika Data Pegawai Tidak Valid
		$rs_result = $this->model->getRowPegawai(['id'=>$id_pegawai]);
		if($rs_result->num_rows() == 0)
			return $this->response->set_message("Data Pegawai Tidak Valid.")->return(true);
		
		if($pass_user == "")
			return $this->response->set_message("Password Kosong.")->return(true);
		else if(strlen($pass_user) < 8)
			return $this->response->set_message("Password minimal harus 8 karakter.")->return(true);
		
		$pass_user = md5($this->input->post('pass_user'));
		
		$this->db->trans_begin();
		if(!$this->model->add(compact('nama_user','id_pegawai','pass_user'))){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Insert User.")->return(true);
		}
		$id = $this->db->insert_id();
		$this->app->save_histori('ADD USER', 'user', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("User Berhasil Ditambahkan.")
			->set_callback("
				$('#form-modal').modal('toggle');
				$('#nama_user, #id_pegawai, #nama, #jabatan, #pass_user').val('');
				tableView.ajax.reload();
			")
			->return(true);
		return;
	}
	
	public function edit($args = null){
		$id			= (int) $this->input->post('id');
		$nama_user 	= trim(strtoupper($this->input->post('nama_user')));
		$id_pegawai = (int) $this->input->post('id_pegawai');
		$pass_user 	= trim($this->input->post('pass_user'));
		
		# Cek Status
		$rs_data = $this->model->getRowData(['a.id'=>$id]);
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$rs_data = $rs_data->row();
		
		if($rs_data->status_user == "1")
			return $this->response->set_message("User Sudah Aktif.")->return(true);
		
		if($rs_data->status_user == "2")
			return $this->response->set_message("User Sudah Non Aktif.")->return(true);
		
		if($nama_user == '')
			return $this->response->set_message("Nama User Kosong.")->return(true);
		# Cek Jika Nama User Sudah Ada
		$cek_ada = $this->model->getRowData(['nama_user'=>$nama_user, 'status_user<>'=>'2', 'a.id<>'=>$id]);
		if($cek_ada->num_rows() > 0)
			return $this->response->set_message("Nama User Sudah Ada.")->return(true);
		
		if($id_pegawai == 0)
			return $this->response->set_message("Nama Pegawai Kosong.")->return(true);
		# Cek Jika Pegawai Sudah Ada
		$cek_ada = $this->model->getRowData(['a.id_pegawai'=>$id_pegawai, 'a.status_user<>'=>'2', 'a.id<>'=>$id]);
		if($cek_ada->num_rows() > 0)
			return $this->response->set_message("Nama Pegawai Sudah Ada.")->return(true);
		
		# Cek Jika Data Pegawai Tidak Valid
		$rs_result = $this->model->getRowPegawai(['id'=>$id_pegawai]);
		if($rs_result->num_rows() == 0)
			return $this->response->set_message("Data Pegawai Tidak Valid.")->return(true);
		
		if($pass_user <> "" && strlen($pass_user) < 8)
			return $this->response->set_message("Password minimal harus 8 karakter.")->return(true);
		
		$this->db->trans_begin();
		$data = compact('nama_user', 'id_pegawai');
		if($pass_user <> "")
			$data['pass_user'] = md5($this->input->post('pass_user'));
		if(!$this->model->editData($data, $id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Update User.")->return(true);
		}
		
		$this->app->save_histori('EDIT USER', 'user', $id);
		$this->db->trans_commit();
		
		$this->response
			->set_result('OK')
			->set_message("User Berhasil Diperbaharui.")
			->set_callback("
				$('#form-modal').modal('toggle');
				$('#id, #nama_user, #id_pegawai, #nama, #jabatan, #pass_user').val('');
				tableView.ajax.reload(null, false);
			")
			->return(true);
		return;
	}
	
	public function hapus($args = null){
		$id = (int) $this->input->post('id');
		
		# Cek Status
		$rs_data = $this->model->getRowData(['a.id'=>$id]);
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$rs_data = $rs_data->row();
		
		if($rs_data->status_user == "2")
			return $this->response->set_message("User Sudah Non Aktif.")->return(true);
		
		$this->db->trans_begin();
		if($rs_data->status_user == '0'){
			if(!$this->model->batalData($id)){
				$this->db->trans_rollback();
				return $this->response->set_message("Error Delete User.")->return(true);
			}
			# Histori
			$this->app->save_histori('DEL USER', 'user', $id);
			
			$this->db->trans_commit();
			$this->response
				->set_result('OK')
				->set_message("User Berhasil Di Non-Aktifkan.")
				->set_callback("                                    
					$('#main-dialog-confirm').modal('toggle');
					tableView.ajax.reload(null, false);
				")
				->return(true);
			return;
		} else if($rs_data->status_user == '1'){
			if(!$this->model->editData(["status_user"=>'0'], $id)){
				$this->db->trans_rollback();
				return $this->response->set_message("Error Update User.")->return(true);
			}
			# Histori
			$this->app->save_histori('CANCEL APP USER', 'user', $id);
			
			$this->db->trans_commit();
			$this->response
				->set_result('OK')
				->set_message("User Berhasil Di Batal Aktif.")
				->set_callback("                                    
					$('#main-dialog-confirm').modal('toggle');
					tableView.ajax.reload(null, false);
				")
				->return(true);
			return;
		}
	}
	
	public function approve1($args = null){
		$id = (int) $this->input->post('id');
		
		# Cek Status
		$rs_data = $this->model->getRowData(['a.id'=>$id]);
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$rs_data = $rs_data->row();
		
		if($rs_data->status_user == "2")
			return $this->response->set_message("User Sudah Non Aktif.")->return(true);
		
		switch($args[0]){
			case 'approve':
				if($rs_data->status_user == "1")
					return $this->response->set_message("User Sudah Aktif.")->return(true);
				
				$this->db->trans_begin();
				if(!$this->model->aktifData($id)){
					$this->db->trans_rollback();
					return $this->response->set_message("Error Approve User.")->return(true);
				}
				# Histori
				$this->app->save_histori('APP USER', 'user', $id);
				
				$this->db->trans_commit();
				$this->response
					->set_result('OK')
					->set_message("User Berhasil Di Aktifkan.")
					->set_callback("                                    
						$('#main-dialog-confirm').modal('toggle');
						tableView.ajax.reload(null, false);
					")
					->return(true);
				return;
				break;
			case 'reset_password':
				$this->db->trans_begin();
				$newPassword = get_random_string(8);
				if(!$this->model->resetPasswordUser($id, $newPassword)){
					$this->db->trans_rollback();
					return $this->response->set_message("Error Reset Password User.")->return(true);
				}
				# Histori
				$this->app->save_histori('RESET PASSWORD', 'user', $id);
				
				$this->db->trans_commit();
				$this->response
					->set_result('OK')
					->set_message("User <b>{$rs_data->nama_user}</b> password berhasil direset <br> [New Password : $newPassword].")
					->set_callback("                                    
						$('#main-dialog-confirm').modal('toggle');
						tableView.ajax.reload(null, false);
					")
					->return(true);
				return;
				break;
			default:
				return $this->response->set_message("Parameter Tidak Valid.")->return(true);
		}
	}
}
