<div class="card">
	<div class="card-header">
		<i class="fa fa-bars"></i>  <strong>Data Group Menu</strong>
		<div class="btn-group float-right show">
			<span><a id="refresh-view" class="card-header-action btn" title="Muat Ulang"><i class="fa fa-refresh"></i></a></span>
		</div>
	</div>
	<div class="card-body">
		<table id="table-view" style="width:100%"></table>
	</div>
</div>

<!-- Modal Add -->
<div id="form-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="FormGroupMenu" aria-hidden="true">
	<div class="modal-dialog modal-success modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">Tambah Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				<span aria-hidden="true"><i class="fa fa-close"></i></span>
			</div>
			<div class="modal-body">
				<form id="form-tambah" class="">
					<div class="form-group row">
						<label for="nama_gmenu" class="col-md-12 col-lg-2">NAMA</label>
						<div class="col-md-12 col-lg-10">
							<input type="hidden" id="id" name="id">
							<input type="text" id="nama_gmenu" name="nama_gmenu" maxlength="20" onclick="this.select()" class="form-control" placeholder="NAMA GROUP MENU">
						</div>
					</div>
					<div class="form-group row">
						<label for="icon_gmenu" class="col-md-12 col-lg-2">IKON</label>
						<div class="col-md-12 col-lg-10 input-group">
							<input type="text" id="icon_gmenu" name="icon_gmenu" maxlength="30" class="form-control" onclick="this.select()" placeholder="CARI IKON">
							<div class="input-group-append">
								<span id="find-icon" class="input-group-text bg-primary pointer" data-toggle="modal" data-target="#find-icon-modal">
									<i class="fa fa-search"></i>
								</span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-success" id="btn-simpan">Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Add -->

<!-- Find Modal Icon -->
<div id="find-icon-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 id="modal-title" class="modal-title">
					<i class="fa fa-bars"></i> Data Ikon
				</h5>
				<span id="refresh-find" aria-hidden="true" class="pointer" title="Muat Ulang"><i class="fa fa-refresh"></i></span>
			</div>
			<div class="modal-body">
				<table id="table-icon" style="width:100%"></table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- End Find Modal Icon -->

<script type="text/javascript">
	var current_module = '<?=base_url().$this->app->getCurrentModule()?>',
		acl_list = <?=$this->app->get_allow_acl(true);?>,
		defaultAction = (data) => {
			let all_action = '', action_histori = '';
			if(is_allow('edit', acl_list))
				all_action += '<a class="btn btn-warning btn-sm tombol-edit '+(data.status_gmenu != '0' ? 'disabled' : '')+'" title="Edit Data"><i class="fa fa-edit text-white"></i></a> ';
			if(is_allow('hapus', acl_list))
				all_action += '<a class="btn btn-danger btn-sm tombol-hapus '+(data.status_gmenu === '2' ? 'disabled' : '')+'" title="Non Aktif Data"><span class="btn-hapus"><i class="fa fa-trash text-white"></i></span></a> ';
			if(is_allow('approve1', acl_list))
				all_action += '<a class="btn btn-primary btn-sm tombol-approve1 '+(data.status_gmenu === '0' ? '' : 'disabled')+'" title="Aktif Data"><span class="btn-approve"><i class="fa fa-lock text-white"></i></span></a> ';
			action_histori = '<a class="btn btn-dark btn-sm tombol-histori" title="Histori Data"><span class="btn-histori"><i class="fa fa-history text-white"></i></span></a>';
			return all_action + action_histori;
		},
		optionsView = {
			dom: "<'#row-header.row'<'col-sm-12 col-md-3 col-lg-2'><'col-sm-12 col-md-3 col-lg-5'><'col-sm-12 col-md-6 col-lg-5'f>>" +
				"<'row'<'col-12 col-sm-12 col-md-12'<'table-responsive'tr>>>" +
				"<'row'<'col-12 col-sm-12 col-md-3 col-lg-2'l><'col-12 col-sm-12 col-md-4 col-lg-6'i><'col-12 col-sm-12 col-md-5 col-lg-4'p>>",
			processing: true,
			serverSide: true,
			sPaginationType: 'full_numbers',
			aaSorting: [0, 'asc'],
			ajax: {
				url: current_module+'/index/get_data', 
				type: 'POST',
				data: function(data){
					let form = $('#table-view_filter'),
						status_gmenu = form.find('#filter_status').val() || '-',
						params = $.extend(true, data, {status_gmenu});
					return params;
				}
			},
			initComplete: function(){
				if(is_allow('tambah', acl_list)){
					let button_add = $("button[data-target='#form-modal']");
					if(button_add.length > 0)
						button_add.remove();
					$('<button/>')
						.addClass('btn btn-success btn-block')
						.attr({'data-toggle':'modal', 'data-target':'#form-modal'})
						.html('<i class="fa fa-plus"></i> Tambah Data')
						.appendTo($('#row-header > div:first'));
				}
				let table = $(this),
					tableId = table[0].id,
					data = [{'label': 'Status', 'id':'filter_status', 'value': <?=get_status(true)?>}];
				// Create Filter
				create_filter(tableId, data);
				
				// Search Auto Focus
				$('#'+tableId+'_filter input[type="search"]')[0].focus();
				
				// Refresh View
				$('#refresh-view').off().on('click', function(e){
					e.preventDefault();
					table.DataTable().ajax.reload();
				});
			},
			columns: [
				{title: 'NAMA', width: '45%', data: 'nama_gmenu'},
				{title: 'IKON', width: '15%', data: 'icon_gmenu', className: 'text-center', orderable: false, searchable: false,
					render: (data, type, row) => {
						if(type === 'display')
							return '<i class="nav-icon icon-'+(row.icon_gmenu === '' ? 'list' : data.toLowerCase())+'"/>';
						return data;
					}
				},
				{title: 'STATUS', width: '20%', data: 'status_group', searchable: false,
					render: (data, type, row) => {
						return type === 'display' ? displayStatusMaster(row.status_gmenu) : data;
					}
				},
				{title: '', data: null, orderable: false, searchable: false, className: 'text-center', 
					render: (data, type, row) => {
						return type === 'display' ? defaultAction(row) : data;
					}
				}
			],
			createdRow: (row, data, index) => {
				$(row).find('a.tombol-edit').data(data);
				$(row).find('a.tombol-hapus').data(data);
				$(row).find('a.tombol-approve1').data(data);
				$(row).find('a.tombol-histori').data('history_id', data.history_id);
			}
		},
		tableView = $('#table-view').DataTable(optionsView),
		event = {
			// All Event Function
			acl_list : {
				tambah: function(){
					$("#btn-simpan").off().on('click', function(e){
						e.preventDefault();
						let url = current_module+'/tambah',
							data = {
								'nama_gmenu': $('#nama_gmenu').val(),
								'icon_gmenu': $('#icon_gmenu').val(),
							};
						load_url(url, data);
					});
				},
				edit: function(){
					tableView.on('click', 'a.tombol-edit', function(e){
						e.preventDefault();
						let data = $(this).data(),
							form = $("#form-modal");
						$('#form-modal div:first').removeClass('modal-success').addClass('modal-warning');
						form.find('#modal-title').html('Edit Data');
						form.find('#id').val(data.id);
						form.find('#nama_gmenu').val(data.nama_gmenu);
						form.find('#icon_gmenu').val(data.icon_gmenu);
						let btn_simpan = form.find('#btn-simpan'),
							btn_edit = form.find('#btn-simpan')
								.clone(true)
								.removeClass('btn-success')
								.addClass('btn-warning')
								.attr('id', 'btn-update')
								.html("Update")
								.appendTo(form.find('.modal-footer'));
						form.find('#btn-simpan').remove();
						btn_edit.off().click(function(e){
							e.preventDefault();
							let url = current_module+'/edit',
								data = {
									'id': $('#id').val(),
									'nama_gmenu': $('#nama_gmenu').val(),
									'icon_gmenu': $('#icon_gmenu').val(),
								};
							load_url(url, data);
						});
						form.modal('toggle');
						
						form.on('hidden.bs.modal', function(e){
							form.find('#modal-title').html('Tambah Data');
							$('#form-modal div:first').removeClass('modal-warning').addClass('modal-success');
							$('#id, #nama_gmenu, #icon_gmenu').val('');
							btn_simpan.appendTo(form.find('.modal-footer'));
							btn_edit.remove();
							event.acl_list.tambah();
						});
					});
				},
				hapus: function(){
					tableView.on('click', 'a.tombol-hapus', function(e){
						e.preventDefault();
						let data = $(this).data(),
							body = 'Data <span class="text-primary">'+data.nama_gmenu+'</span> akan dihapus, apakah anda yakin ?',
							action = {
								url: current_module+'/hapus',
								data: {id: data.id}
							};
						confirm_dialog(action, body, {value:'Hapus', class:'danger'});
					});
				},
				approve1: function(){
					tableView.on('click', 'a.tombol-approve1', function(e){
						e.preventDefault();
						let data = $(this).data(),
							body = 'Data <span class="text-danger">'+data.nama_gmenu+'</span> akan disetujui, apakah anda yakin ?',
							action = {
								url: current_module+'/approve1',
								data: {id: data.id}
							};
						confirm_dialog(action, body);
					});
				},
			},			
			histori: function(){
				tableView.on('click', 'a.tombol-histori', function(e){
					e.preventDefault();
					show_histori($(this).data('history_id'));
				});
			},
			findIcon : function(){
				$('#find-icon').off().on('click', function(e){
					e.preventDefault();
					let optionIcon = create_find({
							ajax: {url: current_module+'/index/get_icon'},
							columns: [
								{title: 'KODE IKON', data: 'icon'},
								{title: 'IKON', data: 'kode_icon', className: 'text-center', searchable: false, orderable: false,
									render: (data, type, row) => {
										return type === 'display' ? '<i class="nav-icon '+data+'"></i>' : data;
									}
								},
							],
						}),
						tableIcon = $('#table-icon').DataTable(optionIcon);
				});
				
				$('#table-icon').off().on('click', 'tbody tr', function(e){
					e.preventDefault();
					let data = $(this).data();
					$('#icon_gmenu').val(data.icon);
					$('#find-icon-modal').modal('toggle');
				});
			},
			show_form : function(){
				$('#form-modal').on('shown.bs.modal', function(){
					let elements = $(this);
					event_focus(elements);
					$("#icon_gmenu" ).autocomplete({
						minLength: 3,
						autoFocus: true,
						source: function(request, response){
							$.ajax({
								url: current_module+'/index/get_suggest_icon',
								dataType: "json",
								type: "POST",
								data: {searchText: request.term},
								success: function(data){
									response($.map(data, function(item){
										return {id: item.id, label: item.name, info: item.info, data: item};
									}));
								}
							});
						},
						select: function(event, data){
							$(this).val(data.item.data.name);
							return false;
						},
						change: function(event, data){
							if(data.item === null)
								$(this).val('')
						}
					})
					.autocomplete( "instance" )._renderItem = function(ul, item){
						return $("<li/>")
							.append('<div class="ui-menu-item-wrapper"><i class="nav-icon '+item.data.info+'"></i>'+item.data.name+'</div>')
							.appendTo(ul);
					};
				});
				$('#form-modal').on('hidden.bs.modal', function(e){
					$('#id, #nama_gmenu, #icon_gmenu').val('');
				});
			}
		};
		
	// All Event Call
	event.acl_list.tambah();
	event.acl_list.edit();
	event.acl_list.hapus();
	event.acl_list.approve1();
	
	event.histori();
	event.findIcon();
	event.show_form();
</script>