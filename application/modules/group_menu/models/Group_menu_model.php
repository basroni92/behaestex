<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Group_menu_model extends CI_Model {
	function __construct () {
        parent::__construct();
	}
	
	function getAllData($offset, $page_size, $sort, $filter = null, $search = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS
				a.id,
				a.nama_gmenu,
				a.icon_gmenu,
				a.status_gmenu,
				IF(a.status_gmenu='0', 'BARU', IF(a.status_gmenu='1', 'AKTIF', 'NON AKTIF')) AS status_group,
				CONCAT(a.nama_gmenu, ' ', IF(a.status_gmenu='0', 'BARU', IF(a.status_gmenu='1', 'AKTIF', 'NON AKTIF')))  AS search,
				MD5(CONCAT('gmenu', '-', a.id)) AS history_id", false)
			->from("gmenu a");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getAllIcon($offset, $page_size, $sort, $filter = null, $search = null){
		$this->db
			->select("SQL_CALC_FOUND_ROWS a.icon, a.kode_icon, a.icon AS search", false)
			->from("icon a");
		return $this->app->paramGetData($offset, $page_size, $sort, $filter, $search);
	}
	
	function getSuggestIcon($search){
		$this->db
			->select("a.icon AS name, a.kode_icon AS id, a.kode_icon AS info", false)
			->from("icon a")
			->like("a.icon", $search)
			->order_by("a.icon", "ASC")
			->limit(10);
		return $this->db->get()->result_array();
	}
	
	function getRowData($filters = null){
		$this->db
			->select("
				a.id,
				a.nama_gmenu,
				a.icon_gmenu,
				a.status_gmenu,
				IF(a.status_gmenu='0', 'BARU', IF(a.status_gmenu='1', 'AKTIF', 'NON AKTIF')) AS status_group,
				MD5(CONCAT('gmenu', '-', a.id)) AS history_id", false)
			->from("gmenu a");
		if(!empty($filters)){
			foreach($filters as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->order_by('a.nama_gmenu')->get();
	}
	
	function getRowIcon($filters = null){
		$this->db->from("icon");
		if(!empty($filters)){
			foreach($filters as $col=>$val){
				if(is_string($col))
					$this->db->where($col, $val);
				else
					$this->db->where($val);
			}
		}
		return $this->db->order_by('icon')->get();
	}
	
	function addData($data){
		return $this->db->insert('gmenu', $data);
	}
	
	function editData($data, $id){
		return $this->db->where("id", $id)->update('gmenu', $data);
	}
	
	function aktifData($id){
		return $this->db->where("id", $id)->update('gmenu', ["status_gmenu"=>'1']);
	}
	
	function batalData($id){
		return $this->db->where("id", $id)->update('gmenu', ["status_gmenu"=>'2']);
	}
}
