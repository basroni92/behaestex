<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Group_menu extends MX_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('Group_menu_model', 'model');
    }
	
	protected function _middleware(){
        return array('auth', 'access');
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    public function index($args = null){
		# Data Parameters
		$offset 	= (int) $this->input->post("start");
		$page_size 	= (int) $this->input->post("length");
		$order		= (int) $this->input->post("order[0][column]");
		$order_by	= $this->input->post("order[0][dir]");
		$sort 		= $this->input->post("columns[$order][data]")." ".$order_by;
		$search 	= addslashes(trim($this->input->post("search[value]")));
		
		switch($args[0]){
			case 'get_data':
				$filter['status_gmenu']	= addslashes(trim($this->input->post("status_gmenu")));
				$data_result = $this->model->getAllData($offset, $page_size, $sort, $filter, $search);
				echo json_encode($data_result);
				break;
			case 'get_icon':
				$data_result = $this->model->getAllIcon($offset, $page_size, $sort, null, $search);
				echo json_encode($data_result);
				break;
			case 'get_suggest_icon':
				$params = addslashes(trim($this->input->post('searchText')));
				$data_result = $this->model->getSuggestIcon($params);
				echo json_encode($data_result);
				break;
			default:
				$this->load->view("main");
		}
    }
	
	public function tambah($args = null){
		$nama_gmenu 	= trim(strtoupper($this->input->post('nama_gmenu')));
		$icon_gmenu 	= trim(strtoupper($this->input->post('icon_gmenu')));
		
		if($nama_gmenu == '')
			return $this->response->set_message("Nama Group Menu Kosong.")->return(true);
		
		if($icon_gmenu <> ""){
			$rs_icon = $this->model->getRowIcon(['icon'=>$icon_gmenu]);
			if($rs_icon->num_rows() == 0)
				return $this->response->set_message("Ikon Tidak Valid.")->return(true);				
		}
			
		# Cek Jika Nama Group Menu Sudah Ada
		$cek_ada = $this->model->getRowData(['nama_gmenu'=>$nama_gmenu, 'status_gmenu<>'=>'2']);
		if($cek_ada->num_rows() > 0)
			return $this->response->set_message("Nama Group Menu Sudah Ada.")->return(true);
		
		$this->db->trans_begin();
		if(!$this->model->addData(compact('nama_gmenu','icon_gmenu'))){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Insert Group Menu.")->return(true);
		}
		$id = $this->db->insert_id();
		$this->app->save_histori('ADD GMENU', 'gmenu', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Group Menu Berhasil Ditambahkan.")
			->set_callback("
				$('#form-modal').modal('toggle');
				$('#nama_gmenu, #icon_gmenu').val('');
				tableView.ajax.reload();
			")
			->return(true);
		return;
	}
	
    public function edit($args = null){
		$id 		= (int) $this->input->post('id');
		$nama_gmenu = trim(strtoupper($this->input->post('nama_gmenu')));
		$icon_gmenu = trim(strtoupper($this->input->post('icon_gmenu')));
		
		# Cek Status
		$rs_data = $this->model->getRowData(compact('id'));
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$rs_data = $rs_data->row();
		
		if($rs_data->status_gmenu == "1")
			return $this->response->set_message("Group Menu Sudah Aktif.")->return(true);
		
		if($rs_data->status_gmenu == "2")
			return $this->response->set_message("Group Menu Sudah Non Aktif.")->return(true);
		
		if($nama_gmenu == '')
			return $this->response->set_message("Nama Group Menu Kosong.")->return(true);
		
		if($icon_gmenu <> ""){
			$rs_icon = $this->model->getRowIcon(['icon'=>$icon_gmenu]);
			if($rs_icon->num_rows() == 0)
				return $this->response->set_message("Ikon Tidak Valid.")->return(true);				
		}
			
		# Cek Jika Nama Group Menu Sudah Ada
		$cek_ada = $this->model->getRowData(['nama_gmenu'=>$nama_gmenu, 'status_gmenu<>'=>'2', 'id<>'=>$id]);
		if($cek_ada->num_rows() > 0)
			return $this->response->set_message("Nama Group Menu Sudah Ada.")->return(true);
		
		$this->db->trans_begin();
		if(!$this->model->editData(compact('nama_gmenu','icon_gmenu'), $id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Update Group Menu.")->return(true);
		}
		
		$this->app->save_histori('EDIT GMENU', 'gmenu', $id);
		$this->db->trans_commit();
		
		$this->response
			->set_result('OK')
			->set_message("Group Menu Berhasil Diperbaharui.")
			->set_callback("
				$('#form-modal').modal('toggle');
				$('#id, #nama_gmenu, #icon_gmenu').val('');
				tableView.ajax.reload(null, false);
			")
			->return(true);
		return;
	}
	
	public function hapus($args = null){
		$id = (int) $this->input->post('id');
		
		# Cek Status
		$rs_data = $this->model->getRowData(compact('id'));
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$rs_data = $rs_data->row();
		
		if($rs_data->status_gmenu == "2")
			return $this->response->set_message("Group Menu Sudah Non Aktif.")->return(true);
		
		$this->db->trans_begin();
		if($rs_data->status_gmenu == '0'){
			if(!$this->model->batalData($id)){
				$this->db->trans_rollback();
				return $this->response->set_message("Error Delete Group Menu.")->return(true);
			}
			# Histori
			$this->app->save_histori('DEL GMENU', 'gmenu', $id);
			
			$this->db->trans_commit();
			$this->response
				->set_result('OK')
				->set_message("Group Menu Berhasil Di Non-Aktifkan.")
				->set_callback("                                    
					$('#main-dialog-confirm').modal('toggle');
					tableView.ajax.reload(null, false);
				")
				->return(true);
			return;
		}else if($rs_data->status_gmenu == '1'){
			if(!$this->model->editData(["status_gmenu"=>'0'], $id)){
				$this->db->trans_rollback();
				return $this->response->set_message("Error Update Group Menu.")->return(true);
			}
			# Histori
			$this->app->save_histori('CANCEL APP GMENU', 'gmenu', $id);
			
			$this->db->trans_commit();
			$this->response
				->set_result('OK')
				->set_message("Group Menu Berhasil Di Batal Aktif.")
				->set_callback("                                    
					$('#main-dialog-confirm').modal('toggle');
					tableView.ajax.reload(null, false);
				")
				->return(true);
			return;
		}
	}
	
	public function approve1($args = null){
		$id = (int) $this->input->post('id');
		
		# Cek Status
		$rs_data = $this->model->getRowData(compact('id'));
		if($rs_data->num_rows() == 0)
			return $this->response->set_message("Data Tidak Valid.")->return(true);
		$rs_data = $rs_data->row();
		
		if($rs_data->status_gmenu == "1")
			return $this->response->set_message("Group Menu Sudah Aktif.")->return(true);
		
		if($rs_data->status_gmenu == "2")
			return $this->response->set_message("Group Menu Sudah Non Aktif.")->return(true);
		
		$this->db->trans_begin();
		if(!$this->model->aktifData($id)){
			$this->db->trans_rollback();
			return $this->response->set_message("Error Approve Group Menu.")->return(true);
		}
		# Histori
		$this->app->save_histori('APP GMENU', 'gmenu', $id);
		
		$this->db->trans_commit();
		$this->response
			->set_result('OK')
			->set_message("Group Menu Berhasil Di Aktifkan.")
			->set_callback("                                    
				$('#main-dialog-confirm').modal('toggle');
				tableView.ajax.reload(null, false);
			")
			->return(true);
		return;
	}
}
