<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Logout extends MX_Controller {
    public function __construct() {
        parent::__construct();
    }
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)){
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}else{
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}else
				$this->index();
		}
    }
	
    function index($args = null){
		$session = array("userdata", "tempdata", "flashdata");
		try{
			foreach($session as $data)
				foreach($this->session->$data() as $key=>$sess_data)
					if(!preg_match("/^(__)/i", $key)){
						$unset_data = "unset_{$data}";
						echo $unset_data."\n".$key;
						$this->session->$unset_data($key);
					}
			session_write_close();
			redirect(base_url('/login'));
		}catch(Exception $e){
			redirect(base_url());
		}
    }
}
