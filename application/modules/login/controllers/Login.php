<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Login extends MX_Controller {
	
	function _remap($method = null, $args = array()){
		if(method_exists($this, $method)) {
			if($method == 'index' && $this->input->method() == 'post') {
				if(!empty($method)){
					if(!empty($args)) $this->_login(array_merge(array($method), $args));
					else $this->_login(array($method));
				}
				else $this->_login();
				return;
			}
			if(!empty($args)) $this->$method($args);
			else $this->$method();
		}
		else {
			if(!empty($method)){
				if(!empty($args)) $this->index(array_merge(array($method), $args));
				else $this->index(array($method));
			}
			else $this->index();
		}
    }
	
    public function __construct() {
        parent::__construct();
		$this->load->model('Login_model', 'model');
    }
	
    function index($args = null){
		$this->load->library('parser');
		if($this->session->flashdata('response')){
			$response = $this->session->flashdata('response');
			$data['Message'] = $response['Message'];
		} else
			$data['Message'] = '';
		$this->parser->parse("main", $data);
    }
	
	function _login($args = null){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		/* Trying to get user form locally */
		$userData = $this->model->user($username, $password);
		if($userData->num_rows() > 0){
			$id_user = $userData->row()->id;
			$nama_user = $userData->row()->nama_user;
			$id_pegawai = $userData->row()->id_pegawai;
			$pegawai = $userData->row()->nama;
			$this->session->set_userdata(["user" => compact('id_user', 'nama_user', 'id_pegawai', 'pegawai')]);
			redirect(base_url());
		}
		/* If login failed */
		else {
			$this->load->library('parser');
			$Message = "
				<div class='alert alert-danger alert-dismissible fade show' role='alert'>
					<strong>Login Gagal !</strong> Username / Password Salah.
					<button class='close' type='button' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>×</span>
					</button>
				</div>";
			$this->session->set_flashdata("response", compact('Message'));
			$this->parser->parse("main", compact('Message'));
		}
	}
}
