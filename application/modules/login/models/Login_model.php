<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Login_model extends CI_Model {
	
	function __construct () {
        parent::__construct();
		$this->load->model('user/User_model');
	}
	
	function user($username, $password){
		return $this->User_model->get(['nama_user'=>$username, 'pass_user'=>md5($password), 'status_user'=>'1']);
	}
		
	function get($conditions = array()){
		$this->db->from('user');
		foreach($conditions as $col => $val){
			if(is_string($col))
				$this->db->where($col, $val);
			else $this->db->where($val);
		}
		return $this->db->order_by('nama_user')->get();
	}
}
