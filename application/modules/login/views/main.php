<!DOCTYPE html>
<html lang="en">
<head>
	<base href="./">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>BEHAESTEX | Login</title>
	<!-- Icons-->
	<link href="<?=base_url()?>assets/img/brand/logo.png" rel="icon">
	<link href="<?=base_url()?>assets/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
	<!-- Main styles for this application-->
	<link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
</head>

<body class="app flex-row align-items-center">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card-group">
					<div class="card p-4">
						<div class="card-body">
							{Message}
							<form action="<?=base_url()?>login" method="POST">
								<h1>Login</h1>
								<p class="text-muted">Masukkan Username dan Password</p>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="icon-user"></i>
										</span>
									</div>
									<input name="username" type="text" class="form-control" placeholder="Username" required="" value="" onclick="this.select()"/>
								</div>
								<div class="input-group mb-4">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="icon-lock"></i>
										</span>
									</div>
									<input name="password" type="password" class="form-control" placeholder="Password" required="" value="" onclick="this.select()"/>
								</div>
								<div class="row">
									<div class="col-6">
										<button type="submit" class="btn btn-primary px-4" type="button">Login</button>
									</div>
									<div class="col-6 text-right">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bootstrap and necessary plugins-->
	<script src="<?=base_url()?>assets/node_modules/jquery/dist/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/pace-progress/pace.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
	<script src="<?=base_url()?>assets/node_modules/@coreui/coreui/dist/js/coreui.min.js"></script>
</body>
</html>