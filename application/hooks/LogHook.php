<?php
class LogHook {
	private $ci;
	private $db;
	private $session;
	
	public function __construct() {
		$this->ci =& get_instance();
		$this->ci->load->database();
		$this->db =& $this->ci->db;
		$this->ci->load->library('session');
		$this->session = $this->ci->session;
	}
	
    public function queries(){
        $queries = $this->ci->db->queries;
        $query_times = $this->ci->db->query_times;
		$queries_count = count($queries);
		$id_user = (int) $this->ci->session->user['id_user'];
		
        if($queries_count == 0)
            return;
		
		$to_insert = array();
		foreach ($queries as $key=>$query){
			$to_insert[] = array(
				"id_user"	=> (bool) $id_user ? (int) $id_user : null,
				"query"		=> substr($query, 0, 65535),
				"waktu"	=> $query_times[$key]
			);
			if(($key + 1) % 25 == 0 || ($key + 1) == $queries_count){
				$row_inserted = $this->db->insert_batch("log_query", $to_insert);
				if(!$row_inserted || $row_inserted < count($to_insert)){}
				$to_insert = array();
			}
		}
    }
}
